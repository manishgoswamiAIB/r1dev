({
    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        22/05/2019
    * @Description: Fire the method to open customer Criteria Page
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    doInit: function(component, event, helper){
        
        helper.callServer(component, 
                          "c.getPicklistValues",
                          function(resultStr){ 
                              var result = JSON.parse(resultStr);
                              if (result.length === 0) {
                                  this.handlePopulatePicklistError(component, event, result);
                                  
                              }
                              else {
                                  this.handlePopulatePicklistSuccess(component, event, result);
                              }				
                          },
                          { }
                         );        
    },
    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        22/05/2019
    * @Description: Fire the method when back button is clicked
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    handleBack : function(component, event, helper) {
        var cmpEvent = component.getEvent("openSelectedSection");
        cmpEvent.setParams({
            "selectedSection" : "SelectedCriteria" 
        })
        cmpEvent.fire();
    },
    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        22/05/2019
    * @Description: Fire the method to reset fields
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    handleReset: function(component, event, helper) {
        
        //To remove customerType and Number Error
        this.addValidityError(component, "inputCardNumber", "");
        this.addValidityError(component, "contactNumberSecond", "");
        this.addValidityError(component, "birthId", "");
        

        component.set("v.isChildVisible", false);
        
        component.set("v.customer",{'customerType': 'P',
                                    'name':'',
                                    'dateOfBirth':'2019-5-2',
                                    'NSC':'',
                                    'contactNumberFirst':'',
                                    'contactNumberSecond':'',
                                    'cardType':'',
                                    'cardNumber':'',
                                    'selfServiceNo':'',
                                    'county':'',
                                    'postCode':'', 
                                    'addressLineOne':''
                                    });
        
		var customerRec = component.get("v.customer");  
        customerRec.dateOfBirth = '';
        component.set("v.customer", customerRec); 
        component.set("v.showErrorMessage", false);
        component.set("v.isGenericError",false);
        component.set("v.responseList",[]);
        component.set("v.isChildVisible", true);
        //To check if contact number field on UI is invalid
        this.validateUI(component,"birthId");
        //To check if contact number field on UI is invalid
        this.validateUI(component,"contactNumberFirst");
        //To check if contact number field on UI is invalid
        this.validateUI(component,"contactNumberSecond");
        //To check if Card Number field on UI is invalid
        this.validateUI(component,"inputCardNumber");
        //To check if SSN field on UI is invalid
        this.validateUI(component,"inputSSN");
    },
    
    addValidityError: function(component, cmpAuraId, message) {
        var inputCmp = component.find(cmpAuraId);
        inputCmp.setCustomValidity(message);
        inputCmp.reportValidity();
    }, 
    
    addErrorClass: function(component, cmpAuraId) {
        var inputCmp = component.find(cmpAuraId);
        $A.util.addClass(inputCmp,"slds-has-error");
    }, 
    
    removeErrorClass: function(component, cmpAuraId) {
        var inputCmp = component.find(cmpAuraId);
        $A.util.removeClass(inputCmp,"slds-has-error");
    }, 
     /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        22/05/2019
    * @Description: check and show error if only of the field is populated in Card Type And Number
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    validateCardTypeAndNumber: function(component) {
        var parameterList=component.get("v.customer");
        if(($A.util.isEmpty(parameterList.cardType) && ! $A.util.isEmpty(parameterList.cardNumber ))||
           (! $A.util.isEmpty(parameterList.cardType)&& $A.util.isEmpty(parameterList.cardNumber))){
            component.set("v.typeOfMessage", "error");
            component.set("v.showErrorMessage",true);
            component.set("v.isGenericError",true);
            this.addValidityError(component, "inputCardNumber", $A.get("$Label.c.AIB_Error_LocatedByCustomerCardType"));
          } 
        else {
            this.addValidityError(component, "inputCardNumber", "");// if there was a custom error before, reset it
        	
        }
    }, 
    
    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        22/05/2019
    * @Description: check and show error if all fields except card type are empty.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    contactNumberValidation: function(component) {
        var parameterList=component.get("v.customer");
        var errorList=component.get("v.allErrors");
        if((!$A.util.isEmpty(parameterList.contactNumberFirst) 
            || !$A.util.isEmpty(parameterList.contactNumberSecond))
           && !(!$A.util.isEmpty(parameterList.contactNumberFirst) 
                 && !$A.util.isEmpty(parameterList.contactNumberSecond)) 
        ){
            component.set("v.isSpinner", false);
            component.set("v.showError",true);
            this.addValidityError(component, "contactNumberSecond", $A.get("$Label.c.AIB_Error_LocateByCustomerPhoneNo"));
            
        }
        else if (!$A.util.isEmpty(parameterList.contactNumberFirst) 
                 && !$A.util.isEmpty(parameterList.contactNumberSecond) 
                 && $A.util.isEmpty(parameterList.name.trim())
        ){
            component.set("v.showError",true);
            this.addValidityError(component, "contactNumberSecond", "");
            component.set("v.typeOfMessage", "Error");
            errorList.push($A.get("$Label.c.AIB_GB_Error_PopulateName"));
        }
        else {
            this.addValidityError(component, "contactNumberSecond", "");// if there was a custom error before, reset it
            component.set("v.showError",false);
        }
        component.set("v.allErrors",errorList);
    },
     /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        6/06/2019
    * @Description: check and show error if other fields are populated when Contact Number field is selected with Name.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
   /* validateContactNumberRestriction: function(component){
          var parameterList = component.get("v.customer");
        //Validation for county and Name restriction
        var isContactFirstSelected = false;
	    var isContactSecondSelected = false;
        var isNameSelected = false;
        var isOtherFieldSelected = false;
        for (var key in parameterList) {
            
            if (key == "contactNumberFirst" 
                && !$A.util.isEmpty(parameterList[key].trim())
            ){  
                isContactFirstSelected = true;
            }
			if (key == "contactNumberSecond" 
                && !$A.util.isEmpty(parameterList[key].trim())
            ){  
                isContactSecondSelected = true;
            }
            if (key == "name" 
                && !$A.util.isEmpty(parameterList[key].trim())
            ){  
                isNameSelected = true;
            } 
            
            if (key != "contactNumberFirst" 
                && key != "contactNumberSecond" 
                && key != "name" && key != "customerType" 
                && !$A.util.isEmpty(parameterList[key].trim())
            ) {
                isOtherFieldSelected = true;
            }
        }
        
        if ((isContactFirstSelected === true) 
            && (isNameSelected === true) && (isContactSecondSelected === true)
            && (isOtherFieldSelected === true)
        ) {
            component.set("v.isSpinner", false);
            component.set("v.typeOfMessage", "error");
            var errorsList = component.get("v.allErrors");
            errorsList.push($A.get("$Label.c.AIB_GB_Error_ContactWithNameValidation"));
            component.set("v.allErrors",errorsList);
        }  
    },*/
    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        22/05/2019
    * @Description: check and show error if Birthdate is selected when Type is Organization.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    birthdateValidation: function(component) {
        var parameterList=component.get("v.customer");
        var errorList=component.get("v.allErrors");
        //Validation for DOB not required for Organisation customer type.
        if( !$A.util.isEmpty(parameterList.dateOfBirth))
        {
            this.addValidityError(component, "birthId", "");
            if(parameterList.customerType==='O'){
                component.set("v.isSpinner", false);
                component.set("v.typeOfMessage", "error");
                component.set("v.showErrorMessage",true);
                component.set("v.isGenericError",true);
                this.addValidityError(component, "birthId", $A.get("$Label.c.AIB_Error_LocateByCustomerOrgDOB"));
            }else if($A.util.isEmpty(parameterList.name.trim())){
                component.set("v.isSpinner", false);
                component.set("v.typeOfMessage", "error");
                component.set("v.showErrorMessage",true);
                component.set("v.isGenericError",false);
                errorList.push($A.get("$Label.c.AIB_GB_Error_DOBandNameValidation")); 
            }else{
                var isDOBOtherSelected = false;
                for (var key in parameterList) {
                    if(key != "dateOfBirth" 
                       && key != "name" 
                       && key!="customerType" 
                       && !$A.util.isEmpty(parameterList[key].trim())
                    ){
                        isDOBOtherSelected = true;
                    }
                } 
                if (isDOBOtherSelected) {
                    component.set("v.isSpinner", false);
                    component.set("v.showErrorMessage",true);
                    component.set("v.typeOfMessage", "error");
                    component.set("v.isChildVisible", true);
                    errorList.push($A.get("$Label.c.AIB_GB_Error_OnlyCustTypeAndDOB"));
                 }
            }
        }
        component.set("v.allErrors",errorList);
        
    },
    
    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        22/05/2019
    * @Description: check and show error if other fields are populated with card type field.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    validateNoFieldsWithCardType : function(component){
        var parameterList = component.get("v.customer");
        
        //Validation for cardType and cardNumber restriction
        var isCardTypeSelected = false;
        var isCardNumberSelected = false;
        var isOtherFieldSelected = false;
        for (var key in parameterList) {
            
            if (key == "cardType" 
                && !$A.util.isEmpty(parameterList[key].trim())
            ){  
                isCardTypeSelected = true;
            }
            
            if (key == "cardNumber" 
                && !$A.util.isEmpty(parameterList[key].trim())
            ){  
                isCardNumberSelected = true;
            } 
            
            if (key != "cardType" 
                && key != "customerType" 
                && key != "cardNumber" 
                && !$A.util.isEmpty(parameterList[key].trim())
            ) {
                isOtherFieldSelected = true;
            }
        }
        
        if ((isCardTypeSelected === true) 
            && (isOtherFieldSelected === true) 
            && (isCardNumberSelected === true)
        ) {
            component.set("v.isSpinner", false);
            component.set("v.typeOfMessage", "error");
            var errorsList = component.get("v.allErrors");
            errorsList.push($A.get("$Label.c.AIB_GB_Label_LocateByCustomerCardTypeOnly"));
            component.set("v.allErrors",errorsList);
        } 
    },
    
    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        22/05/2019
    * @Description: check and show error if all fields except card type are empty.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    validateAllEmptyFields : function(component){
        
        var parameterList = component.get("v.customer");
        var isMapValueEmpty=true;
        for (var key in parameterList) {
            if (key != "customerType" 
                && !$A.util.isEmpty(parameterList[key].trim())
            ) { 
                isMapValueEmpty = false;
            } 
        }
        if (isMapValueEmpty === true) {
            component.set("v.isSpinner", false);
            component.set("v.isChildVisible", true);
            component.set("v.typeOfMessage", "error");
            var errorsList=component.get("v.allErrors");
            errorsList.push($A.get("$Label.c.AIB_Error_LocateByCustomerEmptySearch"));
            component.set("v.allErrors",errorsList);
        }   
    },
    
    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        22/05/2019
    * @Description: check and show error if other fields are populated when NSC field is selected.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    validateNSCAndAccountNumber : function(component){
        var parameterList = component.get("v.customer");
        var isNSCSelected=false;
        var isAnotherFieldSelected=false;
        for (var key in parameterList) {
            
            if (key == "NSC" 
                && !$A.util.isEmpty(parameterList[key].trim())
             ){  
                isNSCSelected = true;
            } 
            else if (key != "NSC" 
                    && key != "customerType" 
                    && !$A.util.isEmpty(parameterList[key].trim())
            ) {
                isAnotherFieldSelected = true;
            }
        }
        if (isNSCSelected === true  
            && isAnotherFieldSelected === true
        ) {
            component.set("v.isSpinner", false);
            component.set("v.typeOfMessage", "error");
            var errorsList=component.get("v.allErrors");
            errorsList.push($A.get("$Label.c.AIB_GB_Error_NSCValidation"));
            component.set("v.allErrors",errorsList);
        } 
        
    },
    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        22/05/2019
    * @Description: check and show error if other fields are populated when county field is selected.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    validateCountyField: function(component){
    var parameterList = component.get("v.customer");
         if ( !$A.util.isEmpty(parameterList.county) && $A.util.isEmpty(parameterList.name.trim())){  
                component.set("v.isSpinner", false);
                component.set("v.typeOfMessage", "error");
                var errorsList=component.get("v.allErrors");
                errorsList.push($A.get("$Label.c.AIB_Error_LocateByCustomerCountyOnly"));
                component.set("v.allErrors",errorsList);
            }	
		
    },
      /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        22/05/2019
    * @Description: check and show error if other fields are populated when county field is selected.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
   /* validateCountyRestriction: function(component){
          var parameterList = component.get("v.customer");
        //Validation for county and Name restriction
        var isCountySelected = false;
        var isNameSelected = false;
        var isOtherFieldSelected = false;
        for (var key in parameterList) {
            
            if (key == "county" 
                && !$A.util.isEmpty(parameterList[key].trim())
            ){  
                isCountySelected = true;
            }
            
            if (key == "name" 
                && !$A.util.isEmpty(parameterList[key].trim())
            ){  
                isNameSelected = true;
            } 
            
            if (key != "county" 
                && key != "customerType" 
                && key != "name" 
                && !$A.util.isEmpty(parameterList[key].trim())
            ) {
                isOtherFieldSelected = true;
            }
        }
        
        if ((isCountySelected === true) 
            && (isNameSelected === true) 
            && (isOtherFieldSelected === true)
        ) {
            component.set("v.isSpinner", false);
            component.set("v.typeOfMessage", "error");
            var errorsList = component.get("v.allErrors");
            errorsList.push($A.get("$Label.c.AIB_GB_Error_CountyRestrictionValidation"));
            component.set("v.allErrors",errorsList);
        }  
    },*/
    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        22/05/2019
    * @Description: Fire to validate UI errors
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    validateUI : function(component,cmpAuraId){
        component.find(cmpAuraId).showHelpMessageIfInvalid();
        var inputCmp =component.find(cmpAuraId).get('v.validity').valid;
        if(inputCmp===false){
            component.set("v.isSpinner", false);
            component.set("v.typeOfMessage", "error");
            component.set("v.showErrorMessage",true);
            component.set("v.isGenericError",true);
        }
        
    },
    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        6/6/2019
    * @Description: check and show error if other fields are populated when SSN field is selected.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    validateSSN : function(component,cmpAuraId){
        var parameterList = component.get("v.customer");
        var isSSNSelected=false;
        var isAnotherFieldSelected=false;
        for (var key in parameterList) {
            
            if (key == "selfServiceNo" 
                && !$A.util.isEmpty(parameterList[key].trim())
             ){  
                isSSNSelected = true;
            } 
            else if (key != "selfServiceNo" 
                    && key != "customerType" 
                    && !$A.util.isEmpty(parameterList[key].trim())
            ) {
                isAnotherFieldSelected = true;
            }
        }
        if (isSSNSelected === true  
            && isAnotherFieldSelected === true
        ) {
            component.set("v.isSpinner", false);
            component.set("v.typeOfMessage", "error");
            var errorsList=component.get("v.allErrors");
            errorsList.push($A.get("$Label.c.AIB_GB_Error_SSNValidation"));
            component.set("v.allErrors",errorsList);
        } 
        
    },
    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        22/05/2019
    * @Description: Fire the method to retrive customer
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    handleSearch: function(component, event, helper) {
        component.set("v.errorFlag",false);
        component.set("v.isChildVisible", false);
        component.set("v.isSpinner", true);
        component.set("v.showErrorMessage", false);
        component.set("v.isGenericError",false);
        var allErrors = [];
        component.set("v.allErrors",allErrors);
       var parameterList = component.get("v.customer");
        //alert(parameterList.dateOfBirth);
         if($A.util.isEmpty(parameterList.dateOfBirth)){
            parameterList.dateOfBirth="";
        }
        component.set("v.customer",parameterList);
        //i: to check if all fields have been populated
        this.validateAllEmptyFields(component);
        //i: to validate card type
        this.validateNoFieldsWithCardType(component);
        //Validation for Phone number
        this.contactNumberValidation(component);
         //Validation for Phone number with name only
        //this.validateContactNumberRestriction(component)
        //i: to validate the NSC and Account Number
        this.validateNSCAndAccountNumber(component);
        //i: to validate the birthdate
        this.birthdateValidation(component);
        //Validation for when only County is selected for search
        this.validateCountyField(component);
        //Validation for when other field is selected with County and Name search
        //this.validateCountyRestriction(component)
        //Validation for CardType And Number fields
        this.validateCardTypeAndNumber(component);
         //Validation for SSN fields
        this.validateSSN (component);
        //To check if contact number field on UI is invalid
        this.validateUI(component,"contactNumberFirst");
        //To check if contact number field on UI is invalid
        this.validateUI(component,"contactNumberSecond");
        //To check if Card Number field on UI is invalid
        this.validateUI(component,"inputCardNumber");
        //To check if SSN field on UI is invalid
        this.validateUI(component,"inputSSN");
      
        var allErrors = component.get("v.allErrors");
        if(allErrors.length>0 || component.get("v.isGenericError")){
            component.set("v.isSpinner", false);
            component.set("v.responseList",[]);
            component.set("v.isChildVisible", true);
            component.set("v.showErrorMessage", true);
            
        }
        else
        {
            var parameterList=component.get("v.customer");
            //i: parameters to be sent to apex
            var params = {
                customerType : parameterList.customerType,
                name : parameterList.name,
                dateOfBirth: parameterList.dateOfBirth,
                NSC: parameterList.NSC,
                contactNumberFirst : parameterList.contactNumberFirst,
                contactNumberSecond : parameterList.contactNumberSecond,
                cardType : parameterList.cardType,
                cardNumber : parameterList.cardNumber,
                selfServiceNo : parameterList.selfServiceNo,
                county : parameterList.county,
                postCode : parameterList.postCode,
                addressLineOne : parameterList.addressLineOne,
                region:component.get("v.userRegion"),
                integrationSettingName:$A.get("$Label.c.AIB_GB_Label_IntegrationSettingName")
            };    
            
            helper.callServer(component, "c.getCustomerResult",
                              function(resultStr){ 
                                  
                                  var result = JSON.parse(resultStr);
                                  if (result.hasError) {
                                      this.showToast(component, result.message,"Toast");
                                  }else if(result.hasWarning){
                                      this.showToast(component, result.message,"Toast");
                                  }
                                  else {
                                      var res = result.customersResponse;
                                      //Error message when result is More than 80
                                      if(res.length >= 80){
                                          component.set("v.responseList",res);
                                          component.set("v.isChildVisible", true);
                                          component.set("v.isSpinner", false);
                                          this.showToast(component, $A.get("$Label.c.AIB_GB_Error_MaxCustomerResult"),"Toast");  
                                      }
                                      else{
                                          component.set("v.responseList",res);
                                          component.set("v.isChildVisible", true);
                                          component.set("v.isSpinner", false);
                                      }
                                  }				
                              },
                              { 
                                  "customerParamStr" : JSON.stringify(params)
                              }
                             );
            
        }
        
    },
    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        22/05/2019
    * @Description: Fire the method to get picklist Values
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    handlePopulatePicklistSuccess :function(component, event, result){
        var CustomerTypeMap = [];
        var CardTypeMap = [];
        CardTypeMap.push({label:"None" , value: ""});
        var CountyMap = [];
        CountyMap.push({label:"None" , value: ""});
        for (var key in result) {
            if (result.hasOwnProperty(key)) {
                for (var key1 in result[key]) {
                    if(result[key][key1].AIB_FieldType__c=="Customer Type"){
                        CustomerTypeMap.push({label:result[key][key1].Label__c , value: result[key][key1].Value__c});
                    }
                    if(result[key][key1].AIB_FieldType__c=="Card Type"){
                        CardTypeMap.push({label:result[key][key1].Label__c , value: result[key][key1].Value__c});
                    }
                    if(result[key][key1].AIB_FieldType__c=="County"){
                        CountyMap.push({label:result[key][key1].Label__c , value: result[key][key1].Value__c});
                    }
                }
            }
        }
        component.set("v.CustomerType", CustomerTypeMap);
        component.set("v.cardType", CardTypeMap);
        component.set("v.county", CountyMap);
    },
    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        22/05/2019
    * @Description: Fire the method to enable/disable Confirm button
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    handlePopulatePicklistError :function(component, event, result){
        var CustomerTypeMap = [],CardTypeMap = [], CountyMap = [];
        CustomerTypeMap.push({label:"None" , value: ""});
        CardTypeMap.push({label:"None" , value: ""});
        CountyMap.push({label:"None" , value: ""});
        component.set("v.CustomerType", CustomerTypeMap);
        component.set("v.cardType", CardTypeMap);
        component.set("v.county", CountyMap);
    },
    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        22/05/2019
    * @Description: Fire the method to enable/disable Confirm button
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    handleCustomersChangeEventHelper : function(component, event, helper){ 
        
        var confirmIndicator = event.getParam("confirmButtonDisabled");
        var selectedCustomerRecordList = event.getParam("selectedCustomerRecords");
        component.set("v.isConfirmEnabled",confirmIndicator);
        component.set("v.restoredRecordsList",selectedCustomerRecordList);
    },
    
    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        5/29/2019
    * @Description: Fire the toast message on Error
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    
    showToast : function(component, message, type) {
        var allErrors=[];
        allErrors=component.get("v.allErrors");
        allErrors.push(message);
        component.set("v.isSpinner", false);
        component.set("v.isChildVisible", true);
        component.set("v.allErrors",allErrors);
        //component.set("v.errorFlag",true);
        component.set("v.typeOfMessage", type);
        component.set("v.showErrorMessage",true);
        component.set("v.isGenericError",false);
    },
   
    /**********************************************************************************************
    * @Author:      Sameer Jewalikar
    * @Date:        5/29/2019
    * @Description: This method will handle the confirm button callout for selected customers.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
     handleConfirmCustomersHelper : function(component, event, helper) {
         component.set("v.isSpinner", true);
        var confirmedCustomerList = component.get("v.restoredRecordsList");
        //var params=[];
        var selectedCustomerId=[];
        var cifParam='customerId=';        
        for (var i=0;i<confirmedCustomerList.length;i++) {            
            selectedCustomerId.push(confirmedCustomerList[i].customerID)                        
        }
        selectedCustomerId= selectedCustomerId.join();
        cifParam=cifParam.concat(selectedCustomerId.toString());
        var params = {
            selectedCustomerId : cifParam,
            recordId : component.get("v.recordId"),
       		integrationSettingName : $A.get("$Label.c.AIB_GB_CustomerGetBorrowerSettingName"),
            region:component.get("v.userRegion"),
        }                
        helper.callServer(component, "c.getBorrowerResult",
                          function(resultStr){                               
                              var result = JSON.parse(resultStr);                              
                              if (result.hasError) {
                                  this.showToast(component, result.message,"Toast");
                              } else if (result.hasWarning) {
                                  this.showToast(component, result.message,"Toast");
                              }
                              else {
                                  component.set("v.isSpinner", false);
                                  this.navigateToRelationshipRecord(component, event, helper, result);
                              }                                                           
                          },
                          { 
                              "customerListParamStr" : JSON.stringify(params)
                          }
                         );        
    },
    /**********************************************************************************************
    * @Author:      Sameer Jewalikar
    * @Date:        5/29/2019
    * @Description: This method will handle the creation of relationship record.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/    
    navigateToRelationshipRecord : function (component, event, helper, result) {
        var createRelationshipRecordEvt = $A.get("e.force:navigateToSObject");
        createRelationshipRecordEvt.setParams({
            "recordId": result.recordId,
            
        });
        createRelationshipRecordEvt.fire();        
    }    
    
    
})