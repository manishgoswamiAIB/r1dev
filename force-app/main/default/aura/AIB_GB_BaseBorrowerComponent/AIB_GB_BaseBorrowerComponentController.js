({
    doInit: function (component, event, helper) {
        helper.handleDoInit(component, event, helper);
    },

    handleOpenSearchSection: function (component, event, helper) {
        helper.handleOpenSearchSection(component, event, helper);
    }

})