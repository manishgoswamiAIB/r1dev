({

    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        20/05/2019
    * @Description: This is a method is used to get User Juridiction. 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    handleDoInit: function (component, event, helper) {
        helper.callServer(component, "c.getCurrentUserRegion",
            function (resultStr) {
                var result = JSON.parse(resultStr);

                if (result.hasError) {
                    console.log(result.errorMessage);
                }
                else {
                    component.set("v.userRegion", result.jurisdiction);
                }
            }
        );
    },
    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        20/05/2019
    * @Description: This is a method is used to Hide and show Entiry/customer/search LC. 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    handleOpenSearchSection: function (component, event, helper) {

        var selectedSection = event.getParam("selectedSection");

        // close other sections based on event param
        if (selectedSection === "SelectedCriteria") {
            component.set("v.selectedCriteria", true);
            component.set("v.entity", false);
            component.set("v.customer", false);
        }
        else if (selectedSection === "Customer") {
            component.set("v.customer", true);
            component.set("v.selectedCriteria", false);
            component.set("v.entity", false);
        }
        else if (selectedSection === "Entity") {
            component.set("v.entity", true);
            component.set("v.customer", false);
            component.set("v.selectedCriteria", false);
        }

    }
})