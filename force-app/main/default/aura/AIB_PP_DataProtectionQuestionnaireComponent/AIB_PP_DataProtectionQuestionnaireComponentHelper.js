({
    /**********************************************************************************************
   * @Author:     Arpita Ostwal
   * @Date:        23/05/2019
   * @Description: Fire the method to extract Questionnaire GroupName-Current User Region
   * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]
   ***********************************************************************************************/
    doInit: function(component, event, helper) {
        //method to get Jurisdiction
        component.set("v.message","");
        helper.callServer(component, "c.getCurrentUserRegion",
                          function (response) {
                              if (response !== null && response !== ''){
                                  component.set("v.questionnaireGroupName", response);
                                  helper.getQuestions(component, event, helper);
                              }
                              else {
                                  component.set("v.message", $A.get("$Label.c.AIB_PP_Error_JurisdictionNotSet"));    
                              }                              
                          });
    },
    /**********************************************************************************************
   * @Author:     Arpita Ostwal
   * @Date:        23/05/2019
   * @Description: Fire the method to display Questionnaire and options 
   * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]
   ***********************************************************************************************/
    getQuestions: function(component, event, helper) {
        //method to get Jurisdiction
        component.set("v.message","");
        helper.callServer(component, "c.getQuestionsAndOptions",
                          function (resultStr) {
                              if (resultStr !== null && resultStr !== "[]"){   
                                  var result = JSON.parse(resultStr);
                                  component.set("v.responseList", result);                              
                              }   
                              else{
                                  component.set("v.message", $A.get("$Label.c.AIB_PP_Error_NoQuestionsAvailable"));    
                              }
                          },
                          {
                              questionnaireGroupName: component.get("v.questionnaireGroupName")
                          }
                         );
    },
    /**********************************************************************************************
   * @Author:     Arpita Ostwal
   * @Date:        23/05/2019
   * @Description: Call this method on click of submit button for Questionnaire
   * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]
   ***********************************************************************************************/
    handleSubmit: function(component, event, helper) {
        var successMessage = $A.get("$Label.c.AIB_PP_Success_DataProtection");
        var errorMessage = $A.get("$Label.c.AIB_PP_Error_AnswerSubmit");
        var allValid = component
        .find("formInput")
        .reduce(function(validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && !inputCmp.get("v.validity").valueMissing;
        }, true);
        if (allValid) {
            var myInputs = component.find("formInput");
            var selectedValues = new Array();
            for (var j = 0; j < myInputs.length; j++) {
                selectedValues.push(myInputs[j].get("v.value"));
            }
            var params = {
                recordId: component.get("v.recordId"),
                questionnaireGroupName: component.get("v.questionnaireGroupName"),
                selectedValues: selectedValues
            };
            helper.callServer(component, "c.processAnswers",
                              function (response) {
                                  if (response !== ""){
                                      this.showToast(component, event, 'success', successMessage); 
                                      var navEvt = $A.get("e.force:navigateToSObject");
                                      navEvt.setParams({
                                          recordId: component.get("v.recordId")
                                      });
                                      navEvt.fire();
                                  }
                                  else {
                                      this.showToast(component, event, 'error', errorMessage);
                                  }     
                              },
                              params   
                             );
        }
    },
  /**********************************************************************************************
   * @Author:      Arpita Ostwal
   * @Date:        28/05/2019
   * @Description: Fire the toast message on Error
   * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]
   ***********************************************************************************************/   
    showToast : function(component, event, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "message": message
        });
        toastEvent.fire();
    }
    
});