({
   /**********************************************************************************************
    * @Author:      Sameer Jewalikar
    * @Date:        22/05/2019
    * @Description: This method is used to delete the customers from selected customers list.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    handleRecordDeletionHelper : function(component, event, helper) {
        var rowId = event.getSource().get('v.name');
        var recordRestored = [];
        var selectedRecList = component.get("v.selectedRecordList");
        for(var i=0;i<selectedRecList.length;i++) {
            if(selectedRecList[i].customerID==rowId) {
                recordRestored.push(selectedRecList[i]);             
                selectedRecList.splice(i,1);                
            }
        }        
        component.set('v.selectedRecordList',selectedRecList);
        
        
        var compEvent = component.getEvent("selectedCustomersDeleteEvent");
            compEvent.setParams({
                "restoredRecordList" : recordRestored,
            });
            compEvent.fire();
    }
})