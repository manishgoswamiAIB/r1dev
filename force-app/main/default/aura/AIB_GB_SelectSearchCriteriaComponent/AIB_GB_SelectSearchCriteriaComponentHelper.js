({
     /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        20/05/2019
    * @Description: This is a method is used to fire an Event when user click on Locate By Customer
    * 				Button.			 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
	customerSearchHelper : function(component, event, helper) {
		// Fire the event to open Customer Search Page
        var cmpEvent = component.getEvent("openSelectedSection");
        cmpEvent.setParams({
            "selectedSection" : "Customer" 
        })
        cmpEvent.fire();
	},
    
    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        20/05/2019
    * @Description: This is a method is used to fire an Event when user click on Locate By Entity
    * 				Button.			 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    entitySearchHelper: function(component, event, helper) {
		// Fire the event to open Entity Search Page
        var cmpEvent = component.getEvent("openSelectedSection");
        cmpEvent.setParams({
            "selectedSection" : "Entity" 
        })
        cmpEvent.fire();
	}
})