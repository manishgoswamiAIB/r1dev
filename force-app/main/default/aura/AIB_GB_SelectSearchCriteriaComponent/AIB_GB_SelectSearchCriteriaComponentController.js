({
	handleCustomerSearch : function(component, event, helper) {
		helper.customerSearchHelper(component, event, helper)
	},
    handleEntitySearch: function(component, event, helper) {
		helper.entitySearchHelper(component, event, helper)
	}
})