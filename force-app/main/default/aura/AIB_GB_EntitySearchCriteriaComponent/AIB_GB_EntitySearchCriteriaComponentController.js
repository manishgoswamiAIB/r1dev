({
	handleBack : function(component, event, helper) {
		helper.handleBack(component, event, helper);
	},
    handleReset:function(component, event, helper) {
        helper.handleReset(component, event, helper);
    },
    handleSearch:function(component, event, helper) {
        helper.handleSearch(component, event, helper);
    },
    handleEnableConfirmEvent :function(component, event, helper) {
        helper.enableConfirmEvent(component, event, helper);
    },
    handleErrorOnFocus :function(component, event, helper) {
        helper.errorOnFocus(component, event, helper);
    },
    handleConfirmBorrower :function(component, event, helper) {
        helper.confirmBorrowerHelper(component, event, helper);
    }
    
})