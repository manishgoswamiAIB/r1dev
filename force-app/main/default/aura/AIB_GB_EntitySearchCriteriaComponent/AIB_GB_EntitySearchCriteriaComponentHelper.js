({
    /**********************************************************************************************
   * @Author:      Manisha Yadav  
   * @Date:        21/05/2019
   * @Description: This method takes back to the Search Criteria Page
   * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
   ***********************************************************************************************/
    handleBack: function (component, event, helper) {
        var cmpEvent = component.getEvent("openSelectedSection");
        cmpEvent.setParams({
            "selectedSection": "SelectedCriteria"
        })
        cmpEvent.fire();
    },
    /**********************************************************************************************
    * @Author:      Manisha Yadav  
    * @Date:        21/05/2019
    * @Description: This method resets the Entity screen.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    handleReset: function (component, event, helper) {
        component.set("v.borrower", {
            'entityName': '',
            'accountNumber': '',
            'entityId': '',
            'coinsNumber': '',
            'coinsGroupLeader': '',
            'coinsTeamCode': ''
        });
        //Re-rendering the component to reset the erroneous fields
        component.set('v.isLoaded', false);
        component.set('v.isLoaded', true);
        component.set("v.showErrorMessage", false);
        //hide the table on reset
        component.set("v.isResult", false);
        component.set("v.responseList", []);
        component.set("v.isResult", true);
        component.set("v.errorFlag", false);
        component.set("v.isDisabled", true);
    },
    /**********************************************************************************************
    * @Author:     Swapnil Mohite
    * @Date:        22/05/2019
    * @Description: This method handles event for Enable/Disable for Confirm button
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    enableConfirmEvent: function (component, event, helper) {
        var isDisabled = event.getParam("isContinue");
        var selectedBorrowerRecords = event.getParam("selectedBorrowerRecords");
        component.set("v.isDisabled", isDisabled);
        component.set("v.selectedRecordsList", selectedBorrowerRecords);
    },
    /**********************************************************************************************
    * @Author:      Manisha Yadav  
    * @Date:        3/06/2019
    * @Description: This method is used to handle the validations.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    handleValidation: function (component, event, helper) {
        component.set("v.errorFlag", false);
        component.set("v.genericErrorMsg", false);
        component.set("v.responseList", []);
        component.set("v.isSpinner", true);
        component.set("v.showErrorMessage", false);
        var borrowerMap = component.get("v.borrower");
        //Validation for Empty Search
        var count = 0;
        var size = 0;
        var inputValuesCount = 0;
        var isError = component.get("v.showErrorMessage");
        var allValid = component.find('inputField').reduce(function (validFields, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validFields && inputCmp.get('v.validity').valid;
        }, true);
        for (var key in borrowerMap) {
            size++;
            if ($A.util.isEmpty(borrowerMap[key].trim())) {//If no input value is populated
                count++;
            }
            if (!$A.util.isEmpty(borrowerMap[key].trim())) {//Number of input values populated
                inputValuesCount++;
            }
        }
        if (count === size) {
            var emptySearchError = $A.get("$Label.c.AIB_Error_LocateByBorrowerEmptySearch");
            this.invalidInputError(component, event, emptySearchError);
        }
        else if (isError === true) {
            component.set("v.isSpinner", false);
            component.set("v.showErrorMessage", true);
        }
        else if (allValid === false) {
            //For not displaying result list if the input fields are errorneous
            this.invalidInputError(component, event, '');
            component.set("v.genericErrorMsg", true);
        }
        else if (inputValuesCount > 1) {
            var multipleParamError = $A.get("$Label.c.AIB_Error_LocateByBorrowerMultipleParameters");
            this.invalidInputError(component, event, multipleParamError);
        }
    },
    /**********************************************************************************************
    * @Author:      Manisha Yadav  
    * @Date:        21/05/2019
    * @Description: This method fetches the record of the borrower based on searched parameter.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    handleSearch: function (component, event, helper) {
        this.handleValidation(component, event, helper);
        if (!component.get("v.showErrorMessage")) {
            //i: IF any field is populated setting the parameters and Calling the Callserver Method
            this.getResponseFromServer(component, event, helper);
        }
    },
    /**********************************************************************************************
    * @Author:      Manisha Yadav  
    * @Date:        29/05/2019
    * @Description: This method displays toast message.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    showToast: function (component, event, message, type) {
        component.set("v.showErrorMessage", true);
        component.set("v.typeOfMessage", type);
        component.set("v.errorMessage", message);

    },
    /**********************************************************************************************
    * @Author:      Manisha Yadav  
    * @Date:        29/05/2019
    * @Description: This method sets component attributes.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    invalidInputError: function (component, event, message) {
        component.set("v.errorMessage", message);
        component.set("v.typeOfMessage", "error");
        component.set("v.isSpinner", false);
        component.set("v.showErrorMessage", true);
        component.set("v.isResult", false);
        component.set("v.isResult", true);
    },
    /**********************************************************************************************
    * @Author:      Manisha Yadav  
    * @Date:        29/05/2019
    * @Description: This method calls server to get the response.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
	***********************************************************************************************/
    getResponseFromServer: function (component, event, helper) {
        var borrowerMap = component.get("v.borrower");
        var params = {
            entityName: borrowerMap.entityName,
            entityId: borrowerMap.entityId,
            coinsNumber: borrowerMap.coinsNumber,
            coinsGroupLeader: borrowerMap.coinsGroupLeader,
            coinsTeamCode: borrowerMap.coinsTeamCode,
            accountNumber: borrowerMap.accountNumber,
            region: component.get("v.userRegion"),
            recordId: component.get("v.recordId"),
            integrationSettingName: $A.get(" $Label.c.AIB_GB_IntegrationSettingName")
        };
        helper.callServer(component, "c.getEntityResult",
            function (resultStr) {
                var result = JSON.parse(resultStr);
                if (result.hasWarning) {
                    this.handleWarning(component, event);
                } else if (result.hasError) {
                    this.handleError(component, event, result);
                } else if (result.hasInfo) {
                    this.handleInfo(component, event, result);
                }
            },
            { "entityParamStr": JSON.stringify(params) }
        );
    },
    /**********************************************************************************************
    * @Author:      Manisha Yadav  
    * @Date:        29/05/2019
    * @Description: This method handles warning message.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    handleWarning: function (component, event) {
        //Validation for no results returned
        component.set("v.isSpinner", false);
        var noResultsError = $A.get("$Label.c.AIB_Error_LocateByEntityNoResults");
        this.showToast(component, event, noResultsError, "Toast");

    },
    /**********************************************************************************************
    * @Author:      Manisha Yadav  
    * @Date:        29/05/2019
    * @Description: This method handles server side error.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    handleError: function (component, event, result) {
        // Server side error
        component.set("v.isSpinner", false);
        this.showToast(component, event, result.message, "Toast");
    },
    /**********************************************************************************************
    * @Author:      Manisha Yadav  
    * @Date:        29/05/2019O
    * @Description: This method sets actual response to component attribute.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    handleInfo: function (component, event, result) {
        var responseArray = result.entities;
        if (responseArray.length >= 80) { 
            component.set("v.isSpinner", false);
            var maxResultsError = $A.get("$Label.c.AIB_Error_GetBorrowerMaxResults");
            this.showToast(component, event, maxResultsError, "Toast");
        }
        else {
            component.set("v.showErrorMessage", false);
        }
        component.set("v.isResult", false);
        component.set("v.responseList", responseArray);
        component.set("v.isResult", true);
        component.set("v.isSpinner", false);
    },
    /**********************************************************************************************
    * @Author:      Manisha Yadav  
    * @Date:        30/05/2019
    * @Description: This method handles Error messages on focus of fields
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    errorOnFocus: function (component, event, helper) {
        component.set("v.showErrorMessage", false);
        component.set("v.errorFlag", false);
    },
    /**********************************************************************************************
    * @Author:      Manisha Yadav  
    * @Date:        6/06/2019
    * @Description: This method handles the confirmation of selected borrowers.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    confirmBorrowerHelper: function (component, event, helper) {
        component.set("v.isSpinner", true);
        var confirmedBorrowerList = component.get("v.selectedRecordsList");
        //var params = [];
        var selectedBorrowerId = [];
        for (var i = 0; i < confirmedBorrowerList.length; i++) {
            selectedBorrowerId.push(confirmedBorrowerList[i].entityId)
        }
        var ccvParam = selectedBorrowerId.toString();
        var params = {
            selectedBorrowerId: ccvParam,
            recordId: component.get("v.recordId"),
            integrationSettingName: $A.get("$Label.c.AIB_GB_EntityGetBorrowerSettingName"),
            region: component.get("v.userRegion"),
        }
        helper.callServer(component, "c.getBorrowerResult",
            function (resultStr) {
                var result = JSON.parse(resultStr);
                component.set("v.isSpinner", false);
                if (result.hasError) {
                    this.showToast(component, event, result.message, "Toast");
                } else if (result.hasWarning) {
                    this.showToast(component, event, result.message, "Toast");
                }
                else {
                    this.navigateToRelationshipRecord(component, event, helper, result);
                }
            },
            {
                "borrowerListParamStr": JSON.stringify(params)
            }
        );
    },
    /**********************************************************************************************
    * @Author:      Manisha Yadav  
    * @Date:        6/06/2019
    * @Description: This method creates a relationship record of the selected borrower.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    navigateToRelationshipRecord: function (component, event, helper, result) {
        $A.get('e.force:refreshView').fire();
        var createRelationshipRecordEvt = $A.get("e.force:navigateToSObject");
        createRelationshipRecordEvt.setParams({
            "recordId": result.recordId,
        });
        createRelationshipRecordEvt.fire();
    }
})