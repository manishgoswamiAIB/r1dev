({ /**********************************************************************************************
    * @Author:      Swapnil Mohite  
    * @Date:        22/05/2019
    * @Description: Init method to initialize attributes on page load.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    initHelper : function(component, event, helper,sortorder){
        var pageSize = component.get("v.pageSize");
        var pageNumber = component.get("v.pageNumber");
        var paginationList = [];
        this.handleSort(component, event, helper,'asc');
        var result = component.get("v.responseList");
        var totalSize = component.get("v.responseList").length;
        for(var i = 0; i< pageSize; i++){
            if(result.length>i){
                paginationList.push(result[i]);}
        }   
        if(totalSize <= pageSize){
            component.set("v.isNextdisabled", true);
        }
        component.set('v.paginationList', paginationList);
        component.set("v.totalSize", totalSize);     
        component.set("v.start",0);
        component.set("v.end",pageSize-1);
    },
    /**********************************************************************************************
    * @Author:      Swapnil Mohite  
    * @Date:        22/05/2019
    * @Description: This method is used to sort the response list.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    handleSort : function(component, event, helper,sortorder){
        var pgList = component.get("v.responseList");
        if(sortorder === 'asc'){
            var sortedList =  pgList.sort(function(a, b){
                if(a.entityName.toLowerCase() == b.entityName.toLowerCase()){
                    if(a.lastRefreshDate < b.lastRefreshDate) { return 1; }
                    if(a.lastRefreshDate > b.lastRefreshDate) { return -1; }
                    return 0; 
                }
                else { 
                    if(a.entityName.toLowerCase() < b.entityName.toLowerCase()) { return -1; }
                    if(a.entityName.toLowerCase() > b.entityName.toLowerCase()) { return 1; }
                    return 0; 
                }
                
            })
            component.set('v.paginationList',sortedList);
            component.set('v.isIconVisible',false);
        }
        else{
            var sortedList =  pgList.sort(function(a, b){
                if(a.entityName.toLowerCase() < b.entityName.toLowerCase()) { return 1; }
                if(a.entityName.toLowerCase() > b.entityName.toLowerCase()) { return -1; }
                return 0;  
            })
            component.set('v.responseList',sortedList);
            component.set('v.isIconVisible',true);
        }        
    },
    /**********************************************************************************************
    * @Author:      Swapnil Mohite  
    * @Date:        22/05/2019
    * @Description: this method is used to highlight the selected row.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    handleRowActionHelper : function(component, event, helper){
        var compEvent = component.getEvent("compEvent");
        var currentRowId = event.currentTarget.id;
        var selectedRowsArray = component.get('v.selectedRecords'); 
        var selectedId = '';
        var isContinue = true;
        var recordsArray = component.get("v.responseList");
        if(!$A.util.isEmpty(selectedRowsArray) && selectedRowsArray.length > 0){
            selectedId = selectedRowsArray[0].entityId;
            document.getElementById(selectedId).className = "";
            selectedRowsArray.pop();
            if(selectedId == currentRowId){
                isContinue = true;
            }
        }
        if(currentRowId !== selectedId){
            setTimeout(function(){
                document.getElementById(currentRowId).className="active";
            },100)
            for(var i=0;i<recordsArray.length;i++){
                if(recordsArray[i].entityId == currentRowId && !(selectedId == (recordsArray[i]))){  
                    selectedRowsArray.push(recordsArray[i]);
                    break;
                }  
            }
            isContinue = false;
        }// event to handle confirm button
        component.set('v.selectedRecords',selectedRowsArray);
        this.fireSelectedCustomersChangeEvent(component, event, helper, isContinue);
    },
    
    /**********************************************************************************************
    * @Author:      Swapnil Mohite  
    * @Date:        22/05/2019
    * @Description: This method is used to handle next page button functionlity in pagination.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    nextPageHelper : function(component, event, helper){    
        var pageNumber = component.get("v.pageNumber");
        var pgList = component.get("v.responseList");        
        var end = component.get("v.end");        
        var start = component.get("v.start");        
        var pageSize = component.get("v.pageSize");        
        var paginationList = [];
        var compEvent = component.getEvent("compEvent");
        for(var i=end+1; i<end+pageSize+1; i++){   
            if(pgList.length > i){       
                paginationList.push(pgList[i]);                     
            }
        }
        pageNumber=pageNumber+1;        
        start = start + pageSize;        
        end = end + pageSize;  
        if((pgList.length-1)<=end){
            component.set("v.isNextdisabled", true);
            component.set("v.isLastdisabled", true);
        }
        component.set("v.pageNumber",pageNumber);
        component.set("v.start",start);        
        component.set("v.end",end);        
        component.set('v.paginationList', paginationList);
        component.set("v.selectedRecords", []);
        compEvent.setParams({
            "isContinue" : true,
            "selectedBorrowerRecords" : component.get('v.selectedRecords')
            });
        compEvent.fire();// event to handle confirm button
        
    },
    /**********************************************************************************************
    * @Author:      Swapnil Mohite  
    * @Date:        22/05/2019
    * @Description: This method is used to handle previous page button functionlity in pagination.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    previousPageHelper : function(component, event, helper){
        var compEvent = component.getEvent("compEvent");
        var pageNumber = component.get("v.pageNumber");
        var pgList = component.get("v.responseList");        
        var end = component.get("v.end");       
        var start = component.get("v.start");        
        var pageSize = component.get("v.pageSize");        
        var paginationList = [];        
        for(var i= start-pageSize; i < start ; i++){                       
            if (i > -1){                
                paginationList.push(pgList[i]);                         
            }            
            else{                
                start++;                
            }            
        }
        pageNumber=pageNumber-1;        
        start = start - pageSize;        
        end = end - pageSize;
        component.set("v.pageNumber",pageNumber);
        component.set("v.start",start);        
        component.set("v.end",end);        
        component.set('v.paginationList', paginationList);
        component.set("v.selectedRecords", []);
        component.set("v.isNextdisabled", false);
        compEvent.setParams({
            "isContinue" : true,
            "selectedBorrowerRecords" : component.get('v.selectedRecords')
        });
        compEvent.fire();// event to handle confirm button
        
    },
    /**********************************************************************************************
    * @Author:      Manisha Yadav  
    * @Date:        6/06/2019
    * @Description: This method is used to fire the component event on selection of a borrower record.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
     fireSelectedCustomersChangeEvent : function(component, event, helper, isContinue) {
        var compEvent = component.getEvent("compEvent");
        compEvent.setParams({
            "isContinue" : isContinue,
            "selectedBorrowerRecords" : component.get('v.selectedRecords')
        });
        compEvent.fire();
    }
})