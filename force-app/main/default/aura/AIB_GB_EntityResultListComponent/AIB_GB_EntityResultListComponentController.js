({
    init : function(component, event, helper) {
         helper.initHelper(component,event,helper);
    },	
    handleRowAction : function (component, event, helper) {       
        helper.handleRowActionHelper(component, event, helper);       
    },
    sortRecords : function(component, event, helper) {
        var sortorder = event.getSource().get('v.name');
        helper.handleSort(component,event,helper,sortorder);       
    },
    nextPage : function(component, event, helper) {         
        helper.nextPageHelper(component,event,helper);       
    },
    
    previousPage : function(component, event, helper) {  
        helper.previousPageHelper(component,event,helper);
    },
    firstPage : function(component, event, helper) {  
        helper.firstPageHelper(component,event,helper);
    },
    lastPage : function(component, event, helper) {  
        helper.lastPageHelper(component,event,helper);
    }  
})