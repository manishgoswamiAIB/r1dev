({
    doInit : function(component, event, helper) {
        helper.doInitHelper(component, event, helper);    
    },	
    
    handleRowAction : function (component, event, helper) {       
        helper.handleRowActionHelper(component, event, helper);       
    },
    
    sortRecords : function(component, event, helper) {
        if(component.get("v.sortOrder") === "asc") {
            component.set("v.sortOrder","desc");
        }else{
            component.set("v.sortOrder","asc");
        }        
        helper.doInitHelper(component, event, helper);                       
    },
    
    handleDeletedCustomerEvent : function(component, event, helper){  
        helper.handleDeletedCustomerEventHelper(component,event,helper);   
    },
    
    goToNextPage : function(component, event, helper) {         
        helper.goToNextPageHelper(component,event,helper);       
    },
    
    goToPreviousPage : function(component, event, helper) {  
        helper.goToPreviousPageHelper(component,event,helper);
    },       
})