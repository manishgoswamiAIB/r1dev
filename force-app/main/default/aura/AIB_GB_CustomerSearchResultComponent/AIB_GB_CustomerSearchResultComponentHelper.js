({
    /**********************************************************************************************
    * @Author:      Sameer Jewalikar 
    * @Date:        22/05/2019
    * @Description: This method is initializes the customer result table.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    doInitHelper : function(component, event, helper){ 
        component.set("v.recordList",component.get("v.originalCustomers"));
        component.set("v.pageNumber",0);
        component.set("v.start",0);
        var pageSize = component.get("v.pageSize");  
        var pageNumber = component.get("v.pageNumber");               
        var totalSize = component.get("v.recordList").length;
       
       if(totalSize <= pageSize){
           component.set("v.isNextDisabled", true);
        } 
        
        //var dis = component.get("v.isNextDisabled");
        this.handleSort(component, event, helper,component.get("v.sortOrder"));
        var recList = component.get("v.recordList");
        var paginationList = [];
        
        for (var i = 0; i< pageSize; i++) {  
            if (recList.length>i) {
                paginationList.push(recList[i]); 
            }
        }        
        component.set("v.totalSize", component.get("v.recordList").length);         
		component.set("v.end",pageSize-1);
        component.set('v.paginationList', paginationList);              
    },
    /**********************************************************************************************
    * @Author:      Sameer Jewalikar  
    * @Date:        22/05/2019
    * @Description: This method sorts the result list.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
   handleSort : function(component, event, helper,sortorder){        
        var rectList = component.get("v.recordList");
        if (sortorder === "asc") {
            var sortedList =  rectList.sort(function(a, b){
                if(a.fullName.toLowerCase() < b.fullName.toLowerCase()) { return -1; }
                if(a.fullName.toLowerCase() > b.fullName.toLowerCase()) { return 1; }
                return 0;                
            })
            component.set('v.recordList',sortedList);
        }
        else {
            var sortedList =  rectList.sort(function(a, b){
                if(a.fullName.toLowerCase() < b.fullName.toLowerCase()) { return 1; }
                if(a.fullName.toLowerCase() > b.fullName.toLowerCase()) { return -1; }
                return 0;                
            })
            component.set('v.recordList',sortedList);
        }        
    },  
    /**********************************************************************************************
    * @Author:      Sameer Jewalikar  
    * @Date:        22/05/2019
    * @Description: This method handles the addition of selected customer.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    handleRowActionHelper : function(component, event, helper){    
        var selectedRowsArray = component.get('v.selectedRecords'); 
        var selectedFlag=false;
        var currentRowId = event.currentTarget.id;
        for (var i=0;i<selectedRowsArray.length;i++) {           
            if (selectedRowsArray[i].customerID === currentRowId) {    
                selectedFlag = true;
                break;
            }
        } 
        var pageSize = component.get("v.pageSize");   
        var recordsArray = component.get("v.recordList");        
        for (var i=0;i<recordsArray.length;i++) {           
            if ((recordsArray[i].customerID === currentRowId) && (selectedFlag === false)) {                
                selectedRowsArray.push(recordsArray[i]);
               
            }
            
            if(recordsArray[i].customerID === currentRowId){
                recordsArray.splice(i,1);
               
            }
        } 
        component.set('v.selectedRecords',selectedRowsArray);
        component.set("v.recordList",recordsArray);
        this.addRecordsToPaginationHelper(component, event, helper);              
        this.fireSelectedCustomersChangeEvent(component, event, helper);        
    },
    /**********************************************************************************************
    * @Author:      Sameer Jewalikar  
    * @Date:        22/05/2019
    * @Description: This method handles the component event fired.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    handleDeletedCustomerEventHelper : function(component, event, helper){         
        var recordsArray = component.get("v.recordList"); 
        var confirmButtonDisabledIndicator = event.getParam("confirmButtonDisabled");
        var restoredRecordList = event.getParam("restoredRecordList");
        var originalCustomers = component.get("v.originalCustomers");           
        
        for (var i=0; i<originalCustomers.length; i++) {
            if (originalCustomers[i].customerID === restoredRecordList[0].customerID ) {                
                recordsArray.push(originalCustomers[i]);
            }
        }                
        component.set('v.recordList',recordsArray);
        this.handleSort(component, event, helper,component.get("v.sortOrder")); 
        this.addRecordsToPaginationHelper(component, event, helper);
        this.fireSelectedCustomersChangeEvent(component, event, helper);
    },
    /**********************************************************************************************
    * @Author:     	Sameer Jewalikar 
    * @Date:        22/05/2019
    * @Description: This method navigates to the next page in result table.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    goToNextPageHelper : function (component, event, helper) {      
        var pageNumber = component.get("v.pageNumber");
        var end = component.get("v.end");        
        var start = component.get("v.start");        
        var pageSize = component.get("v.pageSize");        
        var paginationList = [];        
        var counter = 0;          
        var pgList = component.get("v.recordList");        
        
        for (var i=end+1; i<end+pageSize+1; i++) {            
            if (pgList.length > i) {                        
                paginationList.push(pgList[i]);                
                counter ++ ;                
            }            
        }
        pageNumber = pageNumber+1;        
        start = start + pageSize;        
        end = end + pageSize;  
        if ((pgList.length-1)<=end) {
            component.set("v.isNextDisabled", true);            
        }
        component.set("v.pageNumber",pageNumber);
        component.set("v.start",start);        
        component.set("v.end",end);            
        component.set('v.paginationList', paginationList);
        
    },
    /**********************************************************************************************
    * @Author:     	Sameer Jewalikar
    * @Date:        22/05/2019
    * @Description: This method navigates to the previous page in result table.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    goToPreviousPageHelper : function (component, event, helper) {       
        component.set("v.isNextDisabled", false);
        var pageNumber = component.get("v.pageNumber");                  
        var end = component.get("v.end");       
        var start = component.get("v.start");        
        var pageSize = component.get("v.pageSize");     
        var pgList = component.get("v.recordList");  
        var paginationList = [];        
        var counter = 0;    
      
        for (var i=start-pageSize;i<start;i++) {            
            if (i > -1) {                
                paginationList.push(pgList[i]);                
                counter ++;                
            }            
            else {                
                start++;                
            }            
        }
        var listSize = component.get("v.recordList").length;
        pageNumber=pageNumber-1;        
        start = start - pageSize;         
        end = end - pageSize;                
        component.set("v.pageNumber",pageNumber);
        component.set("v.start",start);        
        component.set("v.end",end);           
        component.set('v.paginationList', paginationList);
        component.set("v.isNextDisabled", false); 
        if(pgList.length <= pageSize){
             component.set("v.isNextDisabled", true);
        }
        
    },     
    /**********************************************************************************************
    * @Author:      Sameer Jewalikar
    * @Date:        22/05/2019
    * @Description: This method is used to refresh the pagination list after selection of particular
    *               customer.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    addRecordsToPaginationHelper : function (component, event, helper) { 
        
        var pageNumber = component.get("v.pageNumber");        
        
        var end = component.get("v.end");       
        var start = component.get("v.start");        
        var pageSize = component.get("v.pageSize");     
        var pgList = component.get("v.recordList");  
        var paginationList = [];        
        for (var i=start; i<=end; i++) {            
            if (pgList.length > i) {                 
                paginationList.push(pgList[i]);                
            }            
        }            
        component.set('v.paginationList', paginationList); 
        if (pgList.length > pageSize) {
            component.set("v.isNextDisabled", false);
        }
        if (paginationList.length == 0) {
            this.goToPreviousPageHelper(component, event, helper);
        }        
    },
    /**********************************************************************************************
    * @Author:      Sameer Jewalikar
    * @Date:        22/05/2019
    * @Description: This method is used fire component event which enables/disables Confirm Button.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    fireSelectedCustomersChangeEvent : function(component, event, helper) {
        var noRecords = false;
        if(component.get('v.selectedRecords').length === 0){
            noRecords = true;
        }        
        var compEvent = component.getEvent("selectedCustomersChangeEvent");
        compEvent.setParams({
            "confirmButtonDisabled" : noRecords,
            "selectedCustomerRecords" : component.get('v.selectedRecords')
        });
        compEvent.fire();
    }
})