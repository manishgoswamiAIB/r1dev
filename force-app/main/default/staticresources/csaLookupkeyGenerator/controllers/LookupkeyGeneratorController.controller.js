(function() {
    'use strict';

    angular.module('csaLookupkeyGenerator').controller(
      'LookupkeyGeneratorController', LookupkeyGeneratorController
    );

    LookupkeyGeneratorController.$inject = [
        'config', 'label', 'remotingManager', 'messageService'
    ];

    function LookupkeyGeneratorController (
        config, label, remotingManager, messageService
    ) {
        var vm = this;

        vm.config = config.get();
        vm.labels = label.getLabels();

        vm.objectList = {};
        vm.selectedObject = '';
        vm.recordList = [];

        vm.getRecords = getRecords;
        vm.checkForDuplicates = checkForDuplicates;
        vm.buildKeys = buildKeys;

        vm.$onInit = function() {
            getObjectList();
            setTimeout(function() {
                jQuery('#object-list').trigger('chosen:updated');
            }, 3000);
        };

        function getObjectList() {
            remotingManager.invokeAction(
                vm.config.actions.invoke,
                vm.config.actions.getObjectList,
                {},
                function(result, event) {
                    if (event.status) {
                        vm.objectList = result;
                    } else {
                        messageService.setMessage({
                            type: messageService.messageTypes.error,
                            text: event.message
                        });
                    }
                }, {
                    escape: false
                }
            );
        }

        function getRecords() {
            remotingManager.invokeAction(
                vm.config.actions.invoke,
                vm.config.actions.getRecords,
                { 'objName' : vm.selectedObject },
                function(result, event) {
                    if (event.status) {
                        vm.recordList = result;
                        if(result.length == 0) {
                            messageService.setMessage({
                                type: messageService.messageTypes.info,
                                text: vm.labels.success.noRecords
                            });
                        }
                    } else {
                        messageService.setMessage({
                            type: messageService.messageTypes.error,
                            text: event.message
                        });
                    }
                }, {
                    escape: false
                }
            );
        }

        function checkForDuplicates() {
            remotingManager.invokeAction(
                vm.config.actions.invoke,
                vm.config.actions.checkForDuplicates,
                { 'objName' : vm.selectedObject },
                function(result, event) {
                    if (event.status) {
                        vm.recordList = result;
                        if(result.length == 0) {
                            messageService.setMessage({
                                type: messageService.messageTypes.success,
                                text: vm.labels.success.noDupes
                            });
                        } else {
                            messageService.setMessage({
                                type: messageService.messageTypes.warning,
                                text: vm.labels.warnings.dupesFound
                            });
                        }
                    } else {
                        messageService.setMessage({
                            type: messageService.messageTypes.error,
                            text: event.message
                        });
                    }
                }, {
                    escape: false
                }
            );
        }

        function buildKeys() {
            var recordIds = [];

            vm.recordList.forEach(function(record) {
                recordIds.push(record.Id);
            });
            
            remotingManager.invokeAction(
                vm.config.actions.invoke,
                vm.config.actions.buildKeys,
                {
                    'objName' : vm.selectedObject,
                    'recordIds' : JSON.stringify(recordIds)
                },
                function(result, event) {
                    if (event.status) {
                        messageService.setMessage({
                            type: messageService.messageTypes.success,
                            text: vm.labels.success.update
                        });
                        vm.getRecords();
                    } else {
                        messageService.setMessage({
                            type: messageService.messageTypes.error,
                            text: event.message
                        });
                    }
                }, {
                    escape: false
                }
            );
        }
    }
})();
