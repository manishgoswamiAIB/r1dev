(function() {
    'use strict';

    angular.module('csaLookupkeyGeneratorInstaller').controller(
      'LookupkeyGeneratorInstallerController', LookupkeyGeneratorInstallerController
    );

    LookupkeyGeneratorInstallerController.$inject = [
        'config', 'label', 'remotingManager', 'messageService'
    ];

    function LookupkeyGeneratorInstallerController (
        config, label, remotingManager, messageService
    ) {
        var vm = this;

        vm.config = config.get();
        vm.labels = label.getLabels();

        vm.isInstalled = false;

        vm.install = install;
        vm.uninstall = uninstall;

        vm.$onInit = function() {
            checkInstall();
        };

        function checkInstall() {
            remotingManager.invokeAction(
                vm.config.actions.invoke,
                vm.config.actions.check,
                {},
                function(result, event) {
                    if (event.status) {
                        vm.isInstalled = result;
                    } else {
                        messageService.setMessage({
                            type: messageService.messageTypes.error,
                            text: event.message
                        });
                    }
                }, {
                    escape: false
                }
            );
        }

        function install() {
            remotingManager.invokeAction(
                vm.config.actions.invoke,
                vm.config.actions.install,
                {},
                function(result, event) {
                    if (event.status) {
                        vm.isInstalled = true;
                        messageService.setMessage({
                            type: messageService.messageTypes.success,
                            text: vm.labels.success.install
                        });
                    } else {
                        messageService.setMessage({
                            type: messageService.messageTypes.error,
                            text: event.message
                        });
                    }
                }, {
                    escape: false
                }
            );
        }

        function uninstall() {
            remotingManager.invokeAction(
                vm.config.actions.invoke,
                vm.config.actions.uninstall,
                {},
                function(result, event) {
                    if (event.status) {
                        vm.isInstalled = false;
                        messageService.setMessage({
                            type: messageService.messageTypes.success,
                            text: vm.labels.success.uninstall
                        });
                    } else {
                        messageService.setMessage({
                            type: messageService.messageTypes.error,
                            text: event.message
                        });
                    }
                }, {
                    escape: false
                }
            );
        }
  }
})();
