/***************************************************************************************************
* @Author:    	Arpita Ostwal
* @Date:        23/05/2018
* @Description: FR1: Base Controller for Data Protection Quetionnarie
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
public with sharing class AIB_PP_DataProtectionQuesController {
    
    /**************************************************************************************************
* @Author:    	Arpita Ostwal
* @Date:        23/05/2018
* @Description: Method to get Current User Region 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
    @AuraEnabled
    public static String getCurrentUserRegion(){
        String questGroupName ='';
        try {
            String userJurisdiction = AIB_Util.getCurrentUserRegion();
            if(!String.isBlank(userJurisdiction)){
                if(userJurisdiction == AIB_Constants.ROI){
                    questGroupName = AIB_Constants.DATA_PROTECTION_ROI;
                }else if(userJurisdiction == AIB_Constants.FT){
                    questGroupName = AIB_Constants.DATA_PROTECTION_FT;
                }  
                else{
                    questGroupName = AIB_Constants.DATA_PROTECTION_GB;
                }
            }
        } 
        catch(Exception exp){
            questGroupName ='';
            AIB_GenericLogger.log(AIB_PP_DataProtectionQuesController.class.getName(),
                                  'getCurrentUserRegion', 
                                  exp);
        }
        return questGroupName;
 }
    /**************************************************************************************************
* @Author:      Arpita Ostwal  
* @Date:        23/05/2018
* @Description: This is a method to get Quetions and Options. 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
    @AuraEnabled
    public static String getQuestionsAndOptions(String questionnaireGroupName){
        List<responseObject> response = new List<responseObject>();
        responseObject responseObj;
        optionsObject optionObj;
        optionsObject defaultOption;
        List<optionsObject> ooList;
        try{
            nFORCE__Question__c[] questions = 
                AIB_PP_DataProtectionQuesProvider.queryQuestionByGroupName(questionnaireGroupName);  
            if	(questions != null 
                 && questions.size() > 0 
                ) {
                    for(nFORCE__Question__c question : questions){
                        responseObj = new responseObject();
                        responseObj.questionId = question.id;
                        responseObj.formtype = question.nFORCE__Form_Type__c;
                        responseObj.textValue = question.AIB_ReadOnlyText__c;
                        responseObj.picklistOptions = null;
                        
                        if(question.nFORCE__Options__r.size() > 0){
                            ooList = new List<optionsObject>();
                            defaultOption = new optionsObject();
                            defaultOption.label = AIB_Constants.DATA_PROTECTION_SELECT_DEFAULT;
                            defaultOption.value = null;
                            ooList.add(defaultOption);
                            
                            for(nFORCE__Option__c option : question.nFORCE__Options__r){
                                optionObj = new optionsObject();
                                optionObj.label = option.nFORCE__Label__c;
                                optionObj.value = option.id;
                                ooList.add(optionObj);
                            }
                            responseObj.picklistOptions = ooList;
                        }
                        response.add(responseObj);
                    }
                }
            else{
       				response = new List<responseObject>();       
            }
        }
        catch(Exception exp) {
            AIB_GenericLogger.log(AIB_PP_DataProtectionQuesController.class.getName(), 'makeCallout', exp);
        }
       
		     
     	return JSON.serialize(response);
     	
     
       
    }
    /***************************************************************************************************
* @Author:      Arpita Ostwal  
* @Date:        23/05/2018
* @Description: This is a method to Process the answer. 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
****************************************************************************************************/
    
    @AuraEnabled
    public static String processAnswers(String recordId, String questionnaireGroupName, List<String> selectedValues){
        // Populate map from SOQL query
        String completionStatus ='';
        nFORCE__Answer__c answer;
        List<nFORCE__Answer__c> answersToInsert = new List<nFORCE__Answer__c>();
      try{
        Map<ID, nFORCE__Option__c> mapOptions =
            AIB_PP_DataProtectionQuesProvider.queryOptions(questionnaireGroupName);
        LLC_BI__Product_package__c applicationObject = new LLC_BI__Product_Package__c(
            			id=recordId,AIB_DataProtectionQuestionsComplete__c = system.today());
        for (String selectvalue: selectedValues) {
            nFORCE__Option__c options = mapOptions.get(selectvalue);
            answer = new nFORCE__Answer__c();
            answer.nFORCE__Option__c = selectvalue;
            answer.nFORCE__Context_Id__c = recordId;
            answer.AIB_Application__c = recordId;
            answer.nFORCE__Question__c = options.nFORCE__Question__c;
            answer.nFORCE__Group__c = options.nFORCE__Question__r.nFORCE__Group__c;
            answersToInsert.add(answer);	
        }
        Database.insert(answersToInsert);
        Database.update(applicationObject);
         completionStatus =  AIB_Constants.DATA_PROTECTION_COMPLETED;
      	}
      catch(Exception exp) {
          completionStatus = '';
          AIB_GenericLogger.log(AIB_PP_DataProtectionQuesController.class.getName(), 'makeCallout', exp);   
          
        }
     return completionStatus;
    
    }
    /***************************************************************************************************
* @Author:      Arpita Ostwal  
* @Date:        23/05/2018
* @Description: This is a method to get response. 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
****************************************************************************************************/
    
    
    //Object definition used to compile for pushing to JS
    public with sharing  class responseObject{
        
        public String questionId {get; set;}
        public String formtype {get; set;} //'Read Only Text', 'Read Only Header', 'Picklist'
        public String textValue {get; set;} //Text value of the read only fields
        public List<optionsObject> picklistOptions {get; set;}// List of optionsObjects that contain the label (nFORCE__Option__c.nFORCE__Label__c) and value (nFORCE__Option__c.id)
        
    }
    /***************************************************************************************************
* @Author:      Arpita Ostwal  
* @Date:        23/05/2018
* @Description: This is a method to get Options Object.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
****************************************************************************************************/
    public with sharing class optionsObject{ 
        public String label {get; set;}
        public String value {get; set;}
        
    }
}