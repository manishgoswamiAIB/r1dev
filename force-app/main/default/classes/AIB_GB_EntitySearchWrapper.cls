/**************************************************************************************************
* @Author:      Swapnil Mohite  
* @Date:        20/05/2019
* @PageCode:    EW
* @Description: This is a Entity search wrapper class to be used for Lightning Component 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
public without sharing class AIB_GB_EntitySearchWrapper extends AIB_BaseWrapper {
    
    /**********************************************************************************************
* @Author:      Swapnil Mohite  
* @Date:        20/05/2019
* @Description: This is an Entity search inner wrapper class to set parameters 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/
    public class Parameter extends AIB_BaseWrapper.Parameter {
        public String entityName {get; set;}
        public String accountNumber {get; set;}
        public String entityId {get; set;}
        public String coinsNumber{get; set;}
        public String coinsGroupLeader{get; set;}
        public String coinsTeamCode {get; set;}
        public String region {get; set;}
        public String integrationSettingName {get; set;}
        public String selectedBorrowerId {get; set;}

    }
    
    /**********************************************************************************************
* @Author:      Swapnil Mohite
* @Date:        20/05/2019
* @Description: This is an Entity search inner wrapper class to store results. 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/
    public class Result extends AIB_BaseWrapper.Result {
        public List<AIB_GB_EntitiesResponseWrapper.creditCustomerList> entities {get; set;} 
    }
    
    
    
}