/**************************************************************************************************
* @Author:      Seb Merritt  
* @Date:        12/07/2017
* @Description: FR1: Base wrapper to be extended for Lightning Components
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
global virtual class AIB_BaseWrapper {

    public Parameter parameter {get; set;}
    public Result result {get; set;}

    /**********************************************************************************************
    * @Author:      Seb Merritt  
    * @Date:        12/07/2017
    * @Description: FR1: Constructor to create inner classes
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public AIB_BaseWrapper() {
        this.parameter = new Parameter();
        this.result = new Result();
    }

    /**********************************************************************************************
    * @Author:      Seb Merritt  
    * @Date:        12/07/2017
    * @Description: FR1: This is to be used for passing parameters from Lightning components to the AEPX
    *               controller
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    global virtual class Parameter {
        public String recordId {get; set;}
    }

    /**********************************************************************************************
    * @Author:      Seb Merritt  
    * @Date:        25/01/2019
    * @Description: FR1: This is to be used to store the results of an action
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    global virtual class Result {
        public Boolean hasError {get; set;} //there has been exception, show message
        public Boolean hasWarning {get; set;} //confirmation required, show message
        public Boolean hasInfo {get; set;} //success, show message
        public String message {get; set;} //the user facing message
        public String code {get; set;} //error code to aid debugging of issues
        public String recordId {get; set;} //record id effected by an action

        /******************************************************************************************
        * @Author:      Seb Merritt  
        * @Date:        25/01/2019
        * @Description: FR1: set boolean flags on construction
        * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]  ///
        *******************************************************************************************/
        public Result() {
            this.hasError = false;
            this.hasWarning = false;
            this.hasInfo = false;
        }
    }
}