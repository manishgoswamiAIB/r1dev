/**************************************************************************************************
* @Author:      Amol Patil 
* @Date:        20/05/2019
* @Description: This class is used to for Customer search.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
public with sharing class AIB_GB_CustomerSearchController {
    
    /**********************************************************************************************
    * @Author:      Amol Patil
    * @Date:        20/05/2019
    * @Description: This method is used to get Customer result 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    @AuraEnabled
    public static String getCustomerResult(String customerParamStr) {
        AIB_GB_CustomerSearchWrapper.Result result = new AIB_GB_CustomerSearchWrapper.Result();   
        
        AIB_GB_CustomerSearchWrapper.Parameter param = deserializeParameter(customerParamStr);
        result = AIB_GB_CustomerSearchHelper.getCustomerResponse(param);
        
        if (result.customersResponse != null && result.customersResponse.size() > 0) {
            AIB_GB_CustomerSearchHelper.updateFullAddress(result.customersResponse);
        }  
        
        return json.serialize(result);
    }
    
    /**********************************************************************************************
    * @Author:      Amol Patil
    * @Date:        20/05/2019
    * @Description: This method will return the deserialized input parameters. 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/    
    private static AIB_GB_CustomerSearchWrapper.Parameter deserializeParameter(String customerParamStr) {
        try {                        
            AIB_GB_CustomerSearchWrapper.Parameter customerParam = (AIB_GB_CustomerSearchWrapper.Parameter) JSON.deserialize(
                customerParamStr, 
                AIB_GB_CustomerSearchWrapper.Parameter.class
            );
            return customerParam;
        }
        catch (Exception exp) {
            AIB_GenericLogger.log(AIB_GB_CustomerSearchController.class.getName(),AIB_Constants.CONSTANT_DESERIALIZE_PARAM_METHOD,exp);
            return null;
        }        
    }
    
    /**********************************************************************************************
    * @Author:      Amol Patil
    * @Date:        20/05/2019
    * @Description: This method will return the dropdown values from custom metadata. 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
	***********************************************************************************************/
    @AuraEnabled
    public static String getPicklistValues() { 
        
        Map<String,List<AIB_DropdownSetting_CMD__mdt>> dropdownsMap 
            = new Map<String,List<AIB_DropdownSetting_CMD__mdt>>();
        dropdownsMap = AIB_GB_CustomerSearchHelper.populatePicklistValues();
        return JSON.serialize(dropdownsMap);
    }
    
    
    /**********************************************************************************************
    * @Author:      Sameer Jewalikar
    * @Date:        20/05/2019
    * @Description: This method will make callout to external system and create Household 
    * 				and its related records in nCino. 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
	***********************************************************************************************/    
    @AuraEnabled
    public static String getBorrowerResult(String customerListParamStr) {
        AIB_GB_CustomerSearchWrapper.Result result = new AIB_GB_CustomerSearchWrapper.Result();   
       
        AIB_GB_CustomerSearchWrapper.Parameter param = deserializeParameter(customerListParamStr);
        
        result = AIB_GB_CustomerSearchHelper.getBorrowerResponse(param);
                
        return json.serialize(result);
    }

}