/**************************************************************************************************
* @Author:      Manish Goswami
* @Date:        31/05/2019
* @Description: This is a test class for AIB_GB_EntitySearchController class.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
@isTest(SeeAllData=false)
public class AIB_UtilProvider_Test {
   @testSetup 
    /**************************************************************************************************
* @Author:      Manish Goswami
* @Date:        31/05/2019
* @Description: This method is used to setup data for test class
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
    private static void setup(){ 
    

    }
    private static testmethod void getmetadatarecordsTest() {
        AIB_JsonKeywords__mdt[] supportSetting = [
            SELECT AIB_Keyword__c, 
            	AIB_Replacement__c 
            FROM AIB_JsonKeywords__mdt 
            LIMIT 1
        ]; 
        
        Test.startTest();
        AIB_JsonKeywords__mdt[] keyword = AIB_UtilProvider.getmetadatarecords();
        System.assertEquals(supportSetting[0], keyword[0]);
        Test.stopTest();
        
    }
}