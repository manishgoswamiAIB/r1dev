/**************************************************************************************************
* @Author:      Manish Goswami  
* @Date:        20/05/2019
* @PageCode:    EXP
* @CurErrCode:  EXP-003
* @Description: This is Util class used in helper controllers. 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
public with sharing class AIB_Util {
      
    /**********************************************************************************************
    * @Author:     Manish Goswami   
    * @Date:        20/05/2019
    * @Description: This method will return juridiction of the user. 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static String getCurrentUserRegion(){        
        String userJurisdiction = '';
        User[] users = AIB_UtilProvider.getCurrentUserDetails();
        if (users != null 
            && users.size() > 0
           ) {
               userJurisdiction = users[0].AIB_Jurisdiction__c;
           }
        return userJurisdiction;  
    }
 
       
    /**********************************************************************************************
    * @Author:     Manish Goswami  
    * @Date:        30/05/2019
    * @Description: This method will replace resrved key words from JSON String
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static String replaceReservedKeywords(String JSONString){
        if(String.isNotBlank(JSONString)){
            AIB_JsonKeywords_CMD__mdt[] keywordsList = AIB_UtilProvider.getMetadataRecords();
            for(AIB_JsonKeywords_CMD__mdt keywordRec : keywordsList){
                JSONString = JSONString.replace(
                    keywordRec.AIB_Keyword__c, 
                    keywordRec.AIB_Replacement__c
                );
                
            }
        }
        return JSONString;
    } 
}