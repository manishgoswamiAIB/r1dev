/**************************************************************************************************
* @Author:      Swapnil Mohite  
* @Date:        20/05/2019
* @Description: This class is used as helper class for AIB_GB_EntitySearchController.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
public with sharing class AIB_GB_EntitySearchHelper {
    
   /**********************************************************************************************
    * @Author:      Swapnil Mohite  
    * @Date:        20/05/2019
    * @Description: This method will return the repsone for the callout. 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static AIB_GB_EntitySearchWrapper.Result getEntityResponse(AIB_GB_EntitySearchWrapper.Parameter paramJs){
        
        AIB_GB_EntitySearchWrapper.Result result = new AIB_GB_EntitySearchWrapper.Result();
        AIB_CalloutWrapper.Parameter param = new AIB_CalloutWrapper.Parameter();
        try{
            String urlPar = generateUrl(paramJs);
            param.integrationSettingName =  paramJs.integrationSettingName;
            param.urlSearchParams = urlPar;
            param.userRegion = paramJs.region;
            AIB_CalloutWrapper.Result calloutResult = AIB_CalloutService.makeCallout(param);
            if (calloutResult.response.getStatusCode() == AIB_Constants.CALLOUT_STATUS_CODE_SUCCESS) {
                result.hasInfo = true;
                AIB_GB_EntitiesResponseWrapper entitiesResponse = 
                    (AIB_GB_EntitiesResponseWrapper)JSON.deserialize(
                        AIB_Util.replaceReservedKeywords(calloutResult.response.getBody()), 
                        AIB_GB_EntitiesResponseWrapper.class);
                result.entities = entitiesResponse.x_embedded.CreditCustomerList;
            }
            else if (calloutResult.response.getStatusCode() ==
                     AIB_Constants.CALLOUT_STATUS_CODE_CUSTOMERNOTFOUND) { 
                result.hasWarning = true;
                result.message = Label.AIB_GB_Error_EntityNotFound;
                result.code = String.valueOf(calloutResult.response.getStatusCode());
                AIB_GenericLogger.log(result.message);
            }
            else { 
                result.hasError = true;
                result.message = Label.AIB_Error_CalloutExceptionOccurred;
                if (calloutResult.response != null 
                    && String.isNotBlank(calloutResult.response.getBody())
                   ){
                       
                       AIB_GB_ErrorResponseWrapper errorResponse = 
                           (AIB_GB_ErrorResponseWrapper)JSON.deserialize(
                               AIB_Util.replaceReservedKeywords(calloutResult.response.getBody()), 
                               AIB_GB_ErrorResponseWrapper.class);
                       if (String.isNotBlank(errorResponse.message)) {
                           result.message = errorResponse.message;                       
                       }
                       result.code = String.valueOf(calloutResult.response.getStatusCode());
                   }
                
                AIB_GenericLogger.log(result.message);
            } 
            return result;
        }
        catch(Exception exp){
            result.hasError = true;
            result.message = Label.AIB_Error_CalloutExceptionOccurred;
            result.code = AIB_Constants.CONSTANT_CODE ;
            AIB_GenericLogger.log(AIB_GB_EntitySearchHelper.class.getName(), AIB_Constants.CONSTANT_METHOD_NAME, exp);
            return result;
        }
    }
    /**********************************************************************************************
    * @Author:      Swapnil Mohite  
    * @Date:        20/05/2019
    * @Description: This method will return the generated URL 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static String generateUrl(AIB_GB_EntitySearchWrapper.Parameter paramJs){
        AIB_GB_EntitySearchWrapper.Result result = new AIB_GB_EntitySearchWrapper.Result();
        List<String> urlParams = new List<String>();
        try{  
            if(String.isNotEmpty(paramJs.entityName)){
                urlParams.add(AIB_Constants.CONSTANT_ENTITY_NAME+paramJs.entityName);
            }if(String.isNotEmpty(paramJs.accountNumber)){
                urlParams.add(AIB_Constants.CONSTANT_ACC_NUMBER+paramJs.accountNumber);
            }if(String.isNotEmpty(paramJs.entityId)){
                urlParams.add(AIB_Constants.CONSTANT_ENTITY_ID+paramJs.entityId);
            }
            if(String.isNotEmpty(paramJs.coinsNumber)){
                urlParams.add(AIB_Constants.CONSTANT_COINS_NUMBER+paramJs.coinsNumber);
            }
            if(String.isNotEmpty(paramJs.coinsGroupLeader)){
                urlParams.add(AIB_Constants.CONSTANT_COINS_GRP_LEADER+paramJs.coinsGroupLeader);
            }
            if(String.isNotEmpty(paramJs.coinsTeamCode)){
                urlParams.add(AIB_Constants.CONSTANT_COINS_TEAM_CODE+paramJs.coinsTeamCode);
            }
            
            
        }
        catch(Exception exp){
            result.hasError = true;
            result.message = Label.AIB_Error_CalloutExceptionOccurred;
            result.code = 'EC-001';
            AIB_GenericLogger.log(AIB_CalloutService.class.getName(), 'handleResponse', exp);
        }
        String urlPar = String.join(urlParams,'&');
        return urlPar;
    }
    /**********************************************************************************************
    * @Author:      Manisha Yadav  
    * @Date:        6/06/2019
    * @Description: This method will return the repsone for the callout.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/    
    public static AIB_GB_EntitySearchWrapper.Result getEntityBorrowerResponse(AIB_GB_EntitySearchWrapper.Parameter paramJs){
        
        AIB_GB_EntitySearchWrapper.Result result = new AIB_GB_EntitySearchWrapper.Result();
        AIB_CalloutWrapper.Parameter param = new AIB_CalloutWrapper.Parameter();
        String recordId = '';
        try{
            param.integrationSettingName =  paramJs.integrationSettingName;
            param.urlSearchParams = paramJs.selectedBorrowerId;
            param.userRegion = paramJs.region;
            param.recordId = paramJs.recordId;
            AIB_CalloutWrapper.Result calloutResult = AIB_CalloutService.makeCallout(param);
            if (calloutResult.response.getStatusCode() == AIB_Constants.CALLOUT_STATUS_CODE_SUCCESS) {
                AIB_CalloutWrapper.Result confirmResult = 
                    AIB_GB_EntitySearchLogic.createRelationshipRecords(
                        param, 
                        calloutResult.response.getbody()
                    );
                result.hasInfo = confirmResult.hasInfo;
                result.hasWarning = confirmResult.hasWarning;
                result.hasError = confirmResult.hasError;
                result.recordId = confirmResult.recordId;
                result.message = confirmResult.message;             
            }
            else { 
                result.hasError = true;
                result.message = Label.AIB_Error_CalloutExceptionOccurred;
                if (calloutResult.response != null 
                    && String.isNotBlank(calloutResult.response.getBody())
                   ){
                       
                       AIB_GB_ErrorResponseWrapper errorResponse = 
                           (AIB_GB_ErrorResponseWrapper)JSON.deserialize(
                               AIB_Util.replaceReservedKeywords(calloutResult.response.getBody()), 
                               AIB_GB_ErrorResponseWrapper.class);
                       if (String.isNotBlank(errorResponse.message)) {
                           result.message = errorResponse.message;                       
                       }
                       result.code = String.valueOf(calloutResult.response.getStatusCode());
                   }
                
                AIB_GenericLogger.log(result.message);
            }
            return result;
        }
        catch(Exception exp){
            result.hasError = true;
            result.message = Label.AIB_Error_CalloutExceptionOccurred;
            result.code = AIB_Constants.CONSTANT_CODE ;
            AIB_GenericLogger.log(AIB_GB_EntitySearchHelper.class.getName(), AIB_Constants.CONSTANT_METHOD_NAME, exp);
            return result;
        }
    }
}