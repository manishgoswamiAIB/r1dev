/**************************************************************************************************
* @Author:      Ajit Shetty  
* @Date:        15/05/2019
* @Description: This is a provider class for AIB_GB_EntitySearchLogic. 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/

public without sharing class AIB_GB_EntitySearchProvider {
    
    /**********************************************************************************************
    * @Author:      Ajit Shetty  
    * @Date:        15/05/2019
    * @Description: This method is used to get the Connection records for households.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static LLC_BI__Connection__c[] getConnections(Set<String> householdIds) {
        LLC_BI__Connection__c[] connections = [
            SELECT 
                Id, 
                LLC_BI__Connected_From__c,
                LLC_BI__Connected_To__c,
            	LLC_BI__Connection_Role__c
            FROM LLC_BI__Connection__c 
            WHERE LLC_BI__Connected_To__c IN :householdIds 
            	AND LLC_BI__Connection_Role__r.Name = :AIB_Constants.RELATIONSHIP_HOUSEHOLD_TYPE 
            LIMIT 1000
        ];
        
        return connections; 
    }
    
    /**********************************************************************************************
    * @Author:      Ajit Shetty  
    * @Date:        15/05/2019
    * @Description: This method is used to fetch current use Jurisdiction.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static String fetchCurrentUserRegion() {
        User[] users = [
            SELECT 
                Id, 
                AIB_Jurisdiction__c
            FROM User 
            WHERE Id = :UserInfo.getUserId() 
            LIMIT 1
        ];
        
        return (users != null && users.size() > 0 ) ? users[0].AIB_Jurisdiction__c : ''; 
    }
    
    /**********************************************************************************************
    * @Author:      Ajit Shetty  
    * @Date:        15/05/2019
    * @Description: This method is used to get the Household Connection role Id.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static LLC_BI__Connection_Role__c[] getHouseholdRoleId() {
        LLC_BI__Connection_Role__c[] connectionRoles = [
            SELECT 
            	Id 
            FROM LLC_BI__Connection_Role__c 
            WHERE Name = :AIB_Constants.RELATIONSHIP_HOUSEHOLD_TYPE
            LIMIT 1
        ];
        
        return connectionRoles;
    }
    
    /**********************************************************************************************
    * @Author:      Ajit Shetty  
    * @Date:        15/05/2019
    * @Description: This method is used to fetch existing shareholder records.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static AIB_Shareholder__c[] fetchRelatedShareholders(Set<String> borrowerIds) {
        AIB_Shareholder__c[] shareholders = [
            SELECT 
            	Id 
            FROM AIB_Shareholder__c 
            WHERE AIB_RelationshipName__c IN :borrowerIds
            LIMIT 10000
        ];
        
        return shareholders;
    }
    
    /**********************************************************************************************
    * @Author:      Ajit Shetty  
    * @Date:        15/05/2019
    * @Description: This method is used to get the Application record.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static LLC_BI__Product_Package__c[] getApplicationDetail(String applicationId) {
        LLC_BI__Product_Package__c[] applications = [
            SELECT 
            	Id, 
            	LLC_BI__Account__c
            FROM LLC_BI__Product_Package__c 
            WHERE Id = :applicationId
            LIMIT 1
        ];
        
        return applications;
    }
}