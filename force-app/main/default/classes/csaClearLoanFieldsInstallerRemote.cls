public without sharing class csaClearLoanFieldsInstallerRemote {

    public class CheckInstall extends nFORCE.RemoteActionController.Endpoint {
        public override virtual Object invoke(Map<String,Object> params) {
            nFORCE.SystemProperties properties = nFORCE.SystemProperties.getInstance();
            return properties.getPropertyAsBoolean(
                nCSA_FRAME.csaConstants.CSASYSTEMPROPERTYCATEGORY,
                csaClearLoanFieldsConstants.PROPERTY_INSTALLEDKEY,
                false
            );
        }
    }

    public class Install extends nFORCE.RemoteActionController.Endpoint {
        public override virtual Object invoke(Map<String,Object> params) {
            List<sObject> toInsert = new List<sObject>();

            toInsert.add(nFORCE.SystemProperties.createProperty(
                nCSA_FRAME.csaConstants.CSASYSTEMPROPERTYCATEGORY,
                csaClearLoanFieldsConstants.PROPERTY_INSTALLEDKEY,
                csaClearLoanFieldsConstants.PROPERTY_INSTALLEDVALUE,
                true
            ));

            toInsert.add(nFORCE.SystemProperties.createProperty(
                csaClearLoanFieldsConstants.PROPERTY_CATEGORY,
                csaClearLoanFieldsConstants.PROPERTY_CLEARKEY,
                csaClearLoanFieldsConstants.PROPERTY_CLEARVALUE,
                true
            ));

            nFORCE.DMLUtility.insertObj(toInsert);
            return null;
        }
    }

    public class Uninstall extends nFORCE.RemoteActionController.Endpoint {
        public override virtual Object invoke(Map<String,Object> params) {
            List<sObject> toDelete = new List<sObject>();

            toDelete.addAll([SELECT
                                    Id
                            FROM
                                    nFORCE__System_Properties__c
                            WHERE
                                    nFORCE__Key__c = :csaClearLoanFieldsConstants.PROPERTY_INSTALLEDKEY
                            OR
                                    nFORCE__Category_Name__c = :csaClearLoanFieldsConstants.PROPERTY_CATEGORY
                            LIMIT 100]);

            if(!toDelete.isEmpty()) {
                nFORCE.DMLUtility.deleteObj(toDelete);
            }

            return null;
        }
    }
}