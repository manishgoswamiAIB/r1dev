/**************************************************************************************************
* @Author:      Seb Merritt  
* @Date:        25/01/2019
* @PageCode:    GL
* @Description: This is a generic logger class. It would handle and have option to turn ON/OFF the
*       logging to System.debug. It can be extended to log at external system based on priority. 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
public without sharing class AIB_GenericLogger {
  
    private static final String GENERIC_MESSAGE = '***AIB Logger*** ';
    private static final String DEBUG_LEVEL_ERROR_CODE = 'Error';
  
 
    /**********************************************************************************************
    * @Author:      Seb Merritt  
    * @Date:        25/01/2019
    * @Description: Method to write passed in string in System.Debug 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static void log(string message){
        System.debug(GENERIC_MESSAGE + message);
    }
    
    /**********************************************************************************************
    * @Author:      Seb Merritt 
    * @Date:        25/01/2019
    * @Description: Overload Method to write passed in exception and call writeDebug with formed string 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static void log(Exception ex){
  
        string errorMessage = GENERIC_MESSAGE + 'at ' + ex.getLineNumber() + ' :: ' + ex.getMessage();
        log (errorMessage);
      
   }
    /**********************************************************************************************
    * @Author:      Seb Merritt   
    * @Date:        25/01/2019
    * @Description: Overload Method to write passed in exception and call writeDebug with formed string 
    *                With Error Code
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static void log(String errorCode, Exception ex){
       
        String errorMessage = GENERIC_MESSAGE 
            + errorCode + 'at ' 
            + ex.getLineNumber() 
            + ' :: ' + ex.getMessage();
        
        log (errorMessage);
        
    }
    

    /**********************************************************************************************
    * @Author:      Seb Merritt 
    * @Date:        25/01/2019
    * @Description: helper method for writing into the exception log object
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
   public static void log(String className, String methodName, Exception ex){
    AIB_CalloutWrapper.Result result = new AIB_CalloutWrapper.Result();
    try{
        AIB_ExceptionLog__c exceptionLogObj = new AIB_ExceptionLog__c();
        exceptionLogObj.AIB_Source__c = className == null ? null : className;
        exceptionLogObj.AIB_SourceFunction__c = methodName == null ? null : methodName;
        exceptionLogObj.AIB_StackTrace__c = ex.getStackTraceString() == null ? null : 
        											ex.getStackTraceString().length() > 255 ? 
                                                	ex.getStackTraceString().substring(0,255) : 
        											ex.getStackTraceString();
        exceptionLogObj.AIB_LineNumber__c= ex.getLineNumber() == null ? 0 :  ex.getLineNumber();
        exceptionLogObj.AIB_Type__c = ex.getTypeName() == null ? null : ex.getTypeName();
        exceptionLogObj.AIB_Message__c = ex.getMessage() == null ? null : ex.getMessage();   
        insert exceptionLogObj;
      }
    catch(Exception exp){
     result.hasError = true;
            result.message = Label.AIB_Error_CalloutExceptionOccurred;
            result.code = 'EC-001';
            AIB_GenericLogger.log(AIB_CalloutService.class.getName(), 'log', exp);    
    }                                   
    
   }
    
    
    /**********************************************************************************************
    * @Author:      Seevendra Singh
    * @Date:        23/05/2019
    * @Description: helper method for writing into the exception log object
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static String log (String className, 
                              String methodName, 
                              AIB_CalloutWrapper.Parameter calloutParam,  
                              String endpoint, 
                              Integer code) {
        
        try{
            AIB_ExceptionLog__c exceptionLogObj = new AIB_ExceptionLog__c();
            exceptionLogObj.AIB_Source__c = className == null ? null : className;
            exceptionLogObj.AIB_SourceFunction__c = methodName == null ? null : methodName;  
            exceptionLogObj.AIB_Type__c = AIB_CONSTANTS.Log_TYPE;
            exceptionLogObj.AIB_Message__c = AIB_Constants.LOG_MESSAGE;
            exceptionLogObj.AIB_Message__c = AIB_Constants.LOG_MESSAGE;
            exceptionLogObj.AIB_Endpoint__c = endpoint.replace(
                Label.AIB_Label_UserRegionPlaceholder, 
                calloutParam.userRegion
            );
            exceptionLogObj.AIB_ResponseCode__c = code;
            exceptionLogObj.AIB_Parameters__c = calloutParam.urlSearchParams;
            insert exceptionLogObj;
            return exceptionLogObj.Id;
        }
        catch(Exception exObj){
            AIB_GenericLogger.log(AIB_CalloutService.class.getName(), 'log', exObj); 
            return null;
        }                                   
        
    }
    
    
}