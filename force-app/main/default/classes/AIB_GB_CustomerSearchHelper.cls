/**************************************************************************************************
* @Author:      Amol Patil  
* @Date:        20/05/2019
* @Description: This is a helper class to be used for AIB_GB_CustomerSearchController
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
public with sharing class AIB_GB_CustomerSearchHelper {              
    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        20/05/2019
    * @Description: This method will return the repsone for the callout. 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static  AIB_GB_CustomerSearchWrapper.Result getCustomerResponse(
        AIB_GB_CustomerSearchWrapper.Parameter param
    ){
        
        AIB_GB_CustomerSearchWrapper.Result result = new AIB_GB_CustomerSearchWrapper.Result();
        AIB_CalloutWrapper.Parameter calloutParam = new AIB_CalloutWrapper.Parameter();    
        try {
            
            calloutParam.integrationSettingName = param.integrationSettingName;
            calloutParam.userRegion = param.region;          
            calloutParam.urlSearchParams = AIB_GB_CustomerSearchHelper.generateSearchParams(param);
                
			AIB_CalloutWrapper.Result calloutResult = AIB_CalloutService.makeCallout(calloutParam);
            
            if (calloutResult.response.getStatusCode() == AIB_Constants.CALLOUT_STATUS_CODE_SUCCESS) {
                
                result.hasInfo = true;
                result.code = String.valueOf(calloutResult.response.getStatusCode());
                AIB_GB_CustomersResponseWrapper customersResponse = 
                    (AIB_GB_CustomersResponseWrapper)JSON.deserialize(
                        AIB_Util.replaceReservedKeywords(calloutResult.response.getBody()),
                        AIB_GB_CustomersResponseWrapper.class); 
               result.customersResponse = customersResponse.x_embedded.summaries;
            }
            else if (calloutResult.response.getStatusCode() ==
                     AIB_Constants.CALLOUT_STATUS_CODE_CUSTOMERNOTFOUND
            ) { 
                result.hasWarning = true;
                result.message = Label.AIB_GB_Error_CustomerNotFound;
                result.code = String.valueOf(calloutResult.response.getStatusCode());
                AIB_GenericLogger.log(result.message);
            } 
            else { 
                result.hasError = true;
                result.message = Label.AIB_Error_CalloutExceptionOccurred;
                if (calloutResult.response != null 
                    && String.isNotBlank(calloutResult.response.getBody())
              )
               
                {
                    AIB_GB_ErrorResponseWrapper errorResponse = 
                        (AIB_GB_ErrorResponseWrapper)JSON.deserialize(
                            AIB_Util.replaceReservedKeywords(calloutResult.response.getBody()), 
                            AIB_GB_ErrorResponseWrapper.class); 
                    if (String.isNotBlank(errorResponse.message)) {
                        result.message = errorResponse.message;
                    }
                    result.code = String.valueOf(calloutResult.response.getStatusCode());
                }                                
                
                AIB_GenericLogger.log(result.message);
            }
        }
        catch (Exception exp) {
            result.hasError = true;
            result.message = Label.AIB_Error_CalloutExceptionOccurred;
            result.code = AIB_Constants.CONSTANT_CODE ;
            AIB_GenericLogger.log(AIB_GB_CustomerSearchHelper.class.getName(), 
                                  AIB_Constants.CONSTANT_GET_CUSTOMER_RESPONSE_METHOD,
                                  exp);
        }
        return result;
    }
    
    /**********************************************************************************************
    * @Author:      Amol Patil  
    * @Date:        20/05/2019
    * @Description: This method will use to Generate URL. 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/    
    public static String generateSearchParams(AIB_GB_CustomerSearchWrapper.Parameter param){
        String  urlPar = '';
        try{
            List<String> urlParams = new List<String>();                                    
            if (String.isNotEmpty(param.name) && String.isNotEmpty(param.customerType)) {
                urlParams.add(AIB_Constants.CONSTANT_CUSTOMER_TYPE + param.customerType);
            }
            
            if (String.isNotEmpty(param.name)) {
                urlParams.add(AIB_Constants.CONSTANT_CUSTOMER_NAME + param.name);
            }if (String.isNotEmpty(param.NSC)) {
                urlParams.add(AIB_Constants.CONSTANT_ACCOUNT_NUMBER + param.NSC);
            }if (String.isNotEmpty(param.contactNumberFirst)) {
                urlParams.add(AIB_Constants.CONSTANT_PHONE_NUMBER_PREFIX + param.contactNumberFirst);
            }
            if (String.isNotEmpty(param.dateOfBirth)) {
                urlParams.add(AIB_Constants.CONSTANT_DOB + param.dateOfBirth);
            }
            if (String.isNotEmpty(param.contactNumberSecond)) {
                urlParams.add(AIB_Constants.CONSTANT_PHONE_NUMBER + param.contactNumberSecond);
            }
            if (String.isNotEmpty(param.cardType)) {
                urlParams.add(AIB_Constants.CONSTANT_CARD_TYPE + param.cardType);
            }
            if (String.isNotEmpty(param.cardNumber)) {
                urlParams.add(AIB_Constants.CONSTANT_CARD_NUMBER + param.cardNumber);
            }
            if (String.isNotEmpty(param.selfServiceNo)) {
                urlParams.add(AIB_Constants.CONSTANT_SELFSERVICE_NUMBER + param.selfServiceNo);
            }
            if (String.isNotEmpty(param.county)) {
                urlParams.add(AIB_Constants.CONSTANT_COUNTY_CODE + param.county);
            }
            if (String.isNotEmpty(param.postCode)) {
                urlParams.add(AIB_Constants.CONSTANT_POST_CODE + param.postCode);
            }
            if (String.isNotEmpty(param.addressLineOne)) {
                urlParams.add(AIB_Constants.CONSTANT_ADDRESSLINE_ONE + param.addressLineOne);
            }
            
            urlPar = String.join(urlParams,AIB_Constants.CONSTANT_AMPERSAND); 
        }
        catch(Exception exp){
            AIB_GenericLogger.log(AIB_GB_CustomerSearchHelper.class.getName(), 
                                  'generateSearchParams',
                                  exp);
        }
        return urlPar;
    }
    
    /**********************************************************************************************
    * @Author:      Amol Patil
    * @Date:        20/05/2019
    * @Description: This method is used to update address with comma. 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static List<AIB_GB_CustomersResponseWrapper.Summaries> updateFullAddress(
        List<AIB_GB_CustomersResponseWrapper.Summaries> customers){            
            try{
                List<String> allAddressList = new List<String>();
                for(AIB_GB_CustomersResponseWrapper.Summaries customer : customers) {
                    
                    if(String.isNotBlank(customer.addressLine1)){
                        allAddressList.add(customer.addressLine1);
                    }
                    if(String.isNotBlank(customer.addressLine2)){
                        allAddressList.add(customer.addressLine2);
                    }
                    if(String.isNotBlank(customer.addressLine3)){
                        allAddressList.add(customer.addressLine3);
                    }
                    if(String.isNotBlank(customer.addressLine4)){
                        allAddressList.add(customer.addressLine4);
                    }                    
                    if (allAddressList.size() > 0){
                        String updatedFullAddress = String.join(allAddressList, 
                                                                AIB_Constants.CONSTANT_COMMA);
                        customer.fullAddress = updatedFullAddress;
                    }
                    allAddressList.clear();
                }
            }
            catch(Exception ex) {
                AIB_GenericLogger.log(AIB_GB_CustomerSearchController.class.getName(),AIB_Constants.CONSTANT_UPDATE_ADDRESS_METHOD,ex);
            }
            return customers;
        }
    /**********************************************************************************************
    * @Author:      Amol Patil
    * @Date:        20/05/2019
    * @Description: This method will return the dropdown values from custom metadata. 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static Map<String,List<AIB_DropdownSetting_CMD__mdt>> populatePicklistValues() {
        Map<String,List<AIB_DropdownSetting_CMD__mdt>> dropdownsMap = new Map<String,List<AIB_DropdownSetting_CMD__mdt>>();
        try{
            
            AIB_DropdownSetting_CMD__mdt[] dropdownValues = AIB_GB_CustomerSearchProvider.fetchDropDownValues();
            for (AIB_DropdownSetting_CMD__mdt dropdown : dropdownValues) {
                if (!dropdownsMap.containsKey(dropdown.AIB_FieldType__c)) {
                    dropdownsMap.put(dropdown.AIB_FieldType__c,new List<AIB_DropdownSetting_CMD__mdt>());
                }
                dropdownsMap.get(dropdown.AIB_FieldType__c).add(dropdown);
            }        
        }
        catch(Exception exp){
            dropdownsMap = new Map<String,List<AIB_DropdownSetting_CMD__mdt>>();
            AIB_GenericLogger.log(AIB_GB_CustomerSearchController.class.getName(), AIB_Constants.CONSTANT_GET_PICKLIST_VALUE_METHOD,exp);
        }
        return dropdownsMap;
    }  
    /**********************************************************************************************
    * @Author:      Sameer Jewalikar  
    * @Date:        04/06/2019
    * @Description: This method will pass selected customer Id's for making callout
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static  AIB_GB_CustomerSearchWrapper.Result getBorrowerResponse(
        AIB_GB_CustomerSearchWrapper.Parameter param
    ){
        String recordId = '';
        AIB_GB_CustomerSearchWrapper.Result result = new AIB_GB_CustomerSearchWrapper.Result();
        AIB_CalloutWrapper.Parameter calloutParam = new AIB_CalloutWrapper.Parameter();    
        try {
            
            calloutParam.integrationSettingName = param.integrationSettingName;
            calloutParam.userRegion = param.region;          
           	calloutParam.urlSearchParams = param.selectedCustomerId;
            calloutParam.recordId = param.recordId;
            calloutParam.requestHeadersMap = 
                new Map<String, String>{'Content-Type'=> 'application/json'};
                
			AIB_CalloutWrapper.Result calloutResult = AIB_CalloutService.makeCallout(calloutParam);
            
            if (calloutResult.response.getStatusCode() == AIB_Constants.CALLOUT_STATUS_CODE_SUCCESS) {
                //i: Call the method to create relationship records              
                AIB_CalloutWrapper.Result confirmResult = 
                    AIB_GB_EntitySearchLogic.createRelationshipRecords(
                        calloutParam, 
                        calloutResult.response.getbody()
                    );
                result.hasInfo = confirmResult.hasInfo;
                result.hasWarning = confirmResult.hasWarning;
                result.hasError = confirmResult.hasError;
                result.recordId = confirmResult.recordId;
                result.message = confirmResult.message;
            }
            else { 
                result.hasError = true;
                result.message = Label.AIB_Error_CalloutExceptionOccurred;
                if (calloutResult.response != null 
                    && String.isNotBlank(calloutResult.response.getBody())
                ) {
                    AIB_GB_ErrorResponseWrapper errorResponse = 
                        (AIB_GB_ErrorResponseWrapper)JSON.deserialize(
                            AIB_Util.replaceReservedKeywords(calloutResult.response.getBody()), 
                            AIB_GB_ErrorResponseWrapper.class); 
                    if (String.isNotBlank(errorResponse.message)) {
                        result.message = errorResponse.message;
                    }
                    result.code = String.valueOf(calloutResult.response.getStatusCode());
                }                                
                
                AIB_GenericLogger.log(result.message);
            }
        }
        catch (Exception exp) {
            result.hasError = true;
            result.message = Label.AIB_Error_CalloutExceptionOccurred;
            result.code = AIB_Constants.CONSTANT_CODE ;
            AIB_GenericLogger.log(AIB_GB_CustomerSearchHelper.class.getName(), 
                                  AIB_Constants.CONSTANT_GET_CUSTOMER_RESPONSE_METHOD,
                                  exp);
        }
        return result;
    }
    

}