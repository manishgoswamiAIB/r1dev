/**************************************************************************************************
* @Author:      Seevendra Singh 
* @Date:        20/05/2019
* @Description: This is a provider class for AIB_CalloutService class. 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
public without sharing class AIB_CalloutServiceProvider {

    /**********************************************************************************************
    * @Author:      Seevendra Singh  
    * @Date:        20/05/2019
    * @Description: This method queries Custom metadata by name and returns the list.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static AIB_IntegrationSetting_CMD__mdt[] queryIntegrationSettingByName(
            String settingName) {
                
       AIB_IntegrationSetting_CMD__mdt[] integrationSettingList = [
            SELECT Id, 
            	DeveloperName,
            	AIB_RequestEndpoint__c, 
            	AIB_RequestType__c, 
            	AIB_RequireMock__c,  
            	AIB_MockResponseResourceName__c, 
            	AIB_LogAll__c, 
            	AIB_LogErrorsOnly__c  
            FROM AIB_IntegrationSetting_CMD__mdt  
            WHERE DeveloperName = :settingName 
            LIMIT 1
        ];
                        
                return integrationSettingList;
    }
    
    /**********************************************************************************************
    * @Author:      Seevendra Singh  
    * @Date:        07/05/2019
    * @Description: This method queries in Static Resource by name and returns the list.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static StaticResource[] queryMockResponseFromResource(String resourceName) {
                StaticResource[]  mockResponseList = [
            SELECT Id, 
            	Body, 
                BodyLength 
            FROM StaticResource  
            WHERE Name = :resourceName 
            LIMIT 1
        ];
              
        return mockResponseList;
    }
    
    /**********************************************************************************************
    * @Author:      Seevendra Singh  
    * @Date:        20/05/2019
    * @Description: This method queries metadata that is used to store request header data.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static AIB_IntegrationRequestHeader_CMD__mdt[] queryHeaderDataBySettingId(String settingId) {
                
       AIB_IntegrationRequestHeader_CMD__mdt[] requestHeaders = [
            SELECT Id, 
            	AIB_RequestHeaderName__c,
				AIB_RequestHeaderValue__c				
            FROM AIB_IntegrationRequestHeader_CMD__mdt  
            WHERE AIB_IntegrationSetting__c = :settingId 
            LIMIT 1000
        ];
                        
		return requestHeaders;
    }
}