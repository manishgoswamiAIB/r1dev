/**************************************************************************************************
* @Author:      Seevendra Singh  
* @Date:        20/05/2019
* @Description: This is a wrapper class to be used for callout 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
global without sharing class AIB_CalloutWrapper extends AIB_BaseWrapper {

    /**********************************************************************************************
    * @Author:      Seevendra Singh  
	* @Date:        20/05/2019
    * @Description: This is a callout inner wrapper class to set parameters 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    global class Parameter extends AIB_BaseWrapper.Parameter {
        public String integrationSettingName {get; set;}
        public String userRegion {get; set;}
        public String urlSearchParams {get; set;}
        public String requestBody {get; set;}
        public Map<String,String> requestHeadersMap {get; set;}
    }

    /**********************************************************************************************
    * @Author:      Seevendra Singh  
	* @Date:        06/05/2019
    * @Description: This is a callout inner wrapper class to store results. 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    global class Result extends AIB_BaseWrapper.Result {
        public HttpResponse response {get; set;}  
    }
}