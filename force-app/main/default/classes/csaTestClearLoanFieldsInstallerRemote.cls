@isTest(IsParallel=true)
private with sharing class csaTestClearLoanFieldsInstallerRemote {

    @isTest
    static void testCheckInstall() {
        nCSA_FRAME.csaTestDataFactory.createForceSystemProperty(
            nCSA_FRAME.csaConstants.CSASYSTEMPROPERTYCATEGORY,
            csaClearLoanFieldsConstants.PROPERTY_INSTALLEDKEY,
            csaClearLoanFieldsConstants.PROPERTY_INSTALLEDVALUE,
            true,
            null
        );

        Map<String, Object> params = new Map<String, Object>();

        Test.startTest();

        Boolean result = (Boolean)nFORCE.RemoteActionController.invoke(
            'csaClearLoanFieldsInstallerRemote.CheckInstall', params
        );

        Test.stopTest();

        System.assertEquals(true, result, 'Ensure result is returned as true');
    }

    @isTest
    static void testInstall() {
        Map<String, Object> params = new Map<String, Object>();

        Test.startTest();

        nFORCE.RemoteActionController.invoke('csaClearLoanFieldsInstallerRemote.Install', params);
        List<nFORCE__System_Properties__c> results = [SELECT Id FROM nFORCE__System_Properties__c LIMIT 15];

        Test.stopTest();

        System.assertEquals(2, results.size(), 'Ensure all Properties are installed');
    }

    @isTest
    static void testUninstall() {
        nCSA_FRAME.csaTestDataFactory.createForceSystemProperty(
            nCSA_FRAME.csaConstants.CSASYSTEMPROPERTYCATEGORY,
            csaClearLoanFieldsConstants.PROPERTY_INSTALLEDKEY,
            csaClearLoanFieldsConstants.PROPERTY_INSTALLEDVALUE,
            true,
            null
        );

        Map<String, Object> params = new Map<String, Object>();

        Test.startTest();

        nFORCE.RemoteActionController.invoke('csaClearLoanFieldsInstallerRemote.Uninstall', params);
        List<nFORCE__System_Properties__c> results = [SELECT Id FROM nFORCE__System_Properties__c LIMIT 1];

        Test.stopTest();

        System.assertEquals(0, results.size(), 'Ensure all properties are uninstalled');
    }

    @TestSetup
    private static void testSetup() {
        nFORCE.BeanRegistry.getInstance().registerBean(
            'ClassTypeProvider',
            nFORCE.ClassTypeProvider.class,
            Type.forName('ClassTypeProvider')
        );
    }
}