/**************************************************************************************************
* @Author:      Seevendra Singh 
* @Date:        20/05/2019
* @Description: This is a test class for AIB_CalloutService class.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
@isTest (SeeAllData = false)
private without sharing class AIB_CalloutService_Test {
    
    
    /***********************************************************************************************
    * @Author:      Seevendra Singh 
    * @Date:        20/05/2019
    * @Description: Method to test status code 200 in response of callout. This method uses
    * 				StaticResourceCalloutMock to mock the callout.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    private static testmethod void getCustomerResponseFromCalloutWithCode200() {
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('AIB_GB_CustomerResponseCode200_Test');
        mock.setStatusCode(AIB_Constants.CALLOUT_STATUS_CODE_SUCCESS);
        mock.setStatus(AIB_Constants.CALLOUT_STATUS_SUCCESS);
        mock.setHeader('Content-Type', 'application/json');
        //i: Setting the mockresponse that will be returned as the response
        //   of the callout
        Test.setMock(HttpCalloutMock.class, mock);

        Test.startTest();
        
        AIB_CalloutWrapper.Parameter params = new AIB_CalloutWrapper.Parameter();
        params.integrationSettingName = 'GetCustomerWith200CodeFromCallout_Test';
        params.userRegion = 'roi';
        params.urlSearchParams = 'customerID=1234&customerType=O&customerName=Test';
        params.requestBody = '';
        params.requestHeadersMap = new Map<String, String>{'Content-Type'=> 'application/json'};
        AIB_CalloutWrapper.Result result = AIB_CalloutService.makeCallout(params);
                
        Test.stopTest();
        
        System.assertEquals(
                    AIB_Constants.CALLOUT_STATUS_CODE_SUCCESS, 
                    result.response.getStatusCode()
                );
        System.assertNotEquals(null, result.response.getBody());
        
    }
    
    /***********************************************************************************************
    * @Author:      Seevendra Singh 
    * @Date:        20/05/2019
    * @Description: Method to test status code 200 in response of callout. This method uses
    * 				Static resource name stored in integrationSettingName variable to create
    * 				response.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    private static testmethod void getCustomerResponseFromResourceWithCode200() {
        
        Test.startTest();
        
        AIB_CalloutWrapper.Parameter params = new AIB_CalloutWrapper.Parameter();
        params.integrationSettingName = 'GetCustomerWith200CodeFromResource_Test';
        params.userRegion = 'roi';
        params.urlSearchParams = 'customerID=1234&customerType=O&customerName=Test';
        params.requestBody = '';
        params.requestHeadersMap = new Map<String, String>{'Content-Type'=> 'application/json'};
        AIB_CalloutWrapper.Result result = AIB_CalloutService.makeCallout(params);

        Test.stopTest();
        
        System.assertEquals(
                    AIB_Constants.CALLOUT_STATUS_CODE_SUCCESS, 
                    result.response.getStatusCode()
                );
        System.assertNotEquals(null, result.response.getBody());
    }
    
    /***********************************************************************************************
    * @Author:      Seevendra Singh 
    * @Date:        20/05/2019
    * @Description: Method to test status code 404 in response of callout. This method uses
    * 				StaticResourceCalloutMock to mock the callout.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    private static testmethod void getCustomerResponseFromCalloutWithCode404() {
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('AIB_GB_CustomerResponseCode404_Test');
        mock.setStatusCode(AIB_Constants.CALLOUT_STATUS_CODE_CUSTOMERNOTFOUND);
        mock.setStatus(AIB_Constants.CALLOUT_STATUS_CUSTOMERNOTFOUND);
        mock.setHeader('Content-Type', 'application/json');
        
        Test.setMock(HttpCalloutMock.class, mock);
        
        Test.startTest();

        AIB_CalloutWrapper.Parameter params = new AIB_CalloutWrapper.Parameter();
        params.integrationSettingName = 'GetCustomerWith404CodeFromCallout_Test';
        params.userRegion = 'roi';
        params.urlSearchParams = 'customerID=1234&customerType=O&customerName=Test';
        params.requestBody = '';
        params.requestHeadersMap = new Map<String, String>{'Content-Type'=> 'application/json'};
        AIB_CalloutWrapper.Result result = AIB_CalloutService.makeCallout(params);
        
        Test.stopTest();
        
        System.assertEquals(
                    AIB_Constants.CALLOUT_STATUS_CODE_CUSTOMERNOTFOUND, 
                    result.response.getStatusCode()
                );
       
    }
    
    /***********************************************************************************************
    * @Author:      Seevendra Singh 
    * @Date:        20/05/2019
    * @Description: Method to test status code 404 in response of callout. This method uses
    * 				Static resource name stored in integrationSettingName variable to create
    * 				response.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    private static testmethod void getCustomerResponseFromResourceWithCode404() {
        
        Test.startTest();

        AIB_CalloutWrapper.Parameter params = new AIB_CalloutWrapper.Parameter();
        params.integrationSettingName = 'GetCustomerWith404CodeFromResource_Test';
        params.userRegion = 'roi';
        params.urlSearchParams = 'customerID=1234&customerType=O&customerName=Test';
        params.requestBody = '';
        params.requestHeadersMap = new Map<String, String>{'Content-Type'=> 'application/json'};
        AIB_CalloutWrapper.Result result = AIB_CalloutService.makeCallout(params);
        
        Test.stopTest();
        
        System.assertEquals(
                    AIB_Constants.CALLOUT_STATUS_CODE_CUSTOMERNOTFOUND, 
                    result.response.getStatusCode()
                );
       
    }
    
    /***********************************************************************************************
    * @Author:      Seevendra Singh 
    * @Date:        20/05/2019
    * @Description: Method to test status code 403 in response of callout. This method uses
    * 				StaticResourceCalloutMock to mock the callout.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    private static testmethod void getCustomerResponseFromCalloutWithOtherError() {
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('AIB_GB_CustomerResponseCode404_Test');
        mock.setStatusCode(AIB_Constants.CALLOUT_STATUS_CODE_FORBIDDEN);
        mock.setStatus(AIB_Constants.CALLOUT_STATUS_FORBIDDEN);
        mock.setHeader('Content-Type', 'application/json');

        Test.setMock(HttpCalloutMock.class, mock);
        
        Test.startTest();

        AIB_CalloutWrapper.Parameter params = new AIB_CalloutWrapper.Parameter();
        params.integrationSettingName = 'GetCustomerWith404CodeFromCallout_Test';
        params.userRegion = 'roi';
        params.urlSearchParams = 'customerID=1234&customerType=O&customerName=Test';
        params.requestBody = '';
        params.requestHeadersMap = new Map<String, String>{'Content-Type'=> 'application/json'};
        AIB_CalloutWrapper.Result result = AIB_CalloutService.makeCallout(params);
        
        Test.stopTest();

        System.assertEquals(
                    AIB_Constants.CALLOUT_STATUS_CODE_FORBIDDEN, 
                    result.response.getStatusCode()
                );
        
    }
    
	/***********************************************************************************************
    * @Author:      Seevendra Singh 
    * @Date:        20/05/2019
    * @Description: Method to test status code 403 in response of callout. This method uses
    * 				StaticResourceCalloutMock to mock the callout.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    private static testmethod void getCustomerMockResponseNotFoundTest() {
        
        Test.startTest();

        AIB_CalloutWrapper.Parameter params = new AIB_CalloutWrapper.Parameter();
        params.integrationSettingName = 'GetCustomerWith500CodeFromResource_Test';
        params.userRegion = 'roi';
        params.urlSearchParams = 'customerID=1234&customerType=O&customerName=Test';
        params.requestBody = '';
        params.requestHeadersMap = new Map<String, String>{'Content-Type'=> 'application/json'};
        AIB_CalloutWrapper.Result result = AIB_CalloutService.makeCallout(params);
        
        Test.stopTest();

        System.assertEquals(
                    AIB_Constants.CALLOUT_STATUS_CODE_INTERNALSERVERERROR, 
                    result.response.getStatusCode()
                );
        
    }
    
    /***********************************************************************************************
    * @Author:      Seevendra Singh 
    * @Date:        20/05/2019
    * @Description: Method to test exception response of callout.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    private static testmethod void testCustomerCalloutException() {
        
        AIB_CalloutWrapper.Parameter params = new AIB_CalloutWrapper.Parameter();
        //i: custom metadata name for testing
        params.integrationSettingName = null;
        params.userRegion = null;
        params.urlSearchParams = null;
        params.requestBody = null;
        params.requestHeadersMap = new Map<String, String>{'Content-Type'=> 'application/json'};
        AIB_CalloutWrapper.Result result = AIB_CalloutService.makeCallout(params);
        //i: To check if there was an exception
        System.assertEquals(
            		Label.AIB_Error_CalloutExceptionOccurred, 
            		result.message
        		);
        
    }
}