/**************************************************************************************************
* @Author:      Pradeep Verma 
* @Date:        22/05/2019
* @Description: This is a test class for AIB_GB_BaseBorrowerController class.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
@isTest(SeeAllData=false)
public class AIB_GB_BaseBorrowerController_Test {
    
    @testSetup 
    private static void setup () {
        Profile profileId =  [Select Id from Profile where Name = 'AIB Credit Underwriters' LIMIT 1] ;
        List<User> testUser = new List<User>();
        User user = new User(FirstName = AIB_Constants.CONSTANT_FIRSTNAME,
                             LastName = AIB_Constants.CONSTANT_LASTNAME,
                             email = AIB_Constants.CONSTANT_USER_EMAIL,
                             Username = AIB_Constants.CONSTANT_USERNAME,
                             Alias =AIB_Constants.CONSTANT_ALIAS,
                             ProfileId = profileId.id,
                             TimeZoneSidKey=AIB_Constants.CONSTANT_TIMEZONESIDKEY,
                             LocaleSidKey=AIB_Constants.CONSTANT_LOCALESIDKEY,
                             LanguageLocaleKey=AIB_Constants.CONSTANT_LANGUAGE_LOCALE_KEY,
                             EmailEncodingKey=AIB_Constants.CONSTANT_EMAIL_ENCODING_KEY,
                             AIB_Jurisdiction__c =AIB_Constants.CONSTANT_JURISDICTION);
        
        testUser.add(user);
        
        Database.insert(testUser);
        
        
    }
    
    /**************************************************************************************************
    * @Author:      Pradeep Verma  
    * @Date:        22/05/2019
    * @Description: Method to check Jurisdiction.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***************************************************************************************************/
    private static testmethod void testJurisdictionCheck() {
        User user = [SELECT
                     FirstName, 
                     LastName, 
                     Email
                     FROM User 
                     WHERE email =: AIB_Constants.CONSTANT_USER_EMAIL
                     LIMIT 1];
        
        System.runAs(user){
            Test.startTest();
            String jsonString = AIB_GB_BaseBorrowerController.getCurrentUserRegion();
            AIB_BaseBorrowerWrapper.Result result = (AIB_BaseBorrowerWrapper.Result)JSON.deserialize
                (jsonString, AIB_BaseBorrowerWrapper.Result.class); 
            Test.stopTest();
            system.assertEquals(AIB_Constants.CONSTANT_JURISDICTION,result.jurisdiction);
        }
    }
 }