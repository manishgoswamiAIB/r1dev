/**************************************************************************************************
* @Author:      Arpita Ostwal  
* @Date:        27/05/2018
* @Description: This is a method to get response. 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
public class AIB_PP_DataProtectionQuesProvider {
    
    /**********************************************************************************************
* @Author:      Arpita Ostwal  
* @Date:        27/05/2018
* @Description: This is a method to get response
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description] 
***********************************************************************************************/
    public static nFORCE__Question__c[] queryQuestionByGroupName(String questionnaireGroupName) {
        nFORCE__Question__c[] questions = [
            SELECT id, 
            nFORCE__Order__c, 
            nFORCE__Form_Type__c, 
            AIB_ReadOnlyText__c, 
            (SELECT Id, 
             nFORCE__Label__c 
             FROM nFORCE__Options__r 
             ORDER BY nFORCE__Order__c ASC)
            FROM nFORCE__Question__c
            WHERE nFORCE__Group__r.Name = :questionnaireGroupName
            ORDER BY nFORCE__Order__c
            LIMIT 1000];
   
        return questions;
    }
    public static Map<ID, nFORCE__Option__c> queryOptions(String questionnaireGroupName) {
       	 Map<ID, nFORCE__Option__c> mapOptions = new Map <ID, nFORCE__Option__c>([
            SELECT id,
            nFORCE__Question__c,
            nFORCE__Question__r.nFORCE__Group__c
            FROM nFORCE__Option__c 
            WHERE nFORCE__Question__r.nFORCE__Group__r.Name = :questionnaireGroupName
            LIMIT 1000]);
        
        return mapOptions;
    }
    
}