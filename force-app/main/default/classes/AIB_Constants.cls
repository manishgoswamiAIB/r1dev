/*
* @Author:     	Seevendra Singh 
* @Date:       	20/05/2019
* @Description: This is a constant class. 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
public without sharing class AIB_Constants {
    
    //i: constants used for callout
    public static final Integer CALLOUT_STATUS_CODE_SUCCESS = 200;
    public static final String CALLOUT_STATUS_SUCCESS = 'OK';
    public static final Integer CALLOUT_STATUS_CODE_CUSTOMERNOTFOUND = 404;
    public static final String CALLOUT_STATUS_CUSTOMERNOTFOUND = 'Customer Not Found';
    public static final Integer CALLOUT_STATUS_CODE_RECORDNOTFOUND = 404;
    public static final String CALLOUT_STATUS_RECORDNOTFOUND = 'Record Not Found';
    public static final Integer CALLOUT_STATUS_CODE_FORBIDDEN = 403;
    public static final String CALLOUT_STATUS_FORBIDDEN = 'Forbidden';
    public static final Integer CALLOUT_STATUS_CODE_INTERNALSERVERERROR = 500;
    public static final String CALLOUT_STATUS_INTERNALSERVERERROR = 'Internal Server Error.';
    //i: For error logging
    public static final String Log_TYPE = 'Logs';
    public static final String LOG_MESSAGE = 'Callout Logs';
    public static final String LOG_REQUEST_ATTACHMENT_NAME = 'Request Data.txt';
    public static final String LOG_RESPONSE_ATTACHMENT_NAME = 'Response Data.txt';
    //i: For for AIB_PP-DaraProtectionQuesController group  name
    public static final String ROI ='ROI';
    
    public static final String GB ='GB';
    public static final String FT ='FT';
    public static final String DATA_PROTECTION_ROI ='Data Protection Requirement/CJA ROI';
    public static final String DATA_PROTECTION_FT ='Data Protection Requirement/CJA FT';
    public static final String DATA_PROTECTION_GB ='Data Protection Requirement/CJA GB';
    public static final String DATA_PROTECTION_SELECT_DEFAULT ='---Select an Option---';
    public static final String DATA_PROTECTION_COMPLETED ='Completed';
    
    //i: For EntitySearchController
    public static final String CONSTANT_ENTITY_NAME = 'entityName =';
    public static final String CONSTANT_ACC_NUMBER = 'accountNumber =';
    public static final String CONSTANT_ENTITY_ID =  'ccvId =';
    public static final String CONSTANT_COINS_NUMBER = 'coinsNumber =';
    public static final String CONSTANT_COINS_GRP_LEADER = 'coinsGroupLeader =';
    public static final String CONSTANT_COINS_TEAM_CODE = 'coinsTeamCode =';
    public static final String CONSTANT_METHOD_NAME = 'getEntityResponse';
    
    //i: For CustomerSearchController    
    public static  final String CONSTANT_CUSTOMER_TYPE = 'customerType=';
    public static  final String CONSTANT_CUSTOMER_NAME = 'customerName=';
    public static  final String CONSTANT_ACCOUNT_NUMBER = 'accountNumber=';
    public static  final String CONSTANT_PHONE_NUMBER_PREFIX = 'phoneNumberPrefix=';
    public static  final String CONSTANT_PHONE_NUMBER = 'phoneNumber=';
    public static  final String CONSTANT_DOB = 'dateOfBirth=';
    public static  final String CONSTANT_CARD_NUMBER = 'cardNumber=';
    public static  final String CONSTANT_CARD_TYPE = 'cardType=';
    public static  final String CONSTANT_POST_CODE = 'postCode=';
    public static  final String CONSTANT_SELFSERVICE_NUMBER = 'registrationID=';
    public static  final String CONSTANT_COUNTY_CODE = 'countyCode=';
    public static  final String CONSTANT_ADDRESSLINE_ONE = 'addressLineOne='; 
    public static  final String CONSTANT_AMPERSAND='&';
    public static  final String CONSTANT_COMMA=', ';
    public static  final String CONSTANT_INTEGRATION_SETTING='GetCustomer';
    public static  final String CONSTANT_GET_CUSTOMER_METHOD = 'getCustomerResult';
    public static  final String CONSTANT_UPDATE_ADDRESS_METHOD = 'updateFullAddress'; 
    public static  final String CONSTANT_DESERIALIZE_PARAM_METHOD = 'deserializeParameter';
    public static  final String CONSTANT_GET_PICKLIST_VALUE_METHOD = 'getPicklistValues';
    public static  final String CONSTANT_CODE = 'EC-001';
    //i: For CustomerSearchHelper
    public static final  String CONSTANT_GET_CUSTOMER_RESPONSE_METHOD = 'getCustomerResponse'; 
    // i: For CustomerSearchProvide
    public static final String CONSTANT_FETCH_DROPDOWN_VALUES_METHOD = 'fetchDropDownValues';
    public static final String CONSTANT_SHORTCODE = 'CustomerSearch';
    //i: For CustomerSearchController_Test
    public static  final String CONSTANT_PROFILE_NAME = 'AIB Business Super User';
    public static  final String CONSTANT_FIRSTNAME = 'Fname';
    public static  final String CONSTANT_LASTNAME = 'Lname';
    public static  final String CONSTANT_USER_EMAIL = 'tuser1234@test.org';
    public static  final String CONSTANT_USERNAME = 'tuser1234@test.org';
    public static  final String CONSTANT_ALIAS = 'tuser1';
    public static  final String CONSTANT_TIMEZONESIDKEY = 'America/Los_Angeles';
    public static  final String CONSTANT_LOCALESIDKEY = 'en_US';
    public static  final String CONSTANT_LANGUAGE_LOCALE_KEY = 'en_US';
    public static  final String CONSTANT_EMAIL_ENCODING_KEY = 'UTF-8';
    public static  final String CONSTANT_JURISDICTION = 'ROI';
    public static  final String CONSTANT_ADDRESSLINEONE='address line 1';
    public static  final String CONSTANT_ADDRESSLINETWO='address line 2';
    public static  final String CONSTANT_ADDRESSLINETHREE='address line 3';
    public static  final String CONSTANT_ADDRESSLINEFOUR='address line 4';
    public static  final String CONSTANT_POSTCODE='143001';
    public static  final String CONSTANT_COUNTY='135';
    public static  final String CONSTANT_CARDTYPE='Visa';
    public static  final String CONSTANT_CARDNUMBER='21232314';
    public static  final String CONSTANT_DATE_OF_BIRTH='3/23/1992';
    public static  final String CONSTANT_NSC='asd';
    public static  final String CONSTANT_SELF_SERVICE_NUMBER='521562';
    public static  final String CONSTANT_REGION='ROI';
    public static  final String CONSTANT_CONTACT_NUMBER_FIRST='123432';
    public static  final String CONSTANT_CONTACT_NUMBER_SECOND='2312436';
    public static  final String CONSTANT_CUSTOMERTYPE='P';
    public static  final String CONSTANT_CUSTOMERNAME='name';
    
    //i: constant to store relationship type
    public static  final String RELATIONSHIP_HOUSEHOLD_TYPE = 'Household';
    public static  final String RELATIONSHIP_INDIVIDUAL_TYPE = 'Individual';
    public static  final String RELATIONSHIP_CORPORATION_TYPE = 'Corporation';
    
    //i: constant to store relationship record type
    public static  final String RELATIONSHIP_HOUSEHOLD_RECORD_TYPE = 'Household';
    public static  final String RELATIONSHIP_INDIVIDUAL_RECORD_TYPE = 'Individual';
    public static  final String RELATIONSHIP_BUSINESS_RECORD_TYPE = 'Business';
    
    //i: Constants to store Customer Type
    public static  final String RELATIONSHIP_CUSTOMERTYPE_PERSONAL = 'Personal';
    public static  final String RELATIONSHIP_CUSTOMERTYPE_SOLETRADER = 'Sole Trader';
    public static  final String RELATIONSHIP_CUSTOMERTYPE_PARTNERSHIP = 'Partnership';
}