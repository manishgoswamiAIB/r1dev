/**************************************************************************************************
* @Author:      Amol Patil  
* @Date:        20/05/2019
* @PageCode:    EW
* @Description: This is a Customer search wrapper class to be used for Lightning Component 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
Public Without Sharing class AIB_GB_CustomerSearchWrapper extends AIB_BaseWrapper {
    
    
/**********************************************************************************************
* @Author:      Amol Patil  
* @Date:        20/05/2019
* @Description: This is an Entity search inner wrapper class to set parameters 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/
    Public Without Sharing class Parameter extends AIB_BaseWrapper.Parameter {
        public String customerType {get; set;}
        public String name {get; set;}
        public String dateOfBirth{get; set;}
        public String NSC{get; set;}
        public String contactNumberFirst {get; set;}
        public String contactNumberSecond {get; set;}
        public String cardType {get; set;}
        public String cardNumber {get; set;}
        public String selfServiceNo {get; set;}
        public String county {get; set;}
        public String postCode {get; set;}
        public String addressLineOne {get; set;}
        public String addressLineTwo {get; set;}
        public String addressLineThree {get; set;}
        public String addressLineFour {get; set;}
        public String region {get; set;}
        public String integrationSettingName {get; set;}
        public String selectedCustomerId {get;set;} 
    }
    
/**********************************************************************************************
* @Author:      Amol Patil
* @Date:        20/05/2019
* @Description: This is an Entity search inner wrapper class to store results. 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/
    public Without Sharing class Result extends AIB_BaseWrapper.Result {
        public List<AIB_GB_CustomersResponseWrapper.Summaries> customersResponse {get; set;} 
    }
    
}