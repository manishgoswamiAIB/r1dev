/**********************************************************************************************
* @Author:      Ajit Shetty 
* @Date:        15/05/2019 
* @Description: This class contains the logic to parse the JSON file received from the
*               core system, as well as the logic needed to create household, individual
*               relationships, and connections.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/

public without sharing class AIB_GB_EntitySearchLogic {
    
    /**********************************************************************************************
    * @Author:      Ajit Shetty  
    * @Date:        15/05/2019
    * @Description: This method contains the logic to parse the JSON file to an wrapper 
    * 				and use it for record creation.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    
    public static AIB_CalloutWrapper.Result createRelationshipRecords(
                                                        AIB_CalloutWrapper.Parameter param, 
                                                        String JSONResponse
    ) {
        
        System.savepoint transactionSavepoint;
        AIB_CalloutWrapper.Result result = new AIB_CalloutWrapper.Result();
        AIB_GB_BorrowerWrapper borrowerWrapper = new AIB_GB_BorrowerWrapper();
        
        try{
            //i: Setting savepoint to rollback all the DML operation incase any exception occurs
            transactionSavepoint = Database.setSavepoint();
            
            String updatedJSON = AIB_Util.replaceReservedKeywords(JSONResponse);
            borrowerWrapper = (AIB_GB_BorrowerWrapper)JSON.deserialize(
                                                                updatedJSON, 
                                                                AIB_GB_BorrowerWrapper.class
                                                            );
            
            List<Account> borrowers = 
                			AIB_GB_EntitySearchLogic.createHouseholdRelationship(borrowerWrapper);
            
            if (borrowers!= null 
               && borrowers.size() > 0
            ) {
                List<Account> customers = 
                                    AIB_GB_EntitySearchLogic.createCustomerRelationships(
                                        borrowerWrapper
                                        .embeddedCustomerList
                                        .x_embedded
                                        .customers
                                    );
                //i: method to create connections between borrower and customers relationships
                AIB_GB_EntitySearchLogic.createConnections(
                    borrowers, 
                    customers
                );
                //i: method to create shareholder records
                AIB_GB_EntitySearchLogic.createShareholders(
                    borrowers, 
                    borrowerWrapper
                    .embeddedShareholderList
                    .x_embedded
                    .customers
                );
                //i: method to create connections between borrower and customers relationships
                /*AIB_GB_EntitySearchLogic.connectHouseholdAndApplication(
                    param,
                    borrowers
                );*/
                
                result.hasInfo = true;
                result.recordId = borrowers[0].Id;
            }
            else {
                result.hasError = true;
                result.message = Label.AIB_Error_CalloutExceptionOccurred;
                result.recordId = '';
            }
            
        }
        catch(Exception exp) {
            
            Database.rollback(transactionSavepoint);
            result.hasError = true;
            result.message = Label.AIB_Error_CalloutExceptionOccurred;
            result.recordId = '';
            AIB_GenericLogger.log(AIB_GB_EntitySearchLogic.class.getName(), 
                                  'createRelationshipRecords', 
                                  exp);
            
        }
        return result;
    }
    
    /**********************************************************************************************
    * @Author:      Seevendra Singh 
    * @Date:        11/06/2019
    * @Description: This method updates the Household Id on Application.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    private static void connectHouseholdAndApplication(
                                        AIB_CalloutWrapper.Parameter param, 
                                        List<Account> borrowers
    ) {
           LLC_BI__Product_Package__c[] applications = 
               AIB_GB_EntitySearchProvider.getApplicationDetail(param.recordId);
        	if(applications != null 
              && applications.size() > 0 
            ) {
                LLC_BI__Product_Package__c application = applications[0];
                application.LLC_BI__Account__c = borrowers[0].Id;
                update application;
            }
    }
    /**********************************************************************************************
    * @Author:      Ajit Shetty  
    * @Date:        16/05/2019
    * @Description: This method contains the logic to check if the borrower exists in salesforce,
    *               and accordingly create or update the record that matches with the one in the JSON
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    private static List<Account> createHouseholdRelationship(AIB_GB_BorrowerWrapper borrowerWrapper) {
        
        List<Account> householdsToUpsert = new List<Account>();
        String householdRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName()
                                                .get(AIB_Constants.RELATIONSHIP_HOUSEHOLD_RECORD_TYPE)
                                                .getRecordTypeId();
        
        AIB_GB_BorrowerWrapper.BorrowerDemographics demograpics = borrowerWrapper.borrowerDemographics;
        String customerSinceDate = borrowerWrapper.customerSinceDate;
        
        Account household = 
            new Account(Name = borrowerWrapper.entityName,
                        LLC_BI__lookupKey__c = borrowerWrapper.entityId,
                        AIB_ExternalKey__c = borrowerWrapper.entityId,
                        AIB_BusinessCategory__c = demograpics.occupationTypeCode.label,
                        NumberOfEmployees = demograpics.employeeTotalNumber,
                        Description = demograpics.businessDescription,
                        AIB_CountryWhereIncorporated__c = demograpics.incorpCountryCode, 
                        AIB_EntityType__c = borrowerWrapper.entityType.label,
                        AIB_PrimaryBusinessSector__c = borrowerWrapper.primaryBusinessSector.label,
                        AIB_CompanyDetails__c = borrowerWrapper.CompanyType.label,
                        AIB_Ownership__c = borrowerWrapper.ownership,
                        AIB_RecordWithBank__c = borrowerWrapper.recordWithBank.label,
                        AIB_PRM__c = borrowerWrapper.primaryRelationshipManager.label,
                        AIB_DeclineDetails__c = borrowerWrapper.commentOnDeclines,
                        recordTypeId = householdRecordTypeId,
                        Type = AIB_Constants.RELATIONSHIP_HOUSEHOLD_TYPE,
                        Jurisdiction__c = AIB_GB_EntitySearchProvider.fetchCurrentUserRegion(),
                        AIB_RelatedToAIBParty__c = (borrowerWrapper.isAibParty == true) ? 'Yes' : 'No',
                        AIB_Declines__c = (borrowerWrapper.hasPreviousDeclines == true) ? 'Yes' : 'No',
                        AIB_CustomerSinceDate__c = String.isNotBlank(customerSinceDate) ? 
                        										date.valueof(customerSinceDate) : null,
                        AIB_LastCCVRefreshDate__c = String.isNotBlank(borrowerWrapper.refreshTimestamp) ? 
                        	DateTime.valueof(borrowerWrapper.refreshTimestamp.replace('T',' ')) : null
                       );
        
        
        //i: Mapping of COINS number and COINS group leader field
        List<AIB_GB_BorrowerWrapper.customers> conisList = borrowerWrapper.embeddedCOINSList.x_embedded.customers;
        
        if ( conisList != null 
            && conisList.size() > 0 
        ) {
            household.AIB_COINSNumber__c = conisList[0].coinsNumber;
            household.AIB_COINSGroupLeader__c = String.isNotBlank(conisList[0].coinsGroupCode) ? 
                									Integer.valueof(conisList[0].coinsGroupCode) : null;
        }
        
        householdsToUpsert.add(household);
        
        if (householdsToUpsert.size() > 0) {
            upsert householdsToUpsert AIB_ExternalKey__c;
        }
        
        return householdsToUpsert;
    }
    
    /**********************************************************************************************
    * @Author:      Ajit Shetty  
    * @Date:        23/05/2019
    * @Description: This method contains the logic to check if the customer exists in salesforce,
    *               and accordingly create or update the record that matches with the one in the JSON
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    private static List<Account> createCustomerRelationships(
        List<AIB_GB_BorrowerWrapper.Customers> customersWrapper
    ) {
        
        List<Account> relationshipsToUpsert = new List<Account>();
        
        String individualRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName()
            												.get('Individual').getRecordTypeId();
        String businessRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName()
            												.get('Business').getRecordTypeId();
        Account customerRec;
        for (AIB_GB_BorrowerWrapper.Customers customer : customersWrapper) {
                
            String relationshipType = '';
            String relRecordTypeId = '';
            String customerType = customer.CustomerProfile.customerType.label;
            //i: to determine if this record is of type Individual or Business
            if (customerType.equals(AIB_Constants.RELATIONSHIP_CUSTOMERTYPE_PERSONAL)  
                || customerType.equals(AIB_Constants.RELATIONSHIP_CUSTOMERTYPE_SOLETRADER) 
                || customerType.equals(AIB_Constants.RELATIONSHIP_CUSTOMERTYPE_PARTNERSHIP)
            ) {
                   relationshipType = AIB_Constants.RELATIONSHIP_INDIVIDUAL_TYPE;
                   relRecordTypeId = individualRecordTypeId;
            }
            else{
                relationshipType = AIB_Constants.RELATIONSHIP_CORPORATION_TYPE;
                relRecordTypeId = businessRecordTypeId;
            }
                    
            AIB_GB_BorrowerWrapper.PersonalProfileSummary profileSummary = customer.PersonalProfileSummary;
            AIB_GB_BorrowerWrapper.HomeAndFamily homeAndFamily = profileSummary.homeAndFamily;
            AIB_GB_BorrowerWrapper.Employment employment = profileSummary.employment;
            AIB_GB_BorrowerWrapper.Indicators profileIndicators = customer.personalProfileSummary.indicators;
            String customerSinceDate = profileIndicators.customerSinceDate;
            AIB_GB_BorrowerWrapper.FarmingDemographics farmDemos = customer.farmingDemographics;
            AIB_GB_BorrowerWrapper.BusinessDemographics businessDemos = customer.businessDemographics;
            AIB_GB_BorrowerWrapper.CustomerProfile custProfile = customer.customerProfile;
            AIB_GB_BorrowerWrapper.Address address = customer.contactDetails.address;
            //i: create a new customer to upsert based on LLC_BI__lookupKey__c external key
            customerRec = 
                new Account(LLC_BI__lookupKey__c = customer.customerID,
                            AIB_ExternalKey__c = customer.customerID,
                            AIB_TreatmentStrategy__c = profileIndicators.treatmentStrategy.label,
                            AIB_RelationshipPersonAssigned__c = custProfile.personAssigned,
                            LLC_BI__Highest_Risk_Grade__c = profileIndicators.creditGradeCode.label,
                            AIB_CustomerFinancialDifficulty__c = profileIndicators.financialDifficultyFlag,
                            AIB_CompanyType__c = businessDemos.companyType.category.label,
                            AIB_CCRStatus__c = custProfile.ccrIdentifiers.status,
                            AIB_CJAStatus__c = customer.statuses.criminalJusticeAct.status,
                            AIB_CROStatus__c = businessDemos.croDetails.companyStatusCode.code,
                            AIB_MaritalStatus__c = profileSummary.maritalStatus.label,
                            AIB_FarmClass__c = farmDemos.class_Z.label,
                            AIB_FarmStatus__c = farmDemos.status.label,
                            AIB_FarmProgramme__c = farmDemos.programme.label,
                            AIB_ResidentalStatus__c = homeAndFamily.residentialStatus.label,
                            AIB_EmploymentType__c = employment.employmentType.label,
                            AIB_JobDescription__c = employment.jobDescription,
                            AIB_EmploymentStatus__c = employment.employmentStatus.label,
                            AIB_EmployerName__c = employment.employerName,
                            AIB_EmployeeClassification__c = employment.employeeClassification.label,
                            AIB_EmployerBusiness__c = employment.employerBusinessSector.label,
                            AIB_BusinessCategory__c = employment.businessCategory.label,
                            AIB_Frequency__c = employment.paymentFrequency,
                            AIB_SME__c = profileIndicators.smeFlag,
                            Type = relationshipType,
                            recordTypeId = relRecordTypeId,
                            AIB_CountryWhereIncorporated__c = businessDemos.countryOfIncorporation.label,
                            Jurisdiction__c = AIB_GB_EntitySearchProvider.fetchCurrentUserRegion(),
                            AIB_Turnover__c = String.isNotBlank(profileSummary.turnover.value) ? 
                            						   Decimal.valueOf(profileSummary.turnover.value) : null,
                            AIB_TurnoverAsAt__c = String.isNotBlank(profileSummary.asAtDate) ? 
                                      							Date.valueof(profileSummary.asAtDate) : null,
                            AIB_FarmValue__c = String.isNotBlank(farmDemos.value.value) ? 
                                      						   Decimal.valueof(farmDemos.value.value) : null,
                            AIB_ValueAsAt__c = String.isNotBlank(farmDemos.valueDate) ? 
                                      								Date.valueof(farmDemos.valueDate) : null,
                            AIB_DateOfLastFarmVisit__c = String.isNotBlank(farmDemos.lastVisitDate) ? 
                                      							Date.valueof(farmDemos.lastVisitDate) : null,
                            AIB_Owned__c = String.isNotBlank(farmDemos.acreageOwned) ? 
                                      						  Integer.valueof(farmDemos.acreageOwned) : null,
                            AIB_Tillage__c = String.isNotBlank(farmDemos.acreageTillage) ?
                                							Integer.valueof(farmDemos.acreageTillage) : null,
                            AIB_Rented__c = String.isNotBlank(farmDemos.acreageRent) ? 
                                      						   Integer.valueof(farmDemos.acreageRent) : null,
                            AIB_Let__c = String.isNotBlank(farmDemos.acreageLet) ? 
                                      							Integer.valueof(farmDemos.acreageLet) : null,
                            AIB_Pasture__c = String.isNotBlank(farmDemos.acreagePasture) ?
                                 							Integer.valueof(farmDemos.acreagePasture) : null,
                            AIB_Forestry__c = String.isNotBlank(farmDemos.acreageForestry) ? 
                                      					   Integer.valueof(farmDemos.acreageForestry) : null,
                            AIB_DateOfBirth__c = String.isNotBlank(profileSummary.dateOfBirth) ?
                                							 Date.valueof(profileSummary.dateOfBirth) : null,
                            AIB_NoOfChildren__c = String.isNotBlank(homeAndFamily.numDependents) ?
                                		  				 Integer.valueof(homeAndFamily.numDependents) : null,
                            AIB_ChildrenAgeFrom__c = String.isNotBlank(homeAndFamily.familyAgeRangeFrom) ?
                                					Integer.valueof(homeAndFamily.familyAgeRangeFrom) : null,
                            AIB_ChildrenAgeTo__c = String.isNotBlank(homeAndFamily.familyAgeRangeTo) ? 
                                					  Integer.valueof(homeAndFamily.familyAgeRangeTo) : null,
                            AIB_ContractExpiryDate__c = String.isNotBlank(employment.contractExpiryDate) ? 
                                						  Date.valueof(employment.contractExpiryDate) : null,
                            AIB_BasicGrossIncomePA__c = String.isNotBlank(employment.yearlyGrossIncome) ? 
                                						Decimal.valueof(employment.yearlyGrossIncome) : null,
                            AIB_SalaryAsAt__c = String.isNotBlank(employment.salaryEffectiveDate) ? 
                                						 Date.valueof(employment.salaryEffectiveDate) : null,
                            AIB_NetMonthlyIncome__c = String.isNotBlank(employment.netMonthlySalary.value) ? 
                                				   Decimal.valueof(employment.netMonthlySalary.value) : null,
                            AIB_CustomerSinceDate__c = String.isNotBlank(customerSinceDate) ? 
                                      								Date.valueof(customerSinceDate) : null,
                            AIB_ContractEmployment__c = (employment.contractEmployment == true) ? 'Yes' : 'No',
                            AIB_PaidDirectToAccount__c = (employment.payedDirectly == true) ? 'Yes' : 'No'
                           );
            
            //i: populating name field based on customer type
            if(relationshipType == AIB_Constants.RELATIONSHIP_INDIVIDUAL_TYPE) {
                customerRec.PersonTitle = profileSummary.title;
                customerRec.FirstName = profileSummary.firstName;
                customerRec.LastName = profileSummary.lastName;
            }else{
                customerRec.Name = custProfile.fullName;  
            }
            
            String streetAddress = AIB_GB_EntitySearchLogic.populateAddress(address, ',\n');
            Boolean isAddressAvailable = (String.isNotBlank(streetAddress) && streetAddress.length() > 255 );
            
            customerRec.BillingStreet =  isAddressAvailable ? streetAddress.substring(0, 255) : streetAddress;
            customerRec.BillingCountry = address.countyCode.label;
            customerRec.BillingPostalCode = address.postCode;
            
            relationshipsToUpsert.add(customerRec);
                
        } 

        if (relationshipsToUpsert.size() > 0) {
            upsert relationshipsToUpsert AIB_ExternalKey__c;
        }
        return relationshipsToUpsert;
    }
    
    /**********************************************************************************************
    * @Author:      Seevendra Singh 
    * @Date:        12/06/2019
    * @Description: This method creates street address from the provided details.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    private static String populateAddress(AIB_GB_BorrowerWrapper.Address address, String separator) {
        //i: populating address
        List<String> addressList = new List<String>();
        
        if (String.isNotBlank(address.addressLine1)) {
            addressList.add(address.addressLine1);
        }
        if (String.isNotBlank(address.addressLine2)) {
            addressList.add(address.addressLine2);
        }
        if (String.isNotBlank(address.addressLine3)) {
               addressList.add(address.addressLine3);
           }
        if (String.isNotBlank(address.addressLine4)) {
            addressList.add(address.addressLine4);
        }
        
        return String.join(addressList, separator);
    }
    
    /**********************************************************************************************
    * @Author:      Ajit Shetty  
    * @Date:        23/05/2019
    * @Description: This method contains the logic to check if the connection exists in salesforce,
    *               and accordingly create the connection record
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description] 
    * 				12/06/2019 - Issue Fix - There is some problem with the logic as the check is base 
    * 										 on household not per Household, Customer combination.  
    ***********************************************************************************************/
    private static void createConnections(
                                        List<Account> householdRecords, 
                                        List<Account> relationshipRecords
    ) {
        List<LLC_BI__Connection__c> connectionsToInsert = new List<LLC_BI__Connection__c>();
        Map<String, Set<String>> connectedRelationsMap = new Map<String, Set<String>>();
        Set<String> connectedRecIdSet;
        for (Account household : householdRecords) {
            connectedRecIdSet = new Set<String>();
            String  householdId = household.Id;
            connectedRelationsMap.put(householdId, connectedRecIdSet); 
        }
        
        LLC_BI__Connection_Role__c[] connectionRoles = AIB_GB_EntitySearchProvider.getHouseholdRoleId();
        String conRoleId = '';

        if (connectionRoles != null && connectionRoles.size() > 0 ) {
               conRoleId = connectionRoles[0].Id; 
        }
        LLC_BI__Connection__c[] connections = AIB_GB_EntitySearchProvider.getConnections(
                                                                                connectedRelationsMap.keySet()
                                                                            );
        for(LLC_BI__Connection__c connection : connections) {
            if(connectedRelationsMap.containsKey(connection.LLC_BI__Connected_To__c)) {
                Set<String> connectionSet = connectedRelationsMap.get(connection.LLC_BI__Connected_To__c);
                connectionSet.add(connection.LLC_BI__Connected_From__c);
            }
        }
        
        LLC_BI__Connection__c connection;
        for (Account household : householdRecords) {
            for(Account relationship : relationshipRecords){
                if (relationship.Type != AIB_Constants.RELATIONSHIP_HOUSEHOLD_TYPE 
                    && connectedRelationsMap.containsKey(household.Id) 
                    && !connectedRelationsMap.get(household.Id).contains(relationship.Id)
                ) {
                    
                    connection = new LLC_BI__Connection__c(LLC_BI__Connection_Role__c = conRoleId, 
                                                           LLC_BI__Connected_From__c = relationship.id, 
                                                           LLC_BI__Connected_To__c = household.id);
                    connectionsToInsert.add(connection);
                    
                }
            }
        }
        if(connectionsToInsert.size() > 0) {
            insert connectionsToInsert;
        }
    }
    
    /**********************************************************************************************
    * @Author:      Seevendra Singh  
    * @Date:        11/06/2019
    * @Description: This method contains the logic to create shareholder records. 
    * 				It first deletes the existing records and then insers new ones.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    private static void createShareholders(
                                    List<Account> borrowers, 
                                    List<AIB_GB_BorrowerWrapper.Customers> shareholders
    ) {
        AIB_Shareholder__c[] shareholdersToInsert = new List<AIB_Shareholder__c>();
        //i: this set is used to delete the existing shareholders record
        Set<String> borrowerIds = new Set<String>();
        String borrowerId;
        for (Account borrower : borrowers) {
            borrowerId = borrower.Id;
            borrowerIds.add(borrowerId);
        }
        
        AIB_Shareholder__c[] shareholdersToDelete = 
            AIB_GB_EntitySearchProvider.fetchRelatedShareholders(borrowerIds); 
        //i: delete the existing shareholder records
        delete shareholdersToDelete;
        
        AIB_Shareholder__c shareholderRec;
        for(Account borrower : borrowers) {
            for (AIB_GB_BorrowerWrapper.Customers shareholder : shareholders) {
                shareholderRec = 
                    new AIB_Shareholder__c( 
                        Name = shareholder.shareholderName,
                        AIB_PercentageShareholding__c = String.isNotBlank(shareholder.shareholderOwnershipPercentage) ? 
                        				Integer.valueof(shareholder.shareholderOwnershipPercentage) : null,
                        AIB_NumberOfShares__c = String.isNotBlank(shareholder.numberOfShares) ? 
                        				Integer.valueof(shareholder.numberOfShares) : null,
                        AIB_AIBParty__c = shareholder.isAibParty,
                        AIB_RelationshipName__c = borrower.Id
                    );
                shareholdersToInsert.add(shareholderRec);
            }
        }
        if(shareholdersToInsert.size() > 0) {
            insert shareholdersToInsert;
        }
    }
    
}