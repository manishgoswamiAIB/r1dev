public without sharing class csaLookupkeyGeneratorConstants {
    public static final String PROPERTY_CATEGORY = 'CSA Lookupkey Generator';
    public static final String PROPERTY_INSTALLEDKEY = 'nCino_Lookupkey_Generator';
    public static final String PROPERTY_INSTALLEDVALUE = 'true';

    public static final String ROUTE_NAME = 'LookupKey Generator';
    public static final String ROUTE_BODY = 'c__csaLookupkeyGenerator';
    public static final String ROUTE_APP = 'csa-lookupkey-generator';
    public static final String ROUTE_KEY = 'R-46irufkjh5nhfsd1';
    public static final String ROUTEGROUP_KEY = 'RG-97yiukhjjgfhdr5';
}