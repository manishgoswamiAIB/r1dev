/******************************************************************************************************
   * @Author:      Kuldeep Parihar  
    * @Date:        21/05/2018
    * @Description: FR1: Base Controller for BaseBorrowerComponent
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***************************************************************************************************/
public with sharing class AIB_GB_BaseBorrowerController {

    /**********************************************************************************************
    * @Author:      Kuldeep Parihar   
    * @Date:        21/05/2018
    * @Description: This is a method to get Current User Region. 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/

    @AuraEnabled
    public static String getCurrentUserRegion(){
       String userJurisdiction = AIB_Util.getCurrentUserRegion();
       AIB_BaseBorrowerWrapper.Result result = new AIB_BaseBorrowerWrapper.Result();
       if(!String.isBlank(userJurisdiction)){
          result.hasInfo = true;
          result.jurisdiction = userJurisdiction;
       }
       else{
          result.hasError = true;
          result.message = System.Label.AIB_GB_Label_JurisdictionError ; 
       }
       
    return JSON.serialize(result);    
      
    }
    
}