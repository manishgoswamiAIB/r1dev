global without sharing class csaClearLoanFieldsHandler extends nFORCE.ATriggerHandler {

    public override Boolean shouldExecute(nFORCE.IPipelineContext context){
        Boolean should = super.shouldExecute(context);
        if(should) {
            should = nFORCE.SystemProperties.getInstance().getPropertyAsBoolean(
                csaClearLoanFieldsConstants.PROPERTY_CATEGORY,
                csaClearLoanFieldsConstants.PROPERTY_CLEARKEY,
                false
            );
        }
        return should;
    }

    public override Boolean isDoubleFireSafe() {
        return true;
    }

    public override Type getType() {
        return csaClearLoanFieldsHandler.class;
    }

    public override void beforeInsert(List<sObject> objs) {
        List<String> copyFields = new List<String>();
        List<String> renModFields = new List<String>();
        List<String> modFields = new List<String>();
        List<String> renFields = new List<String>();
        List<LLC_BI__Loan__c> loans = (List<LLC_BI__Loan__c>)objs;

        for(Schema.FieldSetMember field : getCopyFieldset()) {
            copyFields.add(field.getFieldPath());
        }

        for(Schema.FieldSetMember field : getRenModFieldset()) {
            renModFields.add(field.getFieldPath());
        }

        for(Schema.FieldSetMember field : getModFieldset()) {
            modFields.add(field.getFieldPath());
        }

        for(Schema.FieldSetMember field : getRenFieldset()) {
            renFields.add(field.getFieldPath());
        }

        for(LLC_BI__Loan__c loan: loans) {
            if(loan.LLC_BI__Is_Copy__c == true) {
                for(String field: copyFields) {
                    if(loan.get(field) == true  || loan.get(field) == false) {
                        loan.put(field, false);
                    } else {
                        loan.put(field, null);
                    }
                }
            } else if(loan.LLC_BI__isRenewal__c == true) {
                for(String field: renModFields) {
                    if(loan.get(field) == true  || loan.get(field) == false) {
                        loan.put(field, false);
                    } else {
                        loan.put(field, null);
                    }
                }

                for(String field: renFields) {
                    if(loan.get(field) == true  || loan.get(field) == false) {
                        loan.put(field, false);
                    } else {
                        loan.put(field, null);
                    }
                }
            } else if(loan.LLC_BI__Is_Modification__c == true) {
                for(String field: renModFields) {
                    if(loan.get(field) == true  || loan.get(field) == false) {
                        loan.put(field, false);
                    } else {
                        loan.put(field, null);
                    }
                }

                for(String field: modFields) {
                    if(loan.get(field) == true  || loan.get(field) == false) {
                        loan.put(field, false);
                    } else {
                        loan.put(field, null);
                    }
                }
            }
        }
    }

    private static List<Schema.FieldSetMember> getCopyFieldset() {
        return SObjectType.LLC_BI__Loan__c.FieldSets.getMap().get(csaClearLoanFieldsConstants.FIELDSET_COPY).getFields();
    }

    private static List<Schema.FieldSetMember> getRenModFieldset() {
        return SObjectType.LLC_BI__Loan__c.FieldSets.getMap().get(csaClearLoanFieldsConstants.FIELDSET_RENMOD).getFields();
    }

    private static List<Schema.FieldSetMember> getModFieldset() {
        return SObjectType.LLC_BI__Loan__c.FieldSets.getMap().get(csaClearLoanFieldsConstants.FIELDSET_MOD).getFields();
    }

    private static List<Schema.FieldSetMember> getRenFieldset() {
        return SObjectType.LLC_BI__Loan__c.FieldSets.getMap().get(csaClearLoanFieldsConstants.FIELDSET_REN).getFields();
    }
}