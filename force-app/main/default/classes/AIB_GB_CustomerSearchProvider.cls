/**************************************************************************************************
* @Author:      Amol Patil  
* @Date:        20/05/2019
* @Description: This is a Customer search provider class to be used provide SOQL data 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
public with sharing class AIB_GB_CustomerSearchProvider {
/**************************************************************************************************
* @Author:      Amol Patil  
* @Date:        20/05/2019
* @Description: This is method is used to pass AIB_DropdownSetting_CMD__mdt values 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
    public static list<AIB_DropdownSetting_CMD__mdt> fetchDropDownValues() {
        try{
            
            AIB_DropdownSetting_CMD__mdt[] dropdownValues = [SELECT AIB_FieldType__c, 
                                                             Label__c,
                                                             Value__c,
                                                             ShortCode__c,
                                                             Active__c,AIB_DisplayOrder__c 
                                                             FROM AIB_DropdownSetting_CMD__mdt 
                                                             WHERE Active__c=TRUE 
                                                             AND ShortCode__c= :AIB_Constants.CONSTANT_SHORTCODE
                                                             ORDER BY AIB_DisplayOrder__c
                                                             LIMIT 5000];
            return dropdownValues;
        }
        catch(Exception exp){            
            AIB_GenericLogger.log(AIB_GB_CustomerSearchProvider.class.getName(),
                                  AIB_Constants.CONSTANT_FETCH_DROPDOWN_VALUES_METHOD, exp);
            return null;
        }
    }
    
}