/**********************************************************************************************
* @Author:      Swapnil Mohite
* @Date:        2/05/2019
* @Description: This is an Entity wrapper class to store response. 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/

public without sharing class AIB_GB_EntitiesResponseWrapper {

	public class EntityType {
		public String label {get;set;} 
		public String code {get;set;} 
		
	}
	
	public Embedded x_embedded {get;set;} 
	
	public class CreditCustomerList {
		public String entityId {get;set;} 
		public EntityType entityType {get;set;} 
		public String entityName {get;set;} 
		public String lastRefreshDate {get;set;} 
		public String lastUpdatedTimestamp {get;set;} 
		public String lastUpdatedStaffId {get;set;} 

		}
	
	public class Embedded {
		public List<CreditCustomerList> creditCustomerList {get;set;} 

	}


}