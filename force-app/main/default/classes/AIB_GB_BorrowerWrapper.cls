/**************************************************************************************************
* @Author:    Seevendra Singh   
* @Date:      07/06/2019
* @Description: Updated wrapper class for GetBorrower callout.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
public without sharing class AIB_GB_BorrowerWrapper {
    
    public without sharing class SourcesOfWealth {
        public Personal personal {get;set;} 
        public Personal business {get;set;} 
    }
    
    public String entityId {get;set;} 
    public EntityType entityType {get;set;} 
    public EntityType companyType {get;set;} 
    public String entityName {get;set;} 
    public Boolean isAibParty {get;set;} 
    public Boolean isSme {get;set;} 
    public EntityType recordWithBank {get;set;} 
    public EntityType primaryRelationshipManager {get;set;} 
    public Boolean isBusinessUnit {get;set;} 
    public TotalAssets totalAssets {get;set;} 
    public TotalAssets totalDebt {get;set;} 
    public TotalAssets tangibleNetWorth {get;set;} 
    public String dateOfTangibleNetWorth {get;set;} 
    public EntityType sourceOfTangibleNetWorth {get;set;} 
    public EntityType primaryBusinessSector {get;set;} 
    public String commentOnBusinessSector {get;set;} 
    public Boolean hasPreviousDeclines {get;set;} 
    public String commentOnDeclines {get;set;} 
    public String ownership {get;set;} 
    public Boolean isEntityOnCoins {get;set;} 
    public EntityType legalActionStatus {get;set;} 
    public EntityType legalActionType {get;set;} 
    public String legalActionDate {get;set;} 
    public Boolean isReferredToMabs {get;set;} 
    public String refreshTimestamp {get;set;} 
    public String lastRefreshDate {get;set;} 
    public String customerSinceDate {get;set;} 
    public EntityType taxConfirmationHeld {get;set;} 
    public String provisionLastUpdated {get;set;} 
    public String provisionUpdatedStaffId {get;set;} 
    public String provisionUpdatedStaffName {get;set;} 
    public Boolean isEntityPrivate {get;set;} 
    public Boolean isRestricted {get;set;} 
    public EntityType leverage {get;set;} 
    public String createdTimestamp {get;set;} 
    public String createdStaffId {get;set;} 
    public String lastUpdatedTimestamp {get;set;} 
    public String lastUpdatedStaffId {get;set;} 
    public EmbeddedCreditCustomerList embeddedCreditCustomerList {get;set;} 
    public EmbeddedCOINSList embeddedCOINSList {get;set;} 
    public EmbeddedShareholderList embeddedShareholderList {get;set;} 
    public BorrowerDemographics borrowerDemographics {get;set;} 
    public EmbeddedCustomerList embeddedCustomerList {get;set;} 
    public List<Links> x_links {get;set;} // in json: _links
    
    public without sharing class Address {
        public String addressLine1 {get;set;} 
        public String addressLine2 {get;set;} 
        public String addressLine3 {get;set;} 
        public String addressLine4 {get;set;} 
        public String postCode {get;set;} 
        public EntityType countyCode {get;set;} 
        
    }
    
    public without sharing  class CroDetails {
        public String companyNumber {get;set;} 
        public String legalName {get;set;} 
        public String registeredAddress {get;set;} 
        public EntityType companyStatusCode {get;set;} 
        public EntityType companyStatus {get;set;} 
        public String lastVerifiedDate {get;set;} 
        public Boolean croNumberVerified {get;set;} 
    }
    
    public without sharing class RiskBasedApproach {
        public OngoingDueDiligence ongoingDueDiligence {get;set;} 
        public EntityType flag {get;set;} 
        public SourcesOfWealth sourcesOfWealth {get;set;} 
    }
    
    
    public without sharing class PreviousAddressList {
        public String addressLine1 {get;set;} 
        public String addressLine2 {get;set;} 
        public String addressLine3 {get;set;} 
        public String addressLine4 {get;set;} 
        public String postCode {get;set;} 
        public AuditData auditData {get;set;} 
    }
    
    public without sharing class Embedded {
        public List<AccountList> accountList {get;set;} 
        public List<Customers> customers {get;set;}         
        public List<PreviousAddressList> previousAddressList {get;set;} 
    }
    
    public without sharing class PersonalProfileSummary {
        public String title {get;set;} 
        public String firstName {get;set;} 
        public String middleInitial {get;set;} 
        public String lastName {get;set;} 
        public String nameSuffix {get;set;} 
        public String dateOfBirth {get;set;} 
        public EntityType gender {get;set;} 
        public EntityType countryOfResidence {get;set;} 
        public EntityType nationality {get;set;} 
        public String ppsn {get;set;} 
        public EntityType maritalStatus {get;set;} 
        public TotalAssets turnover {get;set;} 
        public String turnoverDate {get;set;} 
        public String asAtDate {get;set;} 
        public Indicators indicators {get;set;} 
        public Employment employment {get;set;} 
        public HomeAndFamily homeAndFamily {get;set;} 
    }
    
    public without sharing class TotalAssets {
        public String value {get;set;} 
        public String x_currency {get;set;}
    }
    
    public without sharing class BusinessDemographics {
        public Boolean articlesOfMemorandumInOrder {get;set;} 
        public String auditorName {get;set;} 
        public Boolean boughtViaFormationAgent {get;set;} 
        public Boolean customerIsTrust {get;set;} 
        public Boolean complexStructure {get;set;} 
        public EntityType countryOfIncorporation {get;set;} 
        public String formationAgentName {get;set;} 
        public Boolean highCashBusiness {get;set;} 
        public TotalAssets highRiskCountryValue {get;set;} 
        public EntityType otherIdentifierType {get;set;} 
        public EntityType otherIdentifierIdentifier {get;set;} 
        public Boolean otherIdentifierVerified {get;set;} 
        public EntityType riskBusinessType {get;set;} 
        public EntityType riskBusinessSubType {get;set;} 
        public Boolean simplifiedDueDiligence {get;set;} 
        public Boolean specialPurposeVehicle {get;set;} 
        public String tradingCertificateNumber {get;set;} 
        public Boolean principalDetailUpdated {get;set;} 
        public CompanyType companyType {get;set;} 
        public CroDetails croDetails {get;set;} 
    }
    
    public without sharing class EmbeddedCOINSList {
        public Embedded x_embedded {get;set;} // in json: _embedded
    }
    
    public without sharing class AccountList {
        public String customerID {get;set;} 
    }
    
    public without sharing class FinancialStatus {
        public String fsgHolisticFlag {get;set;} 
        public String intentToRepay {get;set;} 
        public String privateBanking {get;set;} 
        public String personalInsolvencyFlag {get;set;} 
    }
    
    
    public without sharing class CcrIdentifiers {
        public EntityType country {get;set;} 
        public String number_Z {get;set;} // in json: number
        public String idType {get;set;} 
        public String status {get;set;} 
    }
    
    public without sharing class EmbeddedShareholderList {
        public Embedded x_embedded {get;set;} // in json: _embedded
    }
    
    public without sharing class HomeAndFamily {
        public EntityType residentialStatus {get;set;} 
        public String yearsAtCurrentAddress {get;set;} 
        public TotalAssets houseValueAmount {get;set;} 
        public String houseValueDate {get;set;} 
        public String numDependents {get;set;} 
        public String familyAgeRangeFrom {get;set;} 
        public String familyAgeRangeTo {get;set;} 
    }
    
    public without sharing class ContactDetails {
        public PhoneAndEmail phoneAndEmail {get;set;} 
        public Address address {get;set;} 
    }
    
    public without sharing class Employment {
        public EntityType employmentType {get;set;} 
        public String jobDescription {get;set;} 
        public EntityType employmentStatus {get;set;} 
        public String employerName {get;set;} 
        public String seventyHoursPermanent {get;set;} 
        public EntityType employerBusiness {get;set;} 
        public EntityType employeeClassification {get;set;} 
        public EntityType businessCategory {get;set;} 
        public EntityType employeeCategory {get;set;} 
        public EntityType employerBusinessSector {get;set;} 
        public EntityType employerBusinessType {get;set;} 
        public String yearsWithCurrentEmployer {get;set;} 
        public Boolean contractEmployment {get;set;} 
        public Boolean employedForSixMonths {get;set;} 
        public String contractExpiryDate {get;set;} 
        public Boolean pensionable {get;set;} 
        public String yearlyGrossIncome {get;set;} 
        public String paymentFrequency {get;set;} 
        public Boolean payedDirectly {get;set;} 
        public String salaryEffectiveDate {get;set;} 
        public TotalAssets netMonthlySalary {get;set;} 
        public Boolean partTimeHours {get;set;} 
    }
    
    public without sharing class MarketingPreference {
        public EntityType preferredDay {get;set;} 
        public AuditData preferredDayAudit {get;set;} 
        public EntityType preferredTime {get;set;} 
        public AuditData preferredTimeAudit {get;set;} 
        public EntityType preferredMethod {get;set;} 
        public Boolean allowEmail {get;set;} 
        public AuditData allowEmailAudit {get;set;} 
        public Boolean allowMail {get;set;} 
        public AuditData allowMailAudit {get;set;} 
        public Boolean allowPhone {get;set;} 
        public AuditData allowPhoneAudit {get;set;} 
    }
    
    public without sharing class SelfService {
        public String registrationId {get;set;} 
        public String businessBankingRegistrationId {get;set;} 
        public String businessBankingRegistrationDate {get;set;} 
    }
    
    public without sharing class Preferences {
        public Boolean voiceBiometrics {get;set;} 
        public Boolean messageCentre {get;set;} 
        public Boolean consumerProtection {get;set;} 
    }
    
    public without sharing class CustomerFlags {
        public EntityType marketSegmentCode {get;set;} 
        public EntityType creditGradeCode {get;set;} 
        public EntityType corporateRetailClassificationCode {get;set;} 
        public String customerSinceDate {get;set;} 
        public String assignedStaffNumber {get;set;} 
        public String assignedPerson {get;set;} 
        public EntityType customerStatus {get;set;} 
        public Boolean isMergePending {get;set;} 
        public Boolean isPotentialMerge {get;set;} 
        public String lastVerifiedDate {get;set;} 
    }
    
    public without sharing class Indicators {
        public EntityType treatmentStrategy {get;set;} 
        public EntityType customerType {get;set;} 
        public Boolean gdprFlag {get;set;} 
        public Boolean politicallyExposedPerson {get;set;} 
        public Boolean highCashBusiness {get;set;} 
        public Boolean mergePending {get;set;} 
        public EntityType marketSegmentCode {get;set;} 
        public String badDebtDate {get;set;} 
        public String smeFlag {get;set;} 
        public String financialDifficultyFlag {get;set;} 
        public String smeSize {get;set;} 
        public String notCooperatingFlag {get;set;} 
        public EntityType creditGradeCode {get;set;} 
        public String customerSinceDate {get;set;} 
        public EntityType customerStatus {get;set;} 
        public RiskBasedApproach riskBasedApproach {get;set;} 
        public Preferences preferences {get;set;} 
        public AssistedDecisionMakingAct assistedDecisionMakingAct {get;set;} 
        public FinancialStatus financialStatus {get;set;} 
    }
    
    public without sharing class PhoneAndEmail {
        public HomePhone homePhone {get;set;} 
        public HomePhone workPhone {get;set;} 
        public HomePhone mobilePhone {get;set;} 
        public String workEmail {get;set;} 
        public String homeEmail {get;set;} 
    }
    
    public without sharing class Personal {
        public String value {get;set;} 
    }
    
    public without sharing class FarmingDemographics {
        public EntityType class_Z {get;set;} // in json: class
        public EntityType status {get;set;} 
        public EntityType programme {get;set;} 
        public EntityType reps {get;set;} 
        public String auditorsName {get;set;} 
        public TotalAssets value {get;set;} 
        public String valueDate {get;set;} 
        public String coopName {get;set;} 
        public String lastVisitDate {get;set;} 
        public String acreageOwned {get;set;} 
        public String acreageRent {get;set;} 
        public String acreageLet {get;set;} 
        public String acreageForestry {get;set;} 
        public String acreageTillage {get;set;} 
        public String acreagePasture {get;set;} 
        public String acreageOther {get;set;} 
        public AuditData audit {get;set;} 
    }
    
    public without sharing class OngoingDueDiligence {
        public String lastUpdatedDate {get;set;} 
    }
    
    public without sharing class CustomerProfile {
        public String fullName {get;set;} 
        public EntityType customerType {get;set;} 
        public String personAssigned {get;set;} 
        public String assignedStaffMemberID {get;set;} 
        public String contactPerson {get;set;} 
        public String salutation {get;set;} 
        public CcrIdentifiers ccrIdentifiers {get;set;} 
    }
    
    public without sharing class EmbeddedPreviousAddresses {
        public Embedded x_embedded {get;set;} // in json: _embedded
        public List<Links> x_links {get;set;} // in json: _links
    }
    
    public without sharing class CompanyType {
        public EntityType category {get;set;} 
        public String companyTypeCode {get;set;} 
        public String subcategory {get;set;} 
    }
    
    public without sharing class AuditData {
        public String lastUpdatedBy {get;set;} 
        public String lastUpdated {get;set;} 
        public String lastUpdatedChannel {get;set;} 
        public String lastConfirmedBy {get;set;} 
        public String lastConfirmed {get;set;} 
        public String lastConfirmedChannel {get;set;} 
    }
    
    public without sharing class EmbeddedCreditCustomerList {
        public Embedded x_embedded {get;set;} // in json: _embedded
    }
    
    public without sharing class EntityType {
        public String label {get;set;} 
        public String code {get;set;} 
    }
    
    public without sharing class AssistedDecisionMakingAct {
        public String status {get;set;} 
        public String summary {get;set;} 
    }
    
    public without sharing class Statuses {
        public CriminalJusticeAct criminalJusticeAct {get;set;} 
    }
    
    public without sharing class Customers {
        //i: COINS related details
        public String coinsGroupCode {get;set;} 
        public Integer coinsNumber {get;set;}
        
        //i: shareholders related details
        public String shareholderName {get;set;} 
        public String shareholderOwnershipPercentage {get;set;} 
        public Boolean isAibParty {get;set;} 
        public String numberOfShares {get;set;} 
        
        //i: customers related details
        public String customerID {get;set;} 
        public CustomerProfile customerProfile {get;set;} 
        public PersonalProfileSummary personalProfileSummary {get;set;} 
        public Statuses statuses {get;set;} 
        public CustomerFlags customerFlags {get;set;} 
        public ContactDetails contactDetails {get;set;} 
        public SelfService selfService {get;set;} 
        public EmbeddedPreviousAddresses embeddedPreviousAddresses {get;set;} 
        public FarmingDemographics farmingDemographics {get;set;} 
        public BusinessDemographics businessDemographics {get;set;} 
        public MarketingPreference marketingPreference {get;set;} 
        public List<Links> x_links {get;set;} // in json: _links
    }
    
    public without sharing class HomePhone {
        public String prefix {get;set;} 
        public String number_Z {get;set;} // in json: number
        public String fullNumber {get;set;} 
    }
    
    public without sharing class BorrowerDemographics {
        public String contactPerson {get;set;} 
        public EntityType occupationTypeCode {get;set;} 
        public String businessRegistrationNumber {get;set;} 
        public String auditorsName {get;set;} 
        public String mainBanker {get;set;} 
        public String businessFaxArea {get;set;} 
        public String businessFaxNumber {get;set;} 
        public String businessTelephoneNumber {get;set;} 
        public TotalAssets businessVolumeAmount {get;set;} 
        public Integer employeeTotalNumber {get;set;} 
        public TotalAssets businessProfitLossAmount {get;set;} 
        public String businessProfitLossDate {get;set;} 
        public TotalAssets noMoProfitLoss {get;set;} 
        public Boolean directorsBorrower {get;set;} 
        public TotalAssets directorsBorrowerPower {get;set;} 
        public Boolean hasCompanyBorrower {get;set;} 
        public TotalAssets companyBorrowerPower {get;set;} 
        public Boolean isBusinessPremisesOwned {get;set;} 
        public Boolean isBusinessPremisesLeased {get;set;} 
        public Boolean isBusinessPremisesRented {get;set;} 
        public EntityType businessRegisterInd {get;set;} 
        public String yearsInBusiness {get;set;} 
        public Boolean hasMemoArticles {get;set;} 
        public String taxCertificateRecordDate {get;set;} 
        public String accountsAsAtDate {get;set;} 
        public String businessDescription {get;set;} 
        public Integer numberOfOutlets {get;set;} 
        public String websiteAddress {get;set;} 
        public String periodEndDate {get;set;} 
        public TotalAssets drawings {get;set;} 
        public TotalAssets stock {get;set;} 
        public TotalAssets debtors {get;set;} 
        public TotalAssets landBuildings {get;set;} 
        public TotalAssets machineryEq {get;set;} 
        public TotalAssets otherAssets {get;set;} 
        public TotalAssets creditors {get;set;} 
        public TotalAssets taxLiabilities {get;set;} 
        public TotalAssets borrowings {get;set;} 
        public TotalAssets otherLiabilities {get;set;} 
        public Integer lastMaintainedId {get;set;} 
        public String lastMaintainedDate {get;set;} 
        public EntityType sourceOfInfo {get;set;} 
        public TotalAssets grossProfit {get;set;} 
        public TotalAssets borrowerODraft {get;set;} 
        public TotalAssets borrowerLessThan12m {get;set;} 
        public TotalAssets otherAssetsShres {get;set;} 
        public TotalAssets assetsInvestProp {get;set;} 
        public TotalAssets otherAssetsDep {get;set;} 
        public String riskCountryCode {get;set;} 
        public EntityType riskBusTypeCode {get;set;} 
        public Boolean hasRiskCompStructure {get;set;} 
        public String incorpCountryCode {get;set;} 
        public String createdTimestamp {get;set;} 
        public String createdStaffId {get;set;} 
        public String lastUpdatedTimestamp {get;set;} 
        public String lastUpdatedStaffId {get;set;} 
        public String shortName {get;set;} 
    }
    
    public without sharing class Links {
        public String rel {get;set;} 
        public String href {get;set;} 
    }
    
    public without sharing class CriminalJusticeAct {
        public String status {get;set;} 
        public String documentSystemID {get;set;} 
        public String documentSystem {get;set;} 
    }
    
    public without sharing class EmbeddedCustomerList {
        public Embedded x_embedded {get;set;} // in json: _embedded
    }
}