/**************************************************************************************************
* @Author:      Manish Goswami  
* @Date:        31/05/2019
* @PageCode:    EXP
* @CurErrCode:  EXP-003
* @Description: This test class for Util class. 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
@isTest(SeeAllData=false)
public class AIB_Util_Test {
    
    /**********************************************************************************************
    * @Author:     Manish Goswami   
    * @Date:        20/05/2019
    * @Description: This method will return juridiction of the user. 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    private static testmethod void getCurrentUserRegionTest(){
        Test.startTest();
        String userJurisdiction = AIB_Util.getCurrentUserRegion();
        Test.stopTest();
        System.assertEquals([SELECT AIB_Jurisdiction__c 
                             FROM User 
                             WHERE Id =: UserInfo.getUserId() 
                             LIMIT 1].AIB_Jurisdiction__c, userJurisdiction);
    }
    
    /**********************************************************************************************
    * @Author:     Manish Goswami  
    * @Date:        30/05/2019
    * @Description: This method will test the replace resrved key words from JSON String
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    private static testmethod void replaceReservedKeywordsTest() {
          String Json = '{ "_embedded": "Test", "X_embedded": "Tests"}';
        
          Test.startTest();
          String S = AIB_Util.replaceReservedKeywords(Json);
          System.assertEquals(false,String.isBlank(S));    
          Test.stopTest();
        
    }

}