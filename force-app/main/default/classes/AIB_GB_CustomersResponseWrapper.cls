/**************************************************************************************************
* @Author:      Amol Patil  
* @Date:        20/05/2019    
* @Description: This is a Customers Response Wrapper to Parse response
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
public class AIB_GB_CustomersResponseWrapper {
	public Embedded x_embedded {get;set;} 
	public class Summaries {
        public String customerID {get;set;}
		public String fullName {get;set;} 
		public String dateOfBirth {get;set;} 
		public String customerType {get;set;} 
		public String addressLine1 {get;set;} 
		public String addressLine2 {get;set;} 
		public String addressLine3 {get;set;} 
		public String addressLine4 {get;set;} 
        public String fullAddress {get;set;} 
 	}
	public class Embedded {
		public List<Summaries> summaries {get;set;} 

	}
}