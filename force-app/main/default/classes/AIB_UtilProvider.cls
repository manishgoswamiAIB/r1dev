/**************************************************************************************************
* @Author:      Manish GOswami  
* @Date:        20/05/2019
* @PageCode:    EXP
* @CurErrCode:  EXP-003
* @Description: This class is prvoide class contain list of methods which will return value. 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
public with sharing class AIB_UtilProvider {
    
    
   /**********************************************************************************************
    * @Author:      Manish Goswami  
    * @Date:        20/05/2019
    * @Description: This method will return array list of user. 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static User[] getCurrentUserDetails(){      
        
      User[] userList = [
            SELECT AIB_Jurisdiction__c, 
            	  Id
            FROM User 
            WHERE Id = : UserInfo.getUserId()
            LIMIT 1
        ];
           
        return userList;
    }
   /**********************************************************************************************
    * @Author:      Manish Goswami  
    * @Date:        30/05/2019
    * @Description: This method will return meta data recored. 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/ 
    public static AIB_JsonKeywords_CMD__mdt[] getmetadatarecords() {
       AIB_JsonKeywords_CMD__mdt[] keywords = [
            SELECT AIB_Keyword__c, 
            	AIB_Replacement__c 
            FROM AIB_JsonKeywords_CMD__mdt 
            LIMIT 1000
        ];  
        return keywords;
    }
   
}