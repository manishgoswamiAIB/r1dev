public with sharing class csaClearLoanFieldsConstants {
    public static final String PROPERTY_CATEGORY = 'CSA Clear Loan Fields';
    public static final String PROPERTY_INSTALLEDKEY = 'nCino_Clear_Loan_Fields';
    public static final String PROPERTY_INSTALLEDVALUE = 'true';

    public static final String PROPERTY_CLEARKEY = 'CLF_Enabled';
    public static final String PROPERTY_CLEARVALUE = 'true';

    public static final String FIELDSET_COPY = 'ClearFields_Copy';
    public static final String FIELDSET_RENMOD = 'ClearFields_Renew_Mod';
    public static final String FIELDSET_MOD = 'ClearFields_Mod';
    public static final String FIELDSET_REN = 'ClearFields_Renew';
}