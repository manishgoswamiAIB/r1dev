/**************************************************************************************************
* @Author:      Poorvy Sharma
* @Date:        27/05/2019
* @Description: This is a test class for AIB_PP_DataProtectionQuesController class.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/

@isTest (SeeAllData = false)
public without sharing class AIB_PP_DataProtectionQuesController_Test {
    private static final String PROFILE_NAME ='AIB Business Super User';
    private static final String FIRST_NAME = 'Fname';
    private static final String LAST_NAME = 'Lname';
    private static final String EMAIL = 'tuser1234@test.org';
    private static final String USER_NAME1 = 'tuser1212@test.org';
    private static final String USER_NAME2 = 'tuser1213@test.org';
    private static final String USER_NAME3 = 'tuser1214@test.org';
    private static final String ALIASROI = 'tuser1';
    private static final String ALIASGB = 'tuser2';
    private static final String ALIASUS = 'tuser3';
    private static final String TIME_ZONE_SID_KEY = 'America/Los_Angeles';
    private static final String LOCALE_SID_KEY = 'en_US';
    private static final String LANGUAGE_LOCALE_KEY = 'en_US';
    private static final String EMAIL_ENCODING_KEY = 'UTF-8';
    private static final String AIB_JURISDICTION_ROI = 'ROI';
    private static final String AIB_JURISDICTION_GB = 'GB';
    private static final String AIB_JURISDICTION_FT = 'FT';
    private static final String FORMTYPE = 'Picklist';
    private static final String LABEL = 'CCR Header';
    private static final String READ_ONLY_TEXT = 'CCR Credit Register VVVV';
    private static final String GROUP_NAME_ROI = 'Data Protection Requirement/CJA ROI';    
    private static final String GROUP_NAME_GB  = 'Data Protection Requirement/CJA GB';
    private static final String GROUP_NAME_FT  = 'Data Protection Requirement/CJA FT';
    private static final String APP_NAME_ROI   = 'pp-dataProtectionRequirementROI';
    private static final String APP_NAME_GB    = 'pp-dataProtectionRequirementGBFT';
    private static final String APP_NAME_FT    = 'pp-dataProtectionRequirementFT';
    private static final String APP_NAME       = 'TestApp'; 	
    private static final String APP_TYPE		  = 'New Funds Sought';
    private static final String VALUES1 = 'I can confirm that I have read both of the above';
    private static final String VALUES2 = 'notifications to the customer';
    private static final String RELEVANT_CREDIT_AUTH	= 'ROI_IDL';
    private static final String LEVEL = '1';
    
    
    /**************************************************************************************************
* @Author:      Manisha Yadav
* @Date:        23/05/2019
* @Description: This method is used to setup data for test class
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/    
    @testSetup    
    private static void setup(){
        Profile profileId =  [Select Id from Profile where Name =: PROFILE_NAME LIMIT 1] ;
        List<User> testUser = new List<User>();
        User user1 = new User(FirstName = FIRST_NAME,
                              LastName = LAST_NAME,
                              email = EMAIL,
                              Username = USER_NAME1,
                              Alias =ALIASROI,
                              ProfileId = profileId.id,
                              TimeZoneSidKey=TIME_ZONE_SID_KEY,
                              LocaleSidKey=LOCALE_SID_KEY,
                              LanguageLocaleKey=LANGUAGE_LOCALE_KEY,
                              EmailEncodingKey=EMAIL_ENCODING_KEY,
                              AIB_Jurisdiction__c=AIB_JURISDICTION_ROI);           
        testUser.add(user1);
        
        User user2 = new User(FirstName = FIRST_NAME,
                              LastName = LAST_NAME,
                              email = EMAIL,
                              Username = USER_NAME2,
                              Alias =ALIASGB,
                              ProfileId = profileId.id,
                              TimeZoneSidKey=TIME_ZONE_SID_KEY,
                              LocaleSidKey=LOCALE_SID_KEY,
                              LanguageLocaleKey=LANGUAGE_LOCALE_KEY,
                              EmailEncodingKey=EMAIL_ENCODING_KEY,
                              AIB_Jurisdiction__c=AIB_JURISDICTION_GB);           
        testUser.add(user2);   
        User user3 = new User(FirstName = FIRST_NAME,
                              LastName = LAST_NAME,
                              email = EMAIL,
                              Username = USER_NAME3,
                              Alias =ALIASUS,
                              ProfileId = profileId.id,
                              TimeZoneSidKey=TIME_ZONE_SID_KEY,
                              LocaleSidKey=LOCALE_SID_KEY,
                              LanguageLocaleKey=LANGUAGE_LOCALE_KEY,
                              EmailEncodingKey=EMAIL_ENCODING_KEY,
                              AIB_Jurisdiction__c=AIB_JURISDICTION_FT);           
        testUser.add(user3);     
        
        Database.insert(testUser);
        
        nFORCE__Group__c groupRecord = new nFORCE__Group__c();
        groupRecord.Name = GROUP_NAME_ROI;
        groupRecord.nFORCE__App__c = APP_NAME_ROI;
        groupRecord.nFORCE__Is_Questionnaire__c = TRUE;
        Database.insert(groupRecord); 
        
        nFORCE__Group__c groupRecord1 = new nFORCE__Group__c();
        groupRecord1.Name = GROUP_NAME_GB;
        groupRecord1.nFORCE__App__c = APP_NAME_GB;
        groupRecord1.nFORCE__Is_Questionnaire__c = TRUE;
        Database.insert(groupRecord1); 

        nFORCE__Group__c groupRecord2 = new nFORCE__Group__c();
        groupRecord2.Name = GROUP_NAME_FT;
        groupRecord2.nFORCE__App__c = APP_NAME_FT;
        groupRecord2.nFORCE__Is_Questionnaire__c = TRUE;
        Database.insert(groupRecord2); 
        
        
        nFORCE__Question__c question1 = new nFORCE__Question__c();
        
        question1.nFORCE__Group__c = groupRecord.Id;
        question1.nFORCE__Form_Type__c = FORMTYPE;
        question1.nFORCE__Label__c = LABEL ;
        question1.AIB_ReadOnlyText__c = READ_ONLY_TEXT;                
        Database.insert(question1);           
        
        nFORCE__Option__c option1 = new nFORCE__Option__c();
        option1.nFORCE__Question__c = question1.Id;
        option1.nFORCE__Label__c = VALUES2;
        
        Database.insert(option1);
        
        
        LLC_BI__Product_Package__c appRecord1 = new LLC_BI__Product_Package__c();
        appRecord1.Name = APP_NAME;
        appRecord1.AIB_ApplicationType__c = APP_TYPE;
        appRecord1.Jurisdiction__c = AIB_JURISDICTION_ROI;
        appRecord1.AIB_RelevantCreditAuthority__c = RELEVANT_CREDIT_AUTH;
        appRecord1.AIB_Level__c  = LEVEL;
        
        Database.insert(appRecord1);
    }
    
    /***********************************************************************************************
* @Author:      Poorvy Sharma 
* @Date:        27/05/2019
* @Description: Method to test User's Jurisdiction (ROI)
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/    
    private static testmethod void currentUserRegionROITest(){
        String questGroupName ='';
        User user = [Select Id, FirstName, LastName, Email FROM User where Username = 'tuser1212@test.org'
                     LIMIT 1];        
        System.runAs(user){
            Test.startTest();            
            questGroupName = AIB_PP_DataProtectionQuesController.getCurrentUserRegion();                
            Test.stopTest();
            system.assertEquals('Data Protection Requirement/CJA ROI', questGroupName);            
        }        
    }
    
    /***********************************************************************************************
* @Author:      Poorvy Sharma 
* @Date:        27/05/2019
* @Description: Method to test User's Jurisdiction (GB)
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/    
    
    private static testmethod void currentUserRegionGBTest(){
        String questGroupName ='';
        User user = [Select FirstName,LastName,Email FROM User where Username ='tuser1213@test.org' 
                     LIMIT 1];        
        System.runAs(user){
            Test.startTest();            
            questGroupName = AIB_PP_DataProtectionQuesController.getCurrentUserRegion();                         
            Test.stopTest();            
            system.assertEquals('Data Protection Requirement/CJA GB', questGroupName);
            
        }                    
    }
    /***********************************************************************************************
* @Author:      Poorvy Sharma 
* @Date:        27/05/2019
* @Description: Method to test User's Jurisdiction (FT)
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/    
    
    private static testmethod void currentUserRegionFTTest(){
        String questGroupName ='';
        User user = [Select FirstName,LastName,Email FROM User where Username ='tuser1214@test.org' 
                     LIMIT 1];        
        System.runAs(user){
            Test.startTest();            
            questGroupName = AIB_PP_DataProtectionQuesController.getCurrentUserRegion();                         
            Test.stopTest();            
            system.assertEquals('Data Protection Requirement/CJA FT', questGroupName);
            
        }                    
    }
    
    
    
    /***********************************************************************************************
* @Author:      Poorvy Sharma 
* @Date:        27/05/2019
* @Description: Method to test if Questions/Options are received correctly as per jurisdiction
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/    
    
    private static testmethod void questionsAndOptionsTest(){
        
        Test.startTest();      
        String dataReceived = AIB_PP_DataProtectionQuesController.getQuestionsAndOptions
            (AIB_CONSTANTS.DATA_PROTECTION_ROI);                                         
        Test.stopTest();         
        system.assertNotEquals(null, dataReceived);                                     
    }


    /***********************************************************************************************
* @Author:      Poorvy Sharma 
* @Date:        27/05/2019
* @Description: Method to test if Questions/Options are not available
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/    
    
    private static testmethod void noquestionsAndOptionsTest(){
        nFORCE__Question__c[] questions = [SELECT id, nFORCE__Order__c, nFORCE__Form_Type__c, 
            AIB_ReadOnlyText__c, 
            (SELECT Id, 
             nFORCE__Label__c 
             FROM nFORCE__Options__r 
             ORDER BY nFORCE__Order__c ASC LIMIT 1)
            FROM nFORCE__Question__c            
            LIMIT 1];
        Delete questions;
        Test.startTest();      
        String dataReceived = AIB_PP_DataProtectionQuesController.getQuestionsAndOptions
            (AIB_CONSTANTS.DATA_PROTECTION_ROI);                                         
        Test.stopTest();         
        system.assertEquals('[]', dataReceived);                                     
    }
    
    
    /***********************************************************************************************
* @Author:      Poorvy Sharma 
* @Date:        27/05/2019
* @Description: Method to test if Answers/Options are processed
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/    
    
    private static testmethod void processAnswersTest(){  
        String dataReceived;        
        List<nFORCE__Option__c> selected_values1 = [SELECT Id from nFORCE__Option__c LIMIT 1];
        List<String> selected_values = new List<String>();
        selected_values.add(selected_values1[0].Id);                
        LLC_BI__Product_Package__c objectData = [SELECT Id from LLC_BI__Product_Package__c LIMIT 1];
           
        Test.startTest();
        if (objectData != null){
            dataReceived = AIB_PP_DataProtectionQuesController.processAnswers(objectData.Id,AIB_CONSTANTS.DATA_PROTECTION_ROI,selected_values);                                      
        }
        Test.stopTest();         
        system.assertEquals('Completed', dataReceived);                                     
    }
    
}