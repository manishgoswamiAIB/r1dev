/**************************************************************************************************
* @Author:      Pradeep Verma
* @Date:        23/05/2019
* @Description: This is a test class for AIB_GB_CustomerSearchController class.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
@isTest(SeeAllData=false)
public class AIB_GB_CustomerSearchController_Test {    
    @testSetup   
    private static void setup () {
        Profile profileId =  [SELECT Id 
                              FROM Profile 
                              WHERE Name = :AIB_Constants.CONSTANT_PROFILE_NAME 
                              LIMIT 1] ;
        List<User> testUser = new List<User>();
        User user = new User(FirstName = AIB_Constants.CONSTANT_FIRSTNAME,
                             LastName = AIB_Constants.CONSTANT_LASTNAME,
                             email = AIB_Constants.CONSTANT_USER_EMAIL,
                             Username = AIB_Constants.CONSTANT_USERNAME,
                             Alias =AIB_Constants.CONSTANT_ALIAS,
                             ProfileId = profileId.id,
                             TimeZoneSidKey=AIB_Constants.CONSTANT_TIMEZONESIDKEY,
                             LocaleSidKey=AIB_Constants.CONSTANT_LOCALESIDKEY,
                             LanguageLocaleKey=AIB_Constants.CONSTANT_LANGUAGE_LOCALE_KEY,
                             EmailEncodingKey=AIB_Constants.CONSTANT_EMAIL_ENCODING_KEY,
                             AIB_Jurisdiction__c =AIB_Constants.CONSTANT_JURISDICTION);
                
        testUser.add(user);
        database.insert(testUser);
        
    }
    /**************************************************************************************************
    * @Author:      Pradeep Verma
    * @Date:        23/05/2019
    * @Description: This is a test method for Customer Result Response 200.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***************************************************************************************************/    
    private static testmethod void customerResultTestResponseCode200() {
        User user = [SELECT
                     FirstName, 
                     LastName, 
                     Email
                     FROM User 
                     WHERE email =: AIB_Constants.CONSTANT_USER_EMAIL
                     LIMIT 1];
        
        System.runAs(user){
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('AIB_GB_CustomerResponseCode200_Test');
            mock.setStatusCode(AIB_Constants.CALLOUT_STATUS_CODE_SUCCESS);
            mock.setStatus(AIB_Constants.CALLOUT_STATUS_SUCCESS);
            mock.setHeader('Content-Type', 'application/json');
            Test.setMock(HttpCalloutMock.class, mock);
            
            Test.startTest();
            AIB_GB_CustomerSearchWrapper.Parameter  parameter = new AIB_GB_CustomerSearchWrapper.Parameter();
            parameter.customerType ='p';
            parameter.name= 'name';
            parameter.dateOfBirth ='3/23/1992';
            parameter.NSC ='asd';
            parameter.contactNumberFirst ='123432';
            parameter.contactNumberSecond ='2312436';
            parameter.cardType ='Visa';
            parameter.cardNumber ='21232314';
            parameter.selfServiceNo ='521562';
            parameter.county ='135';
            parameter.postCode ='143001';
            parameter.addressLineOne ='address line 1';
            parameter.addressLineTwo ='address line 2';
            parameter.addressLineThree ='address line 3';
            parameter.addressLineFour ='address line 4';
            parameter.integrationSettingName='GetCustomerWith200CodeFromCallout_Test';
            parameter.region='ROI';
            
            String customerSearchParams = JSON.serialize(parameter);
            system.debug('customerSearchParams'+customerSearchParams);
            String jsonString = AIB_GB_CustomerSearchController.getCustomerResult(customerSearchParams);
            String jsonStringSecond = AIB_GB_CustomerSearchController.getCustomerResult(NULL);
            AIB_GB_CustomerSearchController.getPicklistValues(); 
            Test.stopTest();         
            System.assertEquals(false,String.isBlank(jsonString));
            
        }
        
    }
 
      /**************************************************************************************************
       * @Author:      Pradeep Verma
       * @Date:        23/05/2019
       * @Description: This is a test method for Customer Result Response 404.
       * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
       ***************************************************************************************************/    
       private static testmethod void customerResultTestResponseCode404() {
        User user = [Select
                     FirstName, 
                     LastName, 
                     Email
                     FROM User where email =: AIB_Constants.CONSTANT_USER_EMAIL LIMIT 1];
        
        System.runAs(user){
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('AIB_GB_CustomerResponseCode404_Test');
            mock.setStatusCode(AIB_Constants.CALLOUT_STATUS_CODE_CUSTOMERNOTFOUND);
            mock.setStatus(AIB_Constants.CALLOUT_STATUS_CUSTOMERNOTFOUND);
            mock.setHeader('Content-Type', 'application/json');
            
            Test.setMock(HttpCalloutMock.class, mock);
            
            Test.startTest();
            AIB_GB_CustomerSearchWrapper.Parameter  parameter = new AIB_GB_CustomerSearchWrapper.Parameter();
            parameter.customerType =AIB_Constants.CONSTANT_CUSTOMERTYPE;
            parameter.name= AIB_Constants.CONSTANT_CUSTOMERNAME;
            parameter.dateOfBirth =AIB_Constants.CONSTANT_DATE_OF_BIRTH;
            parameter.NSC =AIB_Constants.CONSTANT_NSC;
            parameter.contactNumberFirst =AIB_Constants.CONSTANT_CONTACT_NUMBER_FIRST;
            parameter.contactNumberSecond =AIB_Constants.CONSTANT_CONTACT_NUMBER_SECOND;
            parameter.cardType =AIB_Constants.CONSTANT_CARDTYPE;
            parameter.cardNumber =AIB_Constants.CONSTANT_CARDNUMBER;
            parameter.selfServiceNo =AIB_Constants.CONSTANT_SELF_SERVICE_NUMBER;
            parameter.county =AIB_Constants.CONSTANT_COUNTY;
            parameter.postCode =AIB_Constants.CONSTANT_POSTCODE;
            parameter.addressLineOne =AIB_Constants.CONSTANT_ADDRESSLINEONE;
            parameter.addressLineTwo =AIB_Constants.CONSTANT_ADDRESSLINETWO;
            parameter.addressLineThree =AIB_Constants.CONSTANT_ADDRESSLINETHREE;
            parameter.addressLineFour =AIB_Constants.CONSTANT_ADDRESSLINEFOUR;
            parameter.region='ROI';           
            parameter.integrationSettingName='GetCustomerWith404CodeFromCallout_Test';
            String customerSearchParams = JSON.serialize(parameter);
            String jsonString = AIB_GB_CustomerSearchController.getCustomerResult(customerSearchParams);
            Test.stopTest();         
            System.assertEquals(false,String.isBlank(jsonString)); 
            
        }
        
    }
   
    /**************************************************************************************************
    * @Author:      Pradeep Verma
    * @Date:        23/05/2019
    * @Description: This is a test method for Customer Result Response Error.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***************************************************************************************************/    
    private static testmethod void customerResultTestResponseError() {
        User user = [Select
                     FirstName, 
                     LastName, 
                     Email
                     FROM User 
                     WHERE email =: AIB_Constants.CONSTANT_USER_EMAIL
                     LIMIT 1];
        
        System.runAs(user){
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('AIB_GB_CustomerResponseCode404_Test	');
            mock.setStatusCode(AIB_Constants.CALLOUT_STATUS_CODE_FORBIDDEN);
            mock.setStatus(AIB_Constants.CALLOUT_STATUS_FORBIDDEN);
            mock.setHeader('Content-Type', 'application/json');            
            Test.setMock(HttpCalloutMock.class, mock);            
            Test.startTest();
            AIB_GB_CustomerSearchWrapper.Parameter  parameter = new AIB_GB_CustomerSearchWrapper.Parameter();
            parameter.customerType =AIB_Constants.CONSTANT_CUSTOMERTYPE;
            parameter.name= AIB_Constants.CONSTANT_CUSTOMERNAME;
            parameter.dateOfBirth =AIB_Constants.CONSTANT_DATE_OF_BIRTH;
            parameter.NSC =AIB_Constants.CONSTANT_NSC;
            parameter.contactNumberFirst =AIB_Constants.CONSTANT_CONTACT_NUMBER_FIRST;
            parameter.contactNumberSecond =AIB_Constants.CONSTANT_CONTACT_NUMBER_SECOND;
            parameter.cardType =AIB_Constants.CONSTANT_CARDTYPE;
            parameter.cardNumber =AIB_Constants.CONSTANT_CARDNUMBER;
            parameter.selfServiceNo =AIB_Constants.CONSTANT_SELF_SERVICE_NUMBER;
            parameter.county =AIB_Constants.CONSTANT_COUNTY;
            parameter.postCode =AIB_Constants.CONSTANT_POSTCODE;
            parameter.addressLineOne =AIB_Constants.CONSTANT_ADDRESSLINEONE;
            parameter.addressLineTwo =AIB_Constants.CONSTANT_ADDRESSLINETWO;
            parameter.addressLineThree =AIB_Constants.CONSTANT_ADDRESSLINETHREE;
            parameter.addressLineFour =AIB_Constants.CONSTANT_ADDRESSLINEFOUR;
            parameter.region='ROI';           
            parameter.integrationSettingName='GetCustomerWith404CodeFromCallout_Test';
            String customerSearchParams = JSON.serialize(parameter);
            String jsonString = AIB_GB_CustomerSearchController.getCustomerResult(customerSearchParams);
            Test.stopTest();         
            System.assertEquals(false,String.isBlank(jsonString));            
        }
        
    }
    
}