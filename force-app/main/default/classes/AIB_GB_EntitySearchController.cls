/**************************************************************************************************
* @Author:      Swapnil Mohite  
* @Date:        20/05/2019
* @Description: This class is used to for entity search.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
public with sharing class AIB_GB_EntitySearchController {
    /**********************************************************************************************
* @Author:      Swapnil Mohite
* @Date:        20/05/2019
* @Description: This method is used to get entity result 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/
    @AuraEnabled
    public static String getEntityResult(String entityParamStr) {
        
        AIB_GB_EntitySearchWrapper.Result result = new AIB_GB_EntitySearchWrapper.Result(); 
        AIB_GB_EntitySearchWrapper.Parameter param = deserializeParameter(entityParamStr);    
        result = AIB_GB_EntitySearchHelper.getEntityResponse(param);
        return JSON.serialize(result);
    }  
    /**********************************************************************************************
* @Author:      Swapnil Mohite
* @Date:        20/05/2019
* @Description: This method will return the deserialized parameters. 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/
    private static AIB_GB_EntitySearchWrapper.Parameter deserializeParameter(String entityParamStr) {
        AIB_GB_EntitySearchWrapper.Parameter entityParam = new AIB_GB_EntitySearchWrapper.Parameter();
        try {
            entityParam = (AIB_GB_EntitySearchWrapper.Parameter) JSON.deserialize(
                entityParamStr, 
                AIB_GB_EntitySearchWrapper.Parameter.class
            );
        }
        catch(Exception exp) {
            entityParam = null;
            AIB_GenericLogger.log(AIB_GB_EntitySearchController.class.getName(),
                                  AIB_Constants.CONSTANT_DESERIALIZE_PARAM_METHOD,
                                  exp);
        }
        return entityParam;
    }
        /**********************************************************************************************
    * @Author:      Manisha Yadav
    * @Date:        6/06/2019
    * @Description: This method will make callout to external system and create Household 
    * 				and its related records in nCino. 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    @AuraEnabled
    public static String getBorrowerResult(String borrowerListParamStr) {
        AIB_GB_EntitySearchWrapper.Result result = new AIB_GB_EntitySearchWrapper.Result(); 
        
        AIB_GB_EntitySearchWrapper.Parameter param = deserializeParameter(borrowerListParamStr);
        
        result = AIB_GB_EntitySearchHelper.getEntityBorrowerResponse(param);
        
        return json.serialize(result);
    }    
}