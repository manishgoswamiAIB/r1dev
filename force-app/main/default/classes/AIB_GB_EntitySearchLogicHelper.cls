/**********************************************************************************************
* @Author:      Seevendra Singh
* @Date:        12/06/2019 
* @Description: This is a helper class for AIB_GB_EntitySearchLogic. It is a without sharing class 
*				Because it needs to delete data for which user don't have delete access.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/
public without sharing class AIB_GB_EntitySearchLogicHelper {
    
	
}