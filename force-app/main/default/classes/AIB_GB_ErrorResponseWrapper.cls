/**************************************************************************************************
* @Author:      Sameer Jewalikar
* @Date:        29/05/2019
* @PageCode:    EW
* @Description: This is a Error Response wrapper class to be used for Lightning Component 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
public class AIB_GB_ErrorResponseWrapper {

	public String timestamp {get;set;} 
	public String status {get;set;} 
	public String error {get;set;} 
	public String uuid {get;set;} 
	public String code {get;set;} 
	public String path {get;set;} 
	public String message {get;set;} 
	public String info {get;set;} 
	public String x_exception {get;set;} 
	public String errors {get;set;} 
	
}