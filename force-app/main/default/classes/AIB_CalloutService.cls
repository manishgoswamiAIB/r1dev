/**************************************************************************************************
* @Author:      Seevendra Singh  
* @Date:        20/05/2019
* @Description: This is the framework class that makes callouts to core application.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
public without sharing class AIB_CalloutService {
    
    
    /**********************************************************************************************
    * @Author:      Seevendra Singh  
	* @Date:        20/05/2019
	* @Description: This method makes callout based on the parameters received from calling class.
	* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description] 
    ***********************************************************************************************/
    public static AIB_CalloutWrapper.Result makeCallout (
        								AIB_CalloutWrapper.Parameter calloutParam
    ) {
        
        AIB_CalloutWrapper.Result result = new AIB_CalloutWrapper.Result();
        
        Http http = new Http();
        HttpResponse response = new HttpResponse();
        HttpRequest request = new HttpRequest();
        
        try {
            //i: query the custom metadata by DeveloperName
            AIB_IntegrationSetting_CMD__mdt[] integrationSettings = 
                			AIB_CalloutServiceProvider.queryIntegrationSettingByName(
                    											calloutParam.integrationSettingName
                											);
            if ( integrationSettings != null 
                    && integrationSettings.size() > 0
            ) {
                    
                AIB_IntegrationSetting_CMD__mdt integrationSetting = 
                                                            integrationSettings[0];
                //i: method to create HttpRequest from the parameters passed
                request = createHttpRequest(integrationSetting, calloutParam);
                if (request != null) {
                    //i: Check if mock response is needed or not 
                    if (integrationSetting.AIB_RequireMock__c) {
                        //i: method to return HttpResponse using JSON stored in Static Resource
                        response = generateMockResponse(integrationSetting);
                    }
                    else { 
                        //i: send request to external application
                        response = http.send(request);
                    }
                }
                result = handleResponse(calloutParam, integrationSetting, request, response);
                
            }
            else {
                //i: if custom metadata is not found then throw the error which will be handled 
                //   by catch block
                throw new CalloutServiceException(Label.AIB_Error_IntegrationSettingNotFound);
            }
        }
        catch(Exception exp) {
            result.hasError = true;
            result.message = Label.AIB_Error_CalloutExceptionOccurred;
            result.code = 'EC-001';
            AIB_GenericLogger.log(AIB_CalloutService.class.getName(), 'makeCallout', exp);
            
        }
        
        return result;
        
    }
    
	/**********************************************************************************************
    * @Author:      Seevendra Singh  
	* @Date:        20/05/2019
	* @Description: This method handles the response, logs request and response in Exception log and 
	*				return the result accordingly.
	* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description] 
    ***********************************************************************************************/
	public static AIB_CalloutWrapper.Result handleResponse (
											AIB_CalloutWrapper.Parameter calloutParam,
        									AIB_IntegrationSetting_CMD__mdt integrationSetting, 
											HttpRequest request, 
											HttpResponse response
	) {
		
		AIB_CalloutWrapper.Result result = new AIB_CalloutWrapper.Result();
		
		try {
			//i: store response in result and return it. Checks will be performed by 
			//   individual method which has called this method
			if (response != null) {
				if (response.getStatusCode() == AIB_Constants.CALLOUT_STATUS_CODE_SUCCESS) {
					result.hasInfo = true;
					result.code = String.valueOf(response.getStatusCode());
					result.response = response;
					//i: if logging is enabled in integration setting 
					//   then create an Exception log record
					if (integrationSetting.AIB_LogAll__c) {
                        System.debug('Callout Params: '+calloutParam);
                        System.debug('Endpoint Params: '+integrationSetting.AIB_RequestEndpoint__c);
						String logId = AIB_GenericLogger.log(
												AIB_CalloutService.class.getName(), 
												'handleResponse',
												calloutParam, 
                            					integrationSetting.AIB_RequestEndpoint__c,
                            					response.getStatusCode()
											);
						//i: save request and response as attachments
						createLogAttachments(logId, request, response);
					}
				}
				else { 
					result.hasError = true;
					result.response = response;
					result.code = String.valueOf(response.getStatusCode());
					
					if (integrationSetting.AIB_LogAll__c 
						|| integrationSetting.AIB_LogErrorsOnly__c
					) {
						//i: if logging is enabled in integration setting 
						//   then create an Exception log record
						String logId = AIB_GenericLogger.log(
												AIB_CalloutService.class.getName(), 
												'handleResponse',
												calloutParam,
                            					integrationSetting.AIB_RequestEndpoint__c,
                            					response.getStatusCode()
											);
						//i: save request and response as attachments of the log
						createLogAttachments(logId, request, response);
					}
				}
			}
			else{
				throw new CalloutException(Label.AIB_Error_NoResponseReceived);
			}
		}
		catch(Exception exp) {
            result.hasError = true;
            result.message = Label.AIB_Error_CalloutExceptionOccurred;
            result.code = 'EC-001';
            AIB_GenericLogger.log(AIB_CalloutService.class.getName(), 'handleResponse', exp);
        }
		
		return result;
	}
    
	/**********************************************************************************************
    * @Author:      Seevendra Singh  
    * @Date:        20/05/2019
    * @Description: This method creates Http request using passed parameters.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static HttpRequest createHttpRequest (
        							AIB_IntegrationSetting_CMD__mdt integrationSetting, 
        							AIB_CalloutWrapper.Parameter calloutParam
    ) {
        
        HttpRequest request = new HttpRequest();
        try {
              String userJurisdiction = (String.isNotBlank(calloutParam.userRegion) ? 
                                         calloutParam.userRegion : 
                                         Label.AIB_Value_DefaultUserJurisdiction);
                String endpoint = integrationSetting.AIB_RequestEndpoint__c.replace(
                                                        Label.AIB_Label_UserRegionPlaceholder, 
                                                        userJurisdiction
                                                    );
                
                endpoint = endpoint + (String.isNotBlank(calloutParam.urlSearchParams) ? 
                                       							calloutParam.urlSearchParams : '');
                
                System.debug('Integration Endpoint is '+endpoint);
                
                request.setEndpoint(endpoint);
                request.setMethod(integrationSetting.AIB_RequestType__c);
                
                if (String.isNotBlank(calloutParam.requestBody)) {
                    request.setBody(calloutParam.requestBody);
                }  
            //i: Setting headers in request
            AIB_IntegrationRequestHeader_CMD__mdt[] requestHeaders = 
                	AIB_CalloutServiceProvider.queryHeaderDataBySettingId(integrationSetting.Id);
            for (AIB_IntegrationRequestHeader_CMD__mdt requestHeader : requestHeaders) {
                request.setHeader(
                    requestHeader.AIB_RequestHeaderName__c, 
                    requestHeader.AIB_RequestHeaderValue__c
                );
            }
        }
        catch(Exception exp) {
            request = null;
            AIB_GenericLogger.log(
                	AIB_CalloutService.class.getName(), 
                	'createHttpRequest', 
                	exp
            	);
            
        }
        
        return request;
                    
    }
                            
    /**********************************************************************************************
    * @Author:      Seevendra Singh  
    * @Date:        20/05/2019
    * @Description: This method retrieves mockresponse from static resource through provider class
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static HttpResponse generateMockResponse(
       							 AIB_IntegrationSetting_CMD__mdt integrationSetting
    ) {
        HttpResponse response = new HttpResponse();
        try {
            StaticResource[] mockResponses =  
                            AIB_CalloutServiceProvider.queryMockResponseFromResource(
                                                integrationSetting.AIB_MockResponseResourceName__c
                            );
            
            if (mockResponses != null 
                && mockResponses.size() > 0
            ) {
                StaticResource mockResponse = mockResponses[0];
                if (mockResponse.BodyLength == 0) {
                    response.setStatus(AIB_Constants.CALLOUT_STATUS_CUSTOMERNOTFOUND);
                    response.setStatusCode(AIB_Constants.CALLOUT_STATUS_CODE_CUSTOMERNOTFOUND);
                }
                else {
                    response.setStatus(AIB_Constants.CALLOUT_STATUS_SUCCESS);
                    response.setStatusCode(AIB_Constants.CALLOUT_STATUS_CODE_SUCCESS);                                 
                    response.setBody(mockResponse.body.toString());
                }
            }
            else {
                throw new CalloutServiceException(Label.AIB_Error_MockResponseNotFound);
            }
        }
        catch (Exception mockGeneratorException) {
            response.setStatus(AIB_Constants.CALLOUT_STATUS_INTERNALSERVERERROR);
            response.setStatusCode(AIB_Constants.CALLOUT_STATUS_CODE_INTERNALSERVERERROR);
            AIB_GenericLogger.log(
                                AIB_CalloutService.class.getName(), 
                                'generateMockResponse', 
                                mockGeneratorException
                                );
        }
        return response;
    }
    
    /**********************************************************************************************
    * @Author:      Seevendra Singh  
    * @Date:        20/05/2019
    * @Description: This method retrieves mockresponse from static resource through provider class
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static void createLogAttachments (String logId, 
                                             HttpRequest request, 
                                             HttpResponse response
    ) {
        try {
            List<Attachment> calloutData = new List<Attachment>();
            
            if (String.isNotBlank(logId) 
                && request != null
            ) {
                Attachment requestAttachment = new Attachment();
                requestAttachment.ParentId = logId;
                requestAttachment.Name = AIB_Constants.LOG_REQUEST_ATTACHMENT_NAME;
                String requestData = 'Request Data: ';
                requestData = requestData + '\nEndpoint: ' + request.getEndpoint();
                requestData = requestData + '\nMethod: ' + request.getMethod();
                requestData = requestData + '\nBody: ' + request.getBody();
                requestAttachment.Body =  Blob.valueOf(requestData);
                calloutData.add(requestAttachment);
            }
            
            if (String.isNotBlank(logId) 
                && response != null
            ) {
                Attachment responseAttachment = new Attachment();
                responseAttachment.ParentId = logId;
                responseAttachment.Name = AIB_Constants.LOG_RESPONSE_ATTACHMENT_NAME;
                String responseData = 'Response Data:';
                responseData = responseData + '\nStatus Code: ' + response.getStatusCode();
                responseData = responseData + '\nStatus: ' + response.getStatus();
                responseData = responseData + '\nBody: ' + response.getBody();
                responseAttachment.Body =  Blob.valueOf(responseData);
                calloutData.add(responseAttachment);
            }
            
            if (calloutData.size() > 0) {
                insert calloutData;
            }
            
        }
        catch(exception attachmentExp){
            AIB_GenericLogger.log(
                                AIB_CalloutService.class.getName(), 
                                'generateMockResponse', 
                                attachmentExp
                             );
        }
    }

    public without sharing class CalloutServiceException extends Exception {}

}