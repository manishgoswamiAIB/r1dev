/**************************************************************************************************
* @Author:      Manisha Yadav
* @Date:        23/05/2019
* @Description: This is a test class for AIB_GB_EntitySearchController class.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
@isTest(SeeAllData=false)
public class AIB_GB_EntitySearchController_Test{
    
    private static final String PROFILE_NAME ='AIB Business Super User';
    private static final String FIRST_NAME ='Fname';
    private static final String LAST_NAME ='Lname';
    private static final String EMAIL ='tuser1234@test.org';
    private static final String USER_NAME ='tuser1212@test.org';
    private static final String ALIAS ='tuser1';
    private static final String TIME_ZONE_SID_KEY ='America/Los_Angeles';
    private static final String LOCALE_SID_KEY = 'en_US';
    private static final String LANGUAGE_LOCALE_KEY ='en_US';
    private static final String EMAIL_ENCODING_KEY ='UTF-8';
    private static final String AIB_JURISDICTION ='ROI';
    private static final String ACCOUNT_NUMBER ='Acc No';
    private static final String COINS_NUMBER ='123432';
    private static final String COINS_TEAM_CODE ='4321';
    private static final String ENTITY_HASWARNING ='GetEntityWith404CodeFromCallout_Test';
    
    @testSetup 
    /**************************************************************************************************
* @Author:      Manisha Yadav
* @Date:        23/05/2019
* @Description: This method is used to setup data for test class
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
    private static void setup(){
        Profile profileId =  [Select Id from Profile where Name =: PROFILE_NAME LIMIT 1] ;
        List<User> testUser = new List<User>();
        User user = new User(FirstName = FIRST_NAME,
                             LastName = LAST_NAME,
                             email = EMAIL,
                             Username = USER_NAME,
                             Alias =ALIAS,
                             ProfileId = profileId.id,
                             TimeZoneSidKey=TIME_ZONE_SID_KEY,
                             LocaleSidKey=LOCALE_SID_KEY,
                             LanguageLocaleKey=LANGUAGE_LOCALE_KEY,
                             EmailEncodingKey=EMAIL_ENCODING_KEY,
                             AIB_Jurisdiction__c=AIB_JURISDICTION);
        testUser.add(user);
        Database.insert(testUser);
    }
    
    /**************************************************************************************************
* @Author:      Swapnil Mohite
* @Date:        23/05/2019
* @Description: This method will test getEntityResult & deserializeParameter method of AIB_GB_EntitySearchController class.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
    private static testmethod void entityResultInfoTest() {
        User user = [Select FirstName,LastName,Email 
                     FROM User 
                     where email =:EMAIL 
                     LIMIT 1];
        
        System.runAs(user){
            AIB_GB_EntitySearchWrapper.Parameter  parameter = new AIB_GB_EntitySearchWrapper.Parameter();
            parameter.entityName = FIRST_NAME;
            parameter.accountNumber = ACCOUNT_NUMBER;
            parameter.coinsNumber = COINS_NUMBER;
            parameter.entityId = COINS_NUMBER;
            parameter.coinsGroupLeader = COINS_NUMBER;
            parameter.coinsTeamCode = COINS_TEAM_CODE;
            parameter.integrationSettingName = Label.AIB_GB_IntegrationSettingName;  
            
            Test.startTest();
            String entitySearchParams = JSON.serialize(parameter);
            String jsonString = AIB_GB_EntitySearchController.getEntityResult(entitySearchParams); 
            //For Exception 
            // AIB_GB_EntitySearchController.getEntityResult(null);
            Test.stopTest();
            system.assertNotEquals(null, jsonString);
        }
    }
    /**************************************************************************************************
* @Author:      Swapnil Mohite
* @Date:        23/05/2019
* @Description: This method will test getEntityResult & deserializeParameter method of AIB_GB_EntitySearchController class.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
    private static testmethod void entityResultErrorTest() {
        User user = [Select FirstName,LastName,Email 
                     FROM User 
                     where email =:EMAIL 
                     LIMIT 1];
        
        System.runAs(user){
            AIB_GB_EntitySearchWrapper.Parameter  parameter = new AIB_GB_EntitySearchWrapper.Parameter();
            parameter.entityName= FIRST_NAME;
            parameter.accountNumber = ACCOUNT_NUMBER;
            parameter.coinsNumber =COINS_NUMBER;
            parameter.entityId =COINS_NUMBER;
            parameter.coinsGroupLeader =COINS_NUMBER;
            parameter.coinsTeamCode =COINS_TEAM_CODE;
            
            Test.startTest();
            String entitySearchParams = JSON.serialize(parameter);
            String jsonString = AIB_GB_EntitySearchController.getEntityResult(entitySearchParams);
            //For Exception 
            AIB_GB_EntitySearchController.getEntityResult(null);
            Test.stopTest();
            system.assertNotEquals(null, jsonString);
        }
    }
    
    /**************************************************************************************************
* @Author:      Swapnil Mohite
* @Date:        23/05/2019
* @Description: This method will test getEntityResult & deserializeParameter method of AIB_GB_EntitySearchController class.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
    private static testmethod void entityResultWarningTest() {
        User user = [Select FirstName,LastName,Email 
                     FROM User 
                     where email =:EMAIL 
                     LIMIT 1];
        
        System.runAs(user){
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('AIB_GB_CustomerResponseCode404_Test');
            mock.setStatusCode(AIB_Constants.CALLOUT_STATUS_CODE_RECORDNOTFOUND);
            mock.setStatus(AIB_Constants.CALLOUT_STATUS_RECORDNOTFOUND);
            mock.setHeader('Content-Type', 'application/json');
            
            Test.setMock(HttpCalloutMock.class, mock);
            AIB_GB_EntitySearchWrapper.Parameter  parameter = new AIB_GB_EntitySearchWrapper.Parameter();
            parameter.entityName= FIRST_NAME;
            parameter.accountNumber = ACCOUNT_NUMBER;
            parameter.coinsNumber =COINS_NUMBER;
            parameter.entityId =COINS_NUMBER;
            parameter.coinsGroupLeader =COINS_NUMBER;
            parameter.coinsTeamCode =COINS_TEAM_CODE;
            parameter.integrationSettingName = 'GetEntityWith404CodeFromCallout_Test';
            Test.startTest();
            String entitySearchParams = JSON.serialize(parameter);
            String jsonString = AIB_GB_EntitySearchController.getEntityResult(entitySearchParams);
            Test.stopTest();
            system.assertNotEquals(null, jsonString);
        }
    }
    /**************************************************************************************************
* @Author:      Swapnil Mohite
* @Date:        23/05/2019
* @Description: This method will test getEntityResult & deserializeParameter method of AIB_GB_EntitySearchController class.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
    private static testmethod void entityResultTest() {
         User user = [Select FirstName,LastName,Email 
                     FROM User 
                     where email =:EMAIL 
                     LIMIT 1];
        
        System.runAs(user){
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('AIB_GB_CustomerResponseCode404_Test');
            mock.setStatusCode(AIB_Constants.CALLOUT_STATUS_CODE_INTERNALSERVERERROR);
            mock.setStatus(AIB_Constants.CALLOUT_STATUS_INTERNALSERVERERROR);
            mock.setHeader('Content-Type', 'application/json');
            
            Test.setMock(HttpCalloutMock.class, mock);
            AIB_GB_EntitySearchWrapper.Parameter  parameter = new AIB_GB_EntitySearchWrapper.Parameter();
            parameter.entityName = FIRST_NAME;
            parameter.accountNumber = ACCOUNT_NUMBER;
            parameter.coinsNumber = COINS_NUMBER;
            parameter.entityId = COINS_NUMBER;
            parameter.coinsGroupLeader = COINS_NUMBER;
            parameter.coinsTeamCode = COINS_TEAM_CODE;
            parameter.integrationSettingName = 'GetCustomerWith404CodeFromCallout_Test';  
            
            Test.startTest();
            String entitySearchParams = JSON.serialize(parameter);
            String jsonString = AIB_GB_EntitySearchController.getEntityResult(entitySearchParams); 
            //For Exception 
            // AIB_GB_EntitySearchController.getEntityResult(null);
            Test.stopTest();
            system.assertNotEquals(null, jsonString);
        }
        
    }
    /**************************************************************************************************
* @Author:      Pradeep Verma
* @Date:        06/06/2019
* @Description: This method will test getBorrowerResult & deserializeParameter method of AIB_GB_EntitySearchController class.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
    private static testmethod void BorrowerResultInfoTest() {
        User user = [Select FirstName,LastName,Email 
                     FROM User 
                     where email =:EMAIL 
                     LIMIT 1];
        
        System.runAs(user){
            AIB_GB_EntitySearchWrapper.Parameter  parameter = new AIB_GB_EntitySearchWrapper.Parameter();
            parameter.entityName = FIRST_NAME;
            parameter.accountNumber = ACCOUNT_NUMBER;
            parameter.coinsNumber = COINS_NUMBER;
            parameter.entityId = COINS_NUMBER;
            parameter.coinsGroupLeader = COINS_NUMBER;
            parameter.coinsTeamCode = COINS_TEAM_CODE;
            parameter.integrationSettingName = Label.AIB_GB_IntegrationSettingName;  
            
            Test.startTest();
            String borrowerListParam = JSON.serialize(parameter);
            String jsonString = AIB_GB_EntitySearchController.getBorrowerResult(borrowerListParam); 
            
            Test.stopTest();
            system.assertNotEquals(null, jsonString);
        }
    }
    /**************************************************************************************************
* @Author:      Pradeep Verma
* @Date:        06/06/2019
* @Description: This method will test getBorrowerResult & deserializeParameter method of AIB_GB_EntitySearchController class.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
    private static testmethod void BorrowerResultErrorTest() {
        User user = [Select FirstName,LastName,Email 
                     FROM User 
                     where email =:EMAIL 
                     LIMIT 1];
        
        System.runAs(user){
            AIB_GB_EntitySearchWrapper.Parameter  parameter = new AIB_GB_EntitySearchWrapper.Parameter();
            parameter.entityName= FIRST_NAME;
            parameter.accountNumber = ACCOUNT_NUMBER;
            parameter.coinsNumber =COINS_NUMBER;
            parameter.entityId =COINS_NUMBER;
            parameter.coinsGroupLeader =COINS_NUMBER;
            parameter.coinsTeamCode =COINS_TEAM_CODE;
            
            Test.startTest();
            String borrowerListParam = JSON.serialize(parameter);
            String jsonString = AIB_GB_EntitySearchController.getBorrowerResult(borrowerListParam);
            //For Exception 
            AIB_GB_EntitySearchController.getBorrowerResult(null);
            Test.stopTest();
            system.assertNotEquals(null, jsonString);
        }
    }
    
    /**************************************************************************************************
* @Author:     Pradeep Verma
* @Date:        06/06/2019
* @Description: This method will test getBorrowerResult & deserializeParameter method of AIB_GB_EntitySearchController class.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
    private static testmethod void BorrowerResultWarningTest() {
        User user = [Select FirstName,LastName,Email 
                     FROM User 
                     where email =:EMAIL 
                     LIMIT 1];
        
        System.runAs(user){
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('AIB_GB_CustomerResponseCode404_Test');
            mock.setStatusCode(AIB_Constants.CALLOUT_STATUS_CODE_RECORDNOTFOUND);
            mock.setStatus(AIB_Constants.CALLOUT_STATUS_RECORDNOTFOUND);
            mock.setHeader('Content-Type', 'application/json');
            
            Test.setMock(HttpCalloutMock.class, mock);
            AIB_GB_EntitySearchWrapper.Parameter  parameter = new AIB_GB_EntitySearchWrapper.Parameter();
            parameter.entityName= FIRST_NAME;
            parameter.accountNumber = ACCOUNT_NUMBER;
            parameter.coinsNumber =COINS_NUMBER;
            parameter.entityId =COINS_NUMBER;
            parameter.coinsGroupLeader =COINS_NUMBER;
            parameter.coinsTeamCode =COINS_TEAM_CODE;
            parameter.integrationSettingName = 'GetEntityWith404CodeFromCallout_Test';
            Test.startTest();
            String borrowerListParam = JSON.serialize(parameter);
            String jsonString = AIB_GB_EntitySearchController.getBorrowerResult(borrowerListParam);
            Test.stopTest();
            system.assertNotEquals(null, jsonString);
        }
    }
   
      /**************************************************************************************************
    * @Author:      Pradeep Verma
    * @Date:        06/06/2019
    * @Description: This is a test method for Entity Other Errors.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***************************************************************************************************/
    private static testmethod void getEntityOtherErrorTest(){
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('AIB_GB_CustomerResponseCode403_Test');
        mock.setStatusCode(AIB_Constants.CALLOUT_STATUS_CODE_FORBIDDEN);
        mock.setStatus(AIB_Constants.CALLOUT_STATUS_FORBIDDEN);
        mock.setHeader('Content-Type', 'application/json');
        
        Test.setMock(HttpCalloutMock.class, mock);
        AIB_GB_EntitySearchWrapper.Parameter  parameter = new AIB_GB_EntitySearchWrapper.Parameter();
            parameter.entityName= FIRST_NAME;
            parameter.accountNumber = ACCOUNT_NUMBER;
            parameter.coinsNumber =COINS_NUMBER;
            parameter.entityId =COINS_NUMBER;
            parameter.coinsGroupLeader =COINS_NUMBER;
            parameter.coinsTeamCode =COINS_TEAM_CODE;
            parameter.integrationSettingName = 'GetEntityWith404CodeFromCallout_Test';
            Test.startTest();
            String entitySearchParams = JSON.serialize(parameter);
            String jsonString = AIB_GB_EntitySearchController.getEntityResult(entitySearchParams);
            Test.stopTest();
            system.assertNotEquals(null, jsonString);
      
    }
  
       /**************************************************************************************************
    * @Author:      Pradeep Verma
    * @Date:        06/06/2019
    * @Description: This is a test method for Entity Other Errors.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***************************************************************************************************/
    private static testmethod void getBorrowerOtherErrorTest(){
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('AIB_GB_CustomerResponseCode403_Test');
        mock.setStatusCode(AIB_Constants.CALLOUT_STATUS_CODE_FORBIDDEN);
        mock.setStatus(AIB_Constants.CALLOUT_STATUS_FORBIDDEN);
        mock.setHeader('Content-Type', 'application/json');
        
        Test.setMock(HttpCalloutMock.class, mock);
        AIB_GB_EntitySearchWrapper.Parameter  parameter = new AIB_GB_EntitySearchWrapper.Parameter();
            parameter.entityName= FIRST_NAME;
            parameter.accountNumber = ACCOUNT_NUMBER;
            parameter.coinsNumber =COINS_NUMBER;
            parameter.entityId =COINS_NUMBER;
            parameter.coinsGroupLeader =COINS_NUMBER;
            parameter.coinsTeamCode =COINS_TEAM_CODE;
            parameter.integrationSettingName = 'GetEntityWith404CodeFromCallout_Test';
            Test.startTest();
            String borrowerListParam = JSON.serialize(parameter);
            String jsonString = AIB_GB_EntitySearchController.getBorrowerResult(borrowerListParam);
            Test.stopTest();
            system.assertNotEquals(null, jsonString);
      
    }
      
}