/**********************************************************************************************
* @Author:      Kuldeep parihar
* @Date:        20/05/2019
* @Description: This is an Base wrapper class to store results. 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/
global class AIB_BaseBorrowerWrapper extends AIB_BaseWrapper {
    
    /**********************************************************************************************
    * @Author:      Kuldeep pariharss
    * @Date:        20/05/2019
    * @Description: This is an Entity search inner wrapper class to store Juridiction. 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    global class Result extends AIB_BaseWrapper.Result {
        public String jurisdiction {get; set;} 
    }
    
  
    
}