@isTest
private without sharing class csaTestClearLoanFieldsHandler {

    static LLC_BI__Loan__c loanDataSetup() {
        Account testAccount = new Account(
            Name='Testing Account'
        );
        insert testAccount;

        LLC_BI__Loan__c testLoan = new LLC_BI__Loan__c(
            Name = 'Testing Loan',
            LLC_BI__Account__c = testAccount.Id
        );
        return testLoan;
    }

    @isTest
    static void testShouldNotExecute() {
        nCSA_FRAME.csaTestDataFactory.createForceSystemProperty(
            csaClearLoanFieldsConstants.PROPERTY_CATEGORY,
            csaClearLoanFieldsConstants.PROPERTY_CLEARKEY,
            csaClearLoanFieldsConstants.PROPERTY_CLEARVALUE,
            false,
            null
        );
    }

    @isTest
    static void testCopyLoan() {
        nCSA_FRAME.csaTestDataFactory.createForceSystemProperty(
            csaClearLoanFieldsConstants.PROPERTY_CATEGORY,
            csaClearLoanFieldsConstants.PROPERTY_CLEARKEY,
            csaClearLoanFieldsConstants.PROPERTY_CLEARVALUE,
            true,
            null
        );

        Test.startTest();

        LLC_BI__Loan__c testLoan = loanDataSetup();
        testLoan.LLC_BI__Is_Copy__c = true;
        insert testLoan;

        Test.stopTest();

        for(Schema.FieldSetMember field: getCopyFieldset()) {
            if(testLoan.get(field.getFieldPath()) == true || testLoan.get(field.getFieldPath()) == false) {
                System.assert(testLoan.get(field.getFieldPath()) == false);
            } else {
                System.assert(testLoan.get(field.getFieldPath()) == null);
            }
        }
    }

    @isTest
    static void testRenewalLoan() {
        nCSA_FRAME.csaTestDataFactory.createForceSystemProperty(
            csaClearLoanFieldsConstants.PROPERTY_CATEGORY,
            csaClearLoanFieldsConstants.PROPERTY_CLEARKEY,
            csaClearLoanFieldsConstants.PROPERTY_CLEARVALUE,
            true,
            null
        );

        Test.startTest();

        LLC_BI__Loan__c testLoan = loanDataSetup();
        testLoan.LLC_BI__isRenewal__c = true;
        insert testLoan;

        Test.stopTest();

        Set<String> fields = new Set<String>();
        for(Schema.FieldSetMember field: getRenModFieldset()) {
            fields.add(field.getFieldPath());
        }

        for(Schema.FieldSetMember field: getRenFieldset()) {
            fields.add(field.getFieldPath());
        }


        for(String field: fields) {
            if(testLoan.get(field) == true || testLoan.get(field) == false) {
                System.assert(testLoan.get(field) == false);
            } else {
                System.assert(testLoan.get(field) == null);
            }
        }
    }

    @isTest
    static void testModificationLoan() {
        nCSA_FRAME.csaTestDataFactory.createForceSystemProperty(
            csaClearLoanFieldsConstants.PROPERTY_CATEGORY,
            csaClearLoanFieldsConstants.PROPERTY_CLEARKEY,
            csaClearLoanFieldsConstants.PROPERTY_CLEARVALUE,
            true,
            null
        );

        Test.startTest();

        LLC_BI__Loan__c testLoan = loanDataSetup();
        testLoan.LLC_BI__Is_Modification__c = true;
        insert testLoan;

        Test.stopTest();

        Set<String> fields = new Set<String>();
        for(Schema.FieldSetMember field: getRenModFieldset()) {
            fields.add(field.getFieldPath());
        }

        for(Schema.FieldSetMember field: getModFieldset()) {
            fields.add(field.getFieldPath());
        }


        for(String field: fields) {
            if(testLoan.get(field) == true || testLoan.get(field) == false) {
                System.assert(testLoan.get(field) == false);
            } else {
                System.assert(testLoan.get(field) == null);
            }
        }
    }

    @testSetup
    private static void testSetup() {
        nFORCE.BeanRegistry.getInstance().registerBean(
            'nCSA_FRAME.ClassTypeProvider',
            nFORCE.ClassTypeProvider.class,
            nCSA_FRAME.ClassTypeProvider.class
        );

        Map<String, Object> params = new Map<String, Object>{
            'exclude' => 'Test_Do_Not_Delete'
        };

        nFORCE.RemoteActionController.invoke('nCSA_FRAME.csaAdminDashboardRemote.InstallTriggers', params);
    }

    private static List<Schema.FieldSetMember> getCopyFieldset() {
        return SObjectType.LLC_BI__Loan__c.FieldSets.getMap().get(csaClearLoanFieldsConstants.FIELDSET_COPY).getFields();
    }

    private static List<Schema.FieldSetMember> getRenModFieldset() {
        return SObjectType.LLC_BI__Loan__c.FieldSets.getMap().get(csaClearLoanFieldsConstants.FIELDSET_RENMOD).getFields();
    }

    private static List<Schema.FieldSetMember> getModFieldset() {
        return SObjectType.LLC_BI__Loan__c.FieldSets.getMap().get(csaClearLoanFieldsConstants.FIELDSET_MOD).getFields();
    }

    private static List<Schema.FieldSetMember> getRenFieldset() {
        return SObjectType.LLC_BI__Loan__c.FieldSets.getMap().get(csaClearLoanFieldsConstants.FIELDSET_REN).getFields();
    }
}